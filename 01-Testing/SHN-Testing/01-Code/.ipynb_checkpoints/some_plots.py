
import tensorflow as tf
import tensorflow_similarity as tfsim
from matplotlib import pyplot as plt
from collections import Counter
import tensorflow_similarity as tfsim
FLAGS = tf.compat.v1.flags.FLAGS
FLAGS.__delattr__("wikipedia_auto_select_flume_mode")
from shn_operations import get_data,MySim
from augmenters import RandAugment,AutoAugment,SimCLRAugmenter,RandomErasing
from multiprocessing import cpu_count
FLAGS = tf.compat.v1.flags.FLAGS
FLAGS.__delattr__("wikipedia_auto_select_flume_mode")



# import matplotlib.pyplot as plt
#
# # Pie chart, where the slices will be ordered and plotted counter-clockwise:
#
# import matplotlib.pyplot as plt
#
# # Pie chart, where the slices will be ordered and plotted counter-clockwise:
# explode = [0.0,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1]
#
# fig1, ax1 = plt.subplots()
# ax1.pie(sizes, labels=labels,labeldistance=None,explode=explode, autopct='%1.1f%%',pctdistance=1.2,
#         shadow=False, startangle=90)
# ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
# ax1.legend(loc="lower right")
# plt.show()

img_size = 224
train_method = "high_balance"
datatype = "test"
def get_querie_data(class_mappings,train_method,datatype,examples_per_class):
    # Get max examples_per_class examples per class by getting the needed indexes
    queries_index = []
    train_class_list = [*Counter(class_mappings[train_method]["Y_"+datatype]).keys()]
    train_class_list.sort()
    num_classes = len(train_class_list)
    for class_y in range(num_classes):
        c_index = [i for i, y in enumerate(class_mappings[train_method]["Y_"+datatype]) if y == class_y]
        queries_index.extend(c_index[0:examples_per_class])
    queries_x = class_mappings[train_method]["X_"+datatype][queries_index]
    return queries_x,queries_index
X_train,X_val,y_train,y_val,train_class_list,class_mappings = get_data(training_with_top_n_frequency_classes=10,
                                                                       img_size=img_size,
                                                                       train_method=train_method,
                                                                       )
X_train,Y_train = get_querie_data(class_mappings=class_mappings,train_method=train_method,datatype=datatype,examples_per_class=1)

# loaders = []
# augtypes = ["AutoAugment", "RandomErasing", "RandAugment", "SimCLR"]
# num_images=5
# for augtype in augtypes:
#         if augtype == "AutoAugment":
#                 augmenter = AutoAugment(augmentation_name="reduced_cifar10")
#
#         elif augtype == "RandomErasing":
#                 augmenter = RandomErasing(probability=0.4)
#
#         elif augtype == "RandAugment":
#                 augmenter = RandAugment(num_layers=2, magnitude=5)
#
#         elif augtype == "SimCLR":
#                 augmenter = SimCLRAugmenter(img_size,img_size)
#
#
#         @tf.function()
#         def process(img):
#                 img = tf.image.grayscale_to_rgb(img)
#                 img = tf.keras.layers.Rescaling(1/255)(img)
#                 img = augmenter.distort(img)
#                 img = tf.keras.layers.Rescaling(255)(img)
#                 img = tf.image.rgb_to_grayscale(img)
#                 return img
#
#
#         def loader(x, *args):
#                 imgs = tf.stack(x)
#                 imgs = tf.map_fn(process, imgs, parallel_iterations=cpu_count(), dtype="float32")
#                 return imgs
#         loaders.append(loader)
#
#






contrastive_augmentation = {"min_area": 0.25, "brightness": 0.6, "jitter": 0.2}
contrastive_augmentation_strong = {"min_area": 0.2, "brightness": 0.8, "jitter": 0.3}
classification_augmentation = {"min_area": 0.75, "brightness": 0.3, "jitter": 0.1}
augs = [
    classification_augmentation,
    # contrastive_augmentation,
    contrastive_augmentation_strong,
]

def visualize_augmentations(num_images):
    # Sample a batch from a dataset
    images = X_train[1:num_images+1]
    # Apply augmentations
    augmented_images = []
    for a in  augs:
        augmenter = MySim(**a)
        imgs = tf.stack(images)
        imgs = tf.keras.layers.Rescaling(1 / 255)(imgs)
        imgs = augmenter(imgs)
        imgs = tf.keras.layers.Rescaling(255)(imgs)
        augmented_images.append(imgs)

    augmented_images=zip(images,
                         augmented_images[0],
                         augmented_images[1])
    row_titles = [
        "Original",
        "Weak",
        "Strong",
        # "Stronger:",
    ]
    plt.figure(figsize=(num_images * 2.2, 4 * 2.2), dpi=100)
    for column, image_row in enumerate(augmented_images):
        for row, image in enumerate(image_row):
            plt.subplot(4, num_images, row * num_images + column + 1)
            plt.imshow(image)
            if column == 3:
                plt.title(row_titles[row], loc="center",fontsize=25,pad=10)
            plt.axis("off")
    plt.tight_layout()


visualize_augmentations(num_images=7)

