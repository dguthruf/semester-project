import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "3"

from tensorflow.keras.callbacks import TensorBoard
from collections import Counter
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
import tensorflow_similarity as tfsim
tfsim.utils.tf_cap_memory()
FLAGS = tf.compat.v1.flags.FLAGS
FLAGS.__delattr__("wikipedia_auto_select_flume_mode")
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.samplers import MultiShotMemorySampler,SingleShotMemorySampler  # sample data
from shn_operations import get_data,MySim
from shn_visualization import tensorboard_visualization
from augmenters import RandAugment,AutoAugment,SimCLRAugmenter,RandomErasing
from multiprocessing import cpu_count
import gc
from tensorflow.keras import backend as k
from tensorflow.keras.callbacks import Callback
from nn import gpu_augmenter



"""
-------------------------------------------------------------------------------------------------------------------
-----------------------------------------------Define Overall Things-----------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
training_with_top_n_frequency_classes = 10 #Top 5 -> 200||Top 8 -> 100||Top 11 -> 30||Top 17 -> 10||Top 26 -> 4 Ex/Class
dataset='static_helium_nanodroplets'
tuner = "/scratch/dguthruf/shn/"
#Train on the top most frequent classes
channels = 1
mode = "partial"
variant = "B7"
augmenter_from="nux"
datatype="test"
train_method = "high_balance"
variant_dict = { "B0": 224,
                 "B1": 240,
                 "B2": 260,
                 "B3": 300,
                 "B4": 380,
                 "B5": 456,
                 "B6": 528,
                 "B7": 600}
img_size = variant_dict[variant]
name = "SSB7LR00001"
log_dir = os.path.join(tuner,name)

#Examples per class per batch for train and val
#Defines batchsize and is also dependent on "top" parameter
EXAMPLES_PER_CLASS_TRAIN = 2
EXAMPLES_PER_CLASS_VAL = 2

max_querries_per_class = 100
max_targets_per_class = 100


"""
-------------------------------------------------------------------------------------------------------------------
----------------------------------------------Define Modell Parameters---------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
learning_rate=0.00001
epochs=30
#Used for Circleloss only
gamma = 256
#Final dimension of feature vector
embedding_size=256
#Distance used for Multisimilarity loss and Model
distance='cosine'
#EarlyStopping Callback
monitor='val_loss'
patience = 4


"""
-------------------------------------------------------------------------------------------------------------------
----------------------------------------------------Prepare Data---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
if augmenter_from =="tfsim":
    augmenter = AutoAugment(augmentation_name="reduced_cifar10")
    # augmenter = RandomErasing(probability=0.4)
    # augmenter = RandAugment(num_layers=2,magnitude=5)
    # augmenter = SimCLRAugmenter(img_size,img_size)

    @tf.function()
    def process(img):
        img = tf.image.grayscale_to_rgb(img)
        # img = tf.keras.layers.Rescaling(1/255)(img)
        img = augmenter.distort(img)
        # img = tf.keras.layers.Rescaling(255)(img)
        img = tf.image.rgb_to_grayscale(img)
        return img


    def loader(x, y, *args):
        imgs = tf.stack(x)
        imgs = tf.map_fn(process, imgs, parallel_iterations=cpu_count(), dtype="float32")
        return imgs, y
elif augmenter_from=="nux":

    _augmentation = {"brightness": 0.1, "jitter": 0.3,
                     "scale": (0.5, 1.0), "fill_scale": (0.05, 0.15),
                     "fill_ratio": (0.5, 1.5)}

    nux_augmenter = gpu_augmenter(in_shape=(img_size, img_size, 1), ratio=(3 / 4, 4 / 3), strength=0.3, fill_value=0.,
                                  **_augmentation, model="efficient")
    def loader(x, y, *args):
        imgs = tf.stack(x)
        imgs = tf.keras.layers.Rescaling(1 / 255)(imgs)
        imgs = nux_augmenter(imgs)
        return imgs, y
elif augmenter_from=="MySim":
    my_sim_augmenter=MySim(min_area=0.75,brightness=0.3,jitter=0.1)
    def loader(x, y, *args):
        imgs = tf.stack(x)
        imgs = tf.keras.layers.Rescaling(1/255)(imgs)
        imgs = my_sim_augmenter(imgs)
        imgs = tf.keras.layers.Rescaling(255)(imgs)

        return imgs, y


X_train,X_val,y_train,y_val,train_class_list,class_mappings = get_data(training_with_top_n_frequency_classes=training_with_top_n_frequency_classes,
                                                                       img_size=img_size,
                                                                       train_method=train_method,
                                                                       )

num_classes = len([*Counter(y_val).keys()])
num_train_classes = len(train_class_list)

#Steps per epoch choosen such, that in one epoch whole data is run
STEPS_PER_EPOCH_TRAIN = len(y_train) // (num_train_classes * EXAMPLES_PER_CLASS_TRAIN)
STEPS_PER_EPOCH_VAL = len(y_val) // (num_train_classes * EXAMPLES_PER_CLASS_VAL)

def get_querie_data(class_mappings,train_method,datatype,examples_per_class):
    # Get max examples_per_class examples per class by getting the needed indexes
    queries_index = []
    train_class_list = [*Counter(class_mappings[train_method]["Y_"+datatype]).keys()]
    train_class_list.sort()
    num_classes = len(train_class_list)
    for class_y in range(num_classes):
        c_index = [i for i, y in enumerate(class_mappings[train_method]["Y_"+datatype]) if y == class_y]
        queries_index.extend(c_index[0:examples_per_class])
    queries_x = class_mappings[train_method]["X_"+datatype][queries_index]
    return queries_x,queries_index

X_train,Y_train = get_querie_data(class_mappings=class_mappings,train_method=train_method,datatype=datatype,examples_per_class=10)
#Perpare train and val data with MultishotMemorySampler from tensorflow_similarity
#With that it is guaranteed that each batch contains at least 2 samples per class per batch
#If EXAMPLES_PER_CLASS >2 we have EXAMPLES_PER_CLASS per Batch Batchsize = EXAMPLES_PER_CLASS * NumClasses
aug_sampler = SingleShotMemorySampler(X_train,
                                 examples_per_batch=10,
                                 num_augmentations_per_example=4,
                                 augmenter=loader,
                                 )




"""
-------------------------------------------------------------------------------------------------------------------
--------------------------------------------------Prepare Modell---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
    learning_rate,
    decay_steps=500,
    decay_rate=0.75,
    staircase=True)

#Define tensorflow_similarity loss : MultisimilarityLoss
optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
# loss = tfsim.losses.MultiSimilarityLoss(distance=distance)
gamma = 256
# loss = tfsim.losses.CircleLoss(gamma=gamma)
loss = tfsim.losses.MultiSimilarityLoss(distance="cosine")
# loss = tfsim.losses.TripletLoss(distance="L2", positive_mining_strategy="hard",
#                                 negative_mining_strategy="semi-hard")
# #Get AugmentationLayers
# if augmenter=="MySim":
#     augmentation_layers = MySim(min_area=min_area,
#                                 brightness=brightness,
#                                 jitter=jitter)

# Create a MirroredStrategy.
strategy = tf.distribute.MirroredStrategy()
print('Number of devices: {}'.format(strategy.num_replicas_in_sync))

# Open a strategy scope.
# with strategy.scope():
model = EfficientNetSim((img_size, img_size, channels),
                        embedding_size=embedding_size,
                        variant=variant,
                        trainable=mode,
                        augmentation=None,
                        )
model.compile(optimizer,
              loss=loss,
              distance=distance)
model.summary()

# #Define targets whic are needed for indexing and later classifiction take 50 examples per class if possible
# targets_index = []
# for c in range(num_classes):
#     c_index = [i for i, y in enumerate(y_train) if y == c]
#     targets_index.extend(c_index[0:max_targets_per_class])
# targets_x,targets_y = X_train[targets_index],y_train[targets_index]
# # targets_x,targets_y = select_examples(X_train,y_train,num_examples_per_class=40)
#
# #Define Callbacks which are passed to the model
# #Queries are based on whole val dataset needed to get accuracy of model at end of epoch with Callbacks
# queries_x = X_val
# queries_y = y_val

# if num_train_classes ==num_classes:
#     #Needed if we train on all avaialbe classes
#     tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y,
#                        k=1,
#                        metrics=['f1score', 'binary_accuracy'])


# else:
#     #Needed we do not train on all availabe classses, splits accuracy into known and unknown accuracy
#     tsc = SplitValidationLoss(queries_x, queries_y, targets_x, targets_y,
#                               metrics=['binary_accuracy'],
#                               known_classes=tf.constant(train_class_list),
#                               k=1)
from keras.callbacks import ModelCheckpoint, EarlyStopping, LearningRateScheduler, ReduceLROnPlateau

early = EarlyStopping(monitor="val_loss", mode="min", patience=5, verbose=1)
redonplat = ReduceLROnPlateau(monitor="val_loss", mode="min", patience=3, verbose=2)
#Needed to log everthing in tensorboard
tbc = TensorBoard(log_dir=log_dir + '/tb')

class ClearMemory(Callback):
    def on_epoch_end(self, epoch, logs=None):
        gc.collect()
        k.clear_session()

callbacks = [tbc,early,redonplat,ClearMemory()]

#Start training
history = model.fit(aug_sampler,
                    epochs=epochs,
                    validation_data=(X_val,y_val),
                    callbacks=callbacks,
                    steps_per_epoch=50,
)


"""
-------------------------------------------------------------------------------------------------------------------
--------------------------------------------------Post Training----------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
# # Calibrate Model
# model.reset_index()
# model.index(targets_x, targets_y, data=targets_x)
# calibration = model.calibrate(
#     queries_x,
#     queries_y,
#     calibration_metric="f1",
#     matcher="match_nearest",
#     extra_metrics=["precision", "recall", "binary_accuracy"],
#     verbose=1,
# )


# #Save model
# save_path = log_dir+'/model'
# model.save(save_path)
# """
# -------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------Visuallization---------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------
# """
# #Define examples that shall be used for ConfusionMatrix and Projection
# tensorboard_visualization(model, calibration, class_mappings, log_dir,train_method=train_method, datatype=datatype)
