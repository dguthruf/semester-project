import os
import tensorflow as tf
import numpy as np
from collections import Counter
import matplotlib.pyplot as plt
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"]="cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)

del_all_flags(tf.compat.v1.flags.FLAGS)
from tensorflow.keras.callbacks import TensorBoard
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
import tensorflow_similarity as tfsim
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from imgaug import augmenters as iaa
from multiprocessing import cpu_count
from tensorflow_similarity.samplers import MultiShotMemorySampler  # sample data
import keras_tuner as kt
from tensorflow_similarity.samplers import select_examples  # select n example per class
from my_operations import get_augmentation_layers,MySim,TfsimRand
from visualization import visualize
def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)

del_all_flags(tf.compat.v1.flags.FLAGS)
from nn import operations

def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)
del_all_flags(tf.compat.v1.flags.FLAGS)
import squirrel as sq
import random
tfsim.utils.tf_cap_memory()


def to_single_label(y,pos = []):
    labels = []
    for encoded in y:
        temp = np.where(encoded)
        if len(pos) == 0:
            pos.append(temp[0])
            label = 0
        else:
            for j, po in enumerate(pos):
                if np.array_equal(temp[0], po):
                    label = j
                    break
            if not np.array_equal(temp[0], po):
                pos.append(temp[0])
                label = j

        labels.append(label)
    return np.array(labels),pos

def resize(img,IMG_SIZE=224,channels=1):
    with tf.device("/cpu:0"):
        img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
        if channels==3:
            img = tf.image.grayscale_to_rgb(img)
        return img.numpy()

dataset='balanced_static_helium_nanodroplets'
tuner = "/scratch/dguthruf/tuner/bshn"
name = 'Partial'
main_classes = ["Round","Elliptical","Bent","Asymmetric","DoubleRings","Streak"]#not the order as in info
log_dir = os.path.join(tuner,name)
channels = 1
augmenter = "MySim"
data, info = sq.load(name=dataset,
                            split=["train"],
                            with_info=True,
                            batch_size=-1)
data = sq.as_numpy(data)
img_size = 224
data_x,data_y =data[0]["image"], data[0]["label"]
num_classes=6
test_examples_per_class = 50
data_x = resize(data_x,channels=channels)
test_index = []
train_index=[]
for c in range(num_classes):
    c_index = [i for i, x in enumerate(data_y) if x == c]
    test_index.extend(c_index[0:test_examples_per_class])
    train_index.extend(c_index[test_examples_per_class:-1])
random.shuffle(test_index)
random.shuffle(train_index)
test_x,test_y = data_x[test_index],data_y[test_index]
train_x,train_y =data_x[train_index],data_y[train_index]

EXAMPLES_PER_CLASS =40
NUM_CLASSES_TRAIN = 6
STEPS_PER_EPOCH =len(train_y)//(NUM_CLASSES_TRAIN*EXAMPLES_PER_CLASS)
class_list = random.sample(population=range(num_classes), k=num_classes)
train_ds = MultiShotMemorySampler(train_x, train_y,
                                 classes_per_batch=NUM_CLASSES_TRAIN,
                                 examples_per_class_per_batch=EXAMPLES_PER_CLASS,
                                 class_list=class_list,
                                 steps_per_epoch=STEPS_PER_EPOCH)
EXAMPLES_PER_CLASS_TEST =4
NUM_CLASSES_TEST =6
STEPS_PER_EPOCH_VAL =len(test_y)//(NUM_CLASSES_TEST*EXAMPLES_PER_CLASS_TEST)

val_ds = MultiShotMemorySampler(test_x, test_y,
                                 classes_per_batch=NUM_CLASSES_TEST,
                                 examples_per_class_per_batch=EXAMPLES_PER_CLASS_TEST,
                                 steps_per_epoch=STEPS_PER_EPOCH_VAL)



if channels ==3:
    augmentation_layers = tf.keras.Sequential([
        keras.layers.Rescaling(255),
        #MySim(min_area=0.75, brightness=0.3, jitter=0.1),
        TfsimRand(num_layers=2,magnitude=4),
    ])
elif channels==1:
    if augmenter =="Nux":
        nux_augmenter = operations.gpu_augmenter(in_shape=(224, 224, 1),
                                                       brightness=0.2,
                                                       jitter=0.1,
                                                       scale=(0.5, 1.0),
                                                       ratio=(3 / 4, 4 / 3),
                                                       fill_scale=(0.05, 0.15),
                                                       fill_ratio=(0.5, 1.5),
                                                       fill_value=0.,
                                                       strength=0.25,
                                                       )
    elif augmenter =="MySim":
        my_augmenter = MySim(0.75,0.5,0.2,target_size=224)

    augmentation_layers = tf.keras.Sequential([
        my_augmenter,
    ])


learning_rate=0.0001
gamma = 256
embedding_size=256
optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
loss = tfsim.losses.MultiSimilarityLoss(distance="Cosine")

model = EfficientNetSim((224, 224, channels), embedding_size=embedding_size, variant="B0",trainable="partial",
                        augmentation=augmentation_layers)
model.compile(optimizer, loss=loss)
model.summary()
val_steps = 50
epochs=200

queries_x = test_x
queries_y = test_y
targets_x,targets_y = select_examples(train_ds._x,train_ds._y,num_examples_per_class=40)


tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=1,
                   metrics=['f1score', 'binary_accuracy'],
                   #tb_logdir=log_dir + '/tsc'  # uncomment if you want to track in tensorboard
                   )
tbc = TensorBoard(log_dir=log_dir + '/tb')
esc = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)
callbacks = [tsc,esc,tbc]

history = model.fit(
    train_ds, epochs=epochs, validation_data=val_ds, callbacks=callbacks
)

# Calibrate Model
model.reset_index()
model.index(targets_x, targets_y, data=targets_x)
calibration = model.calibrate(
    queries_x,
    queries_y,
    calibration_metric="f1",
    matcher="match_nearest",
    extra_metrics=["precision", "recall", "binary_accuracy"],
    verbose=1,
)

visualize(model,calibration,main_classes,queries_x,queries_y,log_dir)