import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "3"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
import keras_tuner as kt
from tensorflow.keras.callbacks import TensorBoard
from collections import Counter
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
import tensorflow_similarity as tfsim
FLAGS = tf.compat.v1.flags.FLAGS
FLAGS.__delattr__("wikipedia_auto_select_flume_mode")
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.samplers import MultiShotMemorySampler  # sample data
from shn_operations import get_data,MySim
from augmenters import RandAugment,AutoAugment,SimCLRAugmenter,RandomErasing
from multiprocessing import cpu_count
from sklearn.model_selection import train_test_split
from nn import gpu_augmenter
import gc
from tensorflow.keras import backend as k
from tensorflow.keras.callbacks import Callback
tfsim.utils.tf_cap_memory()

"""
-------------------------------------------------------------------------------------------------------------------
-----------------------------------------------Define Overall Things-----------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
dataset='static_helium_nanodroplets'
folder_path = "/scratch/dguthruf/tuner/shn_final/"
#Train on the top most frequent classes
channels = 1
train_method = "high_balance"
folder_name = "AugmentationsB0"
log_dir = os.path.join(folder_path, folder_name)


#resize to image size of max backbone architecture used
img_size = 224
top = 10

#Examples per class per batch for train and val
#Defines batchsize and is also dependent on "top" parameter
examples_per_class_per_batch = 20



"""
-------------------------------------------------------------------------------------------------------------------
----------------------------------------------------Prepare Data---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
X_train,X_val,y_train,y_val,train_class_list,class_mappings = get_data(
                                                                       img_size=img_size,
                                                                       train_method=train_method,
                                                                       )

num_classes = len([*Counter(y_val).keys()])
number_of_classes = len(train_class_list)

#Steps per epoch choosen such, that in one epoch whole data is run
steps__per_epoch = len(y_train) // (number_of_classes * examples_per_class_per_batch)

#Perpare train and val data with MultishotMemorySampler from tensorflow_similarity
#With that it is guaranteed that each batch contains at least 2 samples per class per batch
#If EXAMPLES_PER_CLASS >2 we have EXAMPLES_PER_CLASS per Batch Batchsize = EXAMPLES_PER_CLASS * NumClasses
train_ds = MultiShotMemorySampler(X_train, y_train,
                                  classes_per_batch=number_of_classes,
                                  examples_per_class_per_batch=examples_per_class_per_batch,
                                  class_list=train_class_list,
                                  steps_per_epoch=steps__per_epoch,
                                  )




class MyHyperModel(kt.HyperModel):
    def build(self, hp):

        efficientnet_variant = hp.Choice("efficientnet_variant",
                                         ["B0", "B1","B2","B3","B4", "B5","B6", "B7"],
                                         default="B0")

        trainable = hp.Choice("trainable", ["frozen", "partial","full"], default='partial')

        optimizer = hp.Choice("optimizer",
                              ["adam", "sgd", "adamax", "adadelta"],
                              default='adam')

        learning_rate = hp.Choice("lr",
                                  values=[0.0005, 0.0001, 0.00005, 0.00001, 0.000005, 0.000001],
                                  default=0.00005)

        embedding_size = hp.Choice("emb_size",
                                   values=[128, 192, 256, 320, 384, 448, 512, 1024],
                                   default=512)

        distance = hp.Choice("distance",
                             ["Cosine", "L2", "L1"],
                             default='Cosine')

        loss = hp.Choice("loss",
                         ["circle", "multi", "triplet"],
                         default='multi')

        # decay_steps = hp.Choice("decay_steps",
        #                       values=[100,300,500,1000,2000,5000],
        #                       default=500)
        #
        # decay_rate = hp.Choice("decay_rate",
        #                       values=[0.75,0.8,0.85,0.96],
        #                       default=0.75)




        #Hyperparameters for different augmenters

        #Check which efficientnet_variant is applied to define needed image size
        if efficientnet_variant == 'B0':
            size = 224
        elif efficientnet_variant == "B1":
            size = 240
        elif efficientnet_variant == "B2":
            size = 260
        elif efficientnet_variant == "B3":
            size = 300
        elif efficientnet_variant == "B4":
            size = 380
        elif efficientnet_variant == "B5":
            size = 456
        elif efficientnet_variant == "B6":
            size = 528
        elif efficientnet_variant == "B7":
            size = 600

        # lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
        #     learning_rate,
        #     decay_steps=decay_steps,
        #     decay_rate=decay_rate,
        #     staircase=True)


        if optimizer == 'adam':
            optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
        elif optimizer == 'sgd':
            optimizer = keras.optimizers.SGD(learning_rate=learning_rate)
        elif optimizer == 'adamax':
            optimizer = keras.optimizers.Adamax(learning_rate=learning_rate)
        elif optimizer == 'adadelta':
            optimizer = keras.optimizers.Adadelta(learning_rate=learning_rate)

        if loss == 'circle':
            with hp.conditional_scope("loss", ['circle']):
                gamma = hp.Choice("gamma",
                                  values=[64, 128, 256, 512, 1024],
                                  default=128)
                loss = tfsim.losses.CircleLoss(gamma=gamma)
        elif loss == 'multi':
            with hp.conditional_scope("loss", ['multi']):
                distance_m = hp.Choice("distance_m",
                                       ["Cosine", "L2", "L1"],
                                       default='Cosine')
                loss = tfsim.losses.MultiSimilarityLoss(distance=distance_m)
        elif loss == 'triplet':
            with hp.conditional_scope("loss", ['triplet']):
                p_mining = hp.Choice("p_mining",
                                     ["hard", "easy"],
                                     default="hard")
                n_mining = hp.Choice("n_mining",
                                     ["hard", "semi-hard", "easy"],
                                     default="semi-hard")
                distance_t = hp.Choice("distance_t",
                                       ["Cosine", "L2", "L1"],
                                       default='Cosine')
                loss = tfsim.losses.TripletLoss(distance=distance_t, positive_mining_strategy=p_mining,
                                                negative_mining_strategy=n_mining)

        model = EfficientNetSim((size, size, 1), embedding_size=embedding_size, variant=efficientnet_variant, trainable=trainable,
                                augmentation=None)
        model.compile(optimizer, loss=loss, distance=distance)

        return model

    def fit(self, hp, model, train_ds, val_ds, callbacks, **kwargs):

        (X_val, y_val) = val_ds

        efficientnet_variant = hp.get("efficientnet_variant")
        if efficientnet_variant == 'B0':
            size = 224
        elif efficientnet_variant == "B1":
            size = 240
        elif efficientnet_variant == "B2":
            size = 260
        elif efficientnet_variant == "B3":
            size = 300
        elif efficientnet_variant == "B4":
            size = 380
        elif efficientnet_variant == "B5":
            size = 456
        elif efficientnet_variant == "B6":
            size = 528
        elif efficientnet_variant == "B7":
            size = 600

        #resize function which resize to the appropiate image size for the choosen network structure
        def resize(img, label):
            with tf.device("/cpu:0"):
                img = tf.cast(img, dtype="int32")
                img = tf.image.resize(img, size, size)
                return img, label
        #resize train_ds sampler
        train_ds.preprocess_fn = resize
        #resize validation data
        X_val = tf.image.resize(X_val, (size, size))

        #resize on epoch end classification data
        tsc = callbacks[0]
        tsc.targets = tf.image.resize(tsc.targets, (size, size))
        tsc.queries = tf.image.resize(tsc.queries, (size, size))
        callbacks[0] = tsc

        augmenter_type = hp.Choice("augmenter_type",
                                   values=["None", "RandAugment", "MySimCLR", "AutoAugment", "SimCLR", "NuxSimCLR","RandomEarising"],
                                   default="None")
        if augmenter_type == 'RandAugment':
            with hp.conditional_scope("augmenter_type", ['RandAugment']):
                num_layers = hp.Choice("num_layers",
                                       values=[0, 1, 2, 3, 4, 5, ],
                                       default=2)
                magnitude = hp.Choice("magnitude",
                                      values=[0, 2, 4, 6, 8, 10, 12],
                                      default=10)
                augmenter = RandAugment(num_layers=num_layers, magnitude=magnitude)

        elif augmenter_type == 'AutoAugment':
            with hp.conditional_scope("augmenter_type", ['AutoAugment']):
                autoaugment = hp.Choice("autoaugment",
                                        values=["v0", "reduced_cifar10", "svhn", "reduced_imagenet"],
                                        default="v0")
                augmenter = AutoAugment(augmentation_name=autoaugment)

        elif augmenter_type == 'RandomEarising':
            with hp.conditional_scope("augmenter_type", ['RandomEarising']):
                random_erasing_probability = hp.Choice("random_erasing_probability",
                                                       values=[0.0, 0.1, 0.2, 0.3, 0.4, 0.5],
                                                       default=0.3)
                augmenter = RandomErasing(probability=random_erasing_probability)


        elif augmenter_type == 'MySimCLR':
            with hp.conditional_scope("augmenter_type", ['MySimCLR']):
                min_area = hp.Choice("min_area",
                                     values=[0.5, 0.7, 0.9],
                                     default=0.7)
                brightness = hp.Choice("brightness",
                                       values=[0.1, 0.3, 0.5],
                                       default=0.3)
                jitter = hp.Choice("jitter",
                                   values=[0.1, 0.1, 0.3],
                                   default=0.1)
                augmenter = MySim(min_area=min_area, brightness=brightness, jitter=jitter)


        elif augmenter_type == 'SimCLR':
            with hp.conditional_scope("augmenter_type", ['SimCLR']):
                simclr = hp.Choice("simclr",
                                   values=["v2", "v1"],
                                   default="v1")
                jitter_stength = hp.Choice("jitter_stength",
                                           values=[0.1, 0.3, 0.8],
                                           default=0.1)
                augmenter = SimCLRAugmenter(size, size,version=simclr,color_distort=False,jitter_stength=jitter_stength)

        elif augmenter_type == 'NuxSimCLR':
            with hp.conditional_scope("augmenter_type", ['NuxSimCLR']):
                nux_strength = hp.Choice("nux_strength",
                                 values=[0.1, 0.2, 0.3, 0.4, 0.5],
                                 default=0.3)

                nux_brightness = hp.Choice("nux_brightness",
                                           values=[0.1, 0.3, 0.5],
                                           default=0.3)
                nux_jitter = hp.Choice("nux_jitter",
                                       values=[0.1, 0.3],
                                       default=0.1)
                _augmentation = {"brightness": nux_brightness, "jitter":nux_jitter,
                                 "scale": (0.5, 1.0), "fill_scale": (0.05, 0.15),
                                 "fill_ratio": (0.5, 1.5)}

                augmenter = gpu_augmenter(in_shape=(size, size, 1), ratio=(3 / 4, 4 / 3), strength=nux_strength,
                                              fill_value=0.,
                                              **_augmentation)

        if augmenter_type=="RandAugment" or augmenter_type=="RandomEarising" or augmenter_type=="AutoAugment" or augmenter_type=="SimCLR":
            @tf.function()
            def process(img):
                img = tf.image.grayscale_to_rgb(img)
                if augmenter_type=="SimCLR":
                    img = tf.keras.layers.Rescaling(1/255)(img)
                img = augmenter.distort(img)
                if augmenter_type=="SimCLR":
                    img = tf.keras.layers.Rescaling(255)(img)
                img = tf.image.rgb_to_grayscale(img)
                return img

            def loader(x, y, *args):
                imgs = tf.stack(x)
                imgs = tf.map_fn(process, imgs, parallel_iterations=cpu_count(), dtype="float32")
                return imgs, y

        elif augmenter_type=="MySimCLR" or augmenter_type=="NuxSimCLR":
            def loader(x, y, *args):
                imgs = tf.stack(x)
                imgs = tf.keras.layers.Rescaling(1/255)(imgs)
                imgs = augmenter(imgs)
                imgs = tf.keras.layers.Rescaling(255)(imgs)
                return imgs, y

        elif augmenter_type == "None":
            def loader(x, y, *args):
                imgs = x
                return imgs, y

        train_ds.augmenter = loader



        history = model.fit(
            train_ds,
            validation_data=(X_val,y_val),
            callbacks=callbacks,
            **kwargs,
        )

        return {"f1_score": history.history["f1_score"][-1],
                "binary_accuracy": history.history["binary_accuracy"][-1],
                "val_loss":history.history["val_loss"][-1],
                }

class ClearMemory(Callback):
    def on_epoch_end(self, epoch, logs=None):
        gc.collect()
        k.clear_session()

#Define targets whic are needed for indexing and later classifiction take 50 examples per class if possible
targets_index = []
max_targets_per_class=100
for c in range(num_classes):
    c_index = [i for i, y in enumerate(y_train) if y == c]
    targets_index.extend(c_index[0:max_targets_per_class])
targets_x,targets_y = X_train[targets_index],y_train[targets_index]
# targets_x,targets_y = select_examples(X_train,y_train,num_examples_per_class=40)

#Define Callbacks which are passed to the model
#Queries are based on whole val dataset needed to get accuracy of model at end of epoch with Callbacks
queries_x = X_val
queries_y = y_val


tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=1,
                   metrics=['f1_score', 'binary_accuracy'],
                   )
tbc = TensorBoard(log_dir=log_dir + '/tb')
esc = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=3,mode="min", restore_best_weights=True)
callbacks = [esc, tbc, tsc,ClearMemory()]


hp = kt.HyperParameters()
# loss = hp.Choice("loss", ["circle", "multi", "triplet"], default='multi')
# with hp.conditional_scope("loss", ['circle']):
#     gamma = hp.Choice("gamma", values=[16,32,64,128, 256, 512], default=512)
# with hp.conditional_scope("loss", ['multi']):
#     distance_m = hp.Choice("distance_m", ["Cosine", "L2", "L1"], default='Cosine')
# with hp.conditional_scope("loss", ['triplet']):
#     p_mining = hp.Choice("p_mining", ["hard", "easy"], default="hard")
#     n_mining = hp.Choice("n_mining", ["hard", "semi-hard", "easy"], default="semi-hard")
#     distance_t = hp.Choice("distance_t", ["Cosine", "L2", "L1"], default='Cosine')


trainable = hp.Choice("trainable", ["frozen", "partial","full"], default='frozen')
#
# size=img_size
augmenter_type = hp.Choice("augmenter_type",
                           values=["None",
                                   "RandAugment",
                                   "MySimCLR",
                                   "AutoAugment",
                                   "SimCLR",
                                   "NuxSimCLR",
                                   # "RandomEarising",
                                   ],
                           default="None")
with hp.conditional_scope("augmenter_type", ['RandAugment']):
    num_layers = hp.Choice("num_layers",
                           values=[2,3,5],
                           default=2)
    magnitude = hp.Choice("magnitude",
                          values=[2,4,10],
                          default=10)
#
# with hp.conditional_scope("augmenter_type", ['AutoAugment']):
#     autoaugment = hp.Choice("autoaugment",
#                             values=["v0", "reduced_cifar10", "svhn", "reduced_imagenet"],
#                             default="v0")
#
# with hp.conditional_scope("augmenter_type", ['RandomEarising']):
#     random_erasing_probability = hp.Choice("random_erasing_probability",
#                                            values=[0.0,0.1,0.2, 0.3, 0.5],
#                                            default=0.3)
#
#
# with hp.conditional_scope("augmenter_type", ['MySimCLR']):
#     min_area = hp.Choice("min_area",
#                          values=[0.5, 0.7, 0.9],
#                          default=0.7)
#     brightness = hp.Choice("brightness",
#                            values=[0.1, 0.3, 0.5],
#                            default=0.3)
#     jitter = hp.Choice("jitter",
#                        values=[0.1, 0.3, 0.5],
#                        default=0.1)
#
#
# with hp.conditional_scope("augmenter_type", ['SimCLR']):
#     simclr = hp.Choice("simclr",
#                        values=["v2", "v1"],
#                        default="v1")
#     jitter_stength = hp.Choice("jitter_stength",
#                                values=[0.1, 0.3, 0.8],
#                                default=0.1)
#

with hp.conditional_scope("augmenter_type", ['NuxSimCLR']):
    nux_strength = hp.Choice("nux_strength",
                             values=[0.1, 0.5],
                             default=0.1)

    nux_brightness = hp.Choice("nux_brightness",
                           values=[0.1, 0.5],
                           default=0.5)
    nux_jitter = hp.Choice("nux_jitter",
                       values=[0.1, 0.5],
                       default=0.1)




# variant = hp.Choice("variant", ["B0", "B1", "B2", "B3","B4"], default="B0")
# random_erasing_probability = hp.Choice("random_erasing_probability",
#                                        values=[0.0,0.05,0.1, 0.2, 0.3, 0.4, 0.5],
#                                        default=0.3)
# autoaugment = hp.Choice("autoaugment",
#                         values=["No", "v0", "reduced_cifar10", "svhn", "reduced_imagenet"],
#                         default="v0")
# optimizer = hp.Choice("optimizer", ["adam", "sgd", "adamax", "adadelta"], default='adam')
# learning_rate = hp.Choice("lr",
#                           values=[0.001,0.0005,0.0001,0.00005],
#                           default=0.0005)
# decay_steps = hp.Choice("decay_steps",
#                         values=[100, 1000],
#                         default=100)
# decay_rate = hp.Choice("decay_rate",
#                        values=[0.75, 0.96],
#                        default=0.75)
# variant = hp.Choice("variant", ["B0", "B1"], default="B0")
# trainable = hp.Choice("trainable", ["frozen", "partial", "full"], default='partial')
# learning_rate = hp.Choice("lr",
#                           values=[0.0005, 0.00005, 0.000005],
#                           default=0.00005)
# nux_strength = hp.Choice("nux_strength",
#                          values=[0.1, 0.2, 0.5],
#                          default=0.1)

# variant = hp.Choice("variant", ["B0","B1","B2","B3","B4","B5","B6"], default="B6")

# embedding_size = hp.Choice("emb_size", values=[8,16,28,56,128, 256, 512, 1024], default=512)
# distance = hp.Choice("distance", ["Cosine", "L2", "L1"], default='Cosine')
# learning_rate = hp.Choice("lr",
#                           values=[0.0005,0.00005, 0.000005],
#                           default=0.0005)
#
tuner = kt.RandomSearch(
    MyHyperModel(),
    objective=kt.Objective("val_loss", "min"),
    seed=42,
    max_trials=200,
    hyperparameters=hp,
    tune_new_entries=False,
    executions_per_trial=1,
    #distribution_strategy=tf.distribute.MirroredStrategy(),
    overwrite=True,
    directory=log_dir + '/model',
    project_name=folder_name,
)

#
# tuner = kt.Hyperband(
#     MyHyperModel(),
#     objective=kt.Objective("f1_score", "max"),
#     seed=42,
#     max_epochs=25,
#     hyperband_iterations=3,
#     hyperparameters=hp,
#     factor=2,
#     tune_new_entries=False,
#     #distribution_strategy=tf.distribute.MirroredStrategy(),
#     overwrite=True,
#     directory=log_dir + '/model',
#     project_name=folder_name,
# )


# tuner = kt.BayesianOptimization(
#     MyHyperModel(),
#     objective=kt.Objective("val_loss", "min"),
#     seed=42,
#     max_trials=300,
#     hyperparameters=hp,
#     tune_new_entries=False,
#     executions_per_trial=2,
#     #distribution_strategy=tf.distribute.MirroredStrategy(),
#     overwrite=True,
#     directory=log_dir + '/model',
#     project_name=name,
# )



tuner.search_space_summary()
tuner.search(train_ds, epochs=30, val_ds=(X_val, y_val), callbacks=[tsc, tbc, esc])

