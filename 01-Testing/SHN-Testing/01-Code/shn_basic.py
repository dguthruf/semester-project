import os
import tensorflow as tf
import numpy as np
from collections import Counter
import matplotlib.pyplot as plt
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"]="cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)

del_all_flags(tf.compat.v1.flags.FLAGS)
from tensorflow.keras.callbacks import TensorBoard
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
import tensorflow_similarity as tfsim
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from imgaug import augmenters as iaa
from multiprocessing import cpu_count
from tensorflow_similarity.samplers import MultiShotMemorySampler  # sample data
import keras_tuner as kt
from tensorflow_similarity.samplers import select_examples  # select n example per class
from my_operations import get_augmentation_layers,MySim,TfsimRand
from visualization import visualize
def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)

del_all_flags(tf.compat.v1.flags.FLAGS)
from nn import operations

def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)
del_all_flags(tf.compat.v1.flags.FLAGS)
import squirrel as sq
import random
tfsim.utils.tf_cap_memory()

def main_class(y):
    main_classes_dict = {}
    main_classes_dict["Round"]=[0, 2, 34, 37]
    main_classes_dict["Elliptical"]=[1,10,31,40]
    main_classes_dict["Bent"]=[5, 23, 24, 27, 35, 36]
    main_classes_dict["Asymmetric"]=[3, 14, 16, 8]
    main_classes_dict["DoubleRings"]=[7, 9, 15, 17, 19, 22, 39, 41, 42]
    main_classes_dict["Streak"]=[6, 12, 13, 18, 21, 26, 28, 29, 30, 32, 33]
    # main_classes_dict["Layered"]= [25,38]
    # main_classes_dict["Prolate"]=[20]
    main_classes_dict["Empty"]=[4]
    labels = []
    classes = []
    for lab in y:
        for j, key in enumerate([*main_classes_dict.keys()]):
            if lab in main_classes_dict[key]:
                label = key
                clas = j
        labels.append(label)
        classes.append(clas)
    return labels,np.array(classes),main_classes_dict

def to_single_label(y):
    class_full = np.array(["spherical_oblate","round","elliptical","prolate","streak","bent","asymmetric","newtonrings","doublerings","layered","empty"])
    class_short = np.array(["SO","R","E","P","S","B","A","NR","DR","L","EY"])
    labels_full =[]
    labels_short = []
    for single in y:
        single = single.astype(dtype=bool)
        label_f = '-'.join(class_full[single])
        label_s = '-'.join(class_short[single])
        labels_full.append(label_f)
        labels_short.append(label_s)

    keys = np.array([*Counter(labels_short).keys()])
    keys_full = np.array([*Counter(labels_full).keys()])
    values =  np.array([*Counter(labels_short).values()])
    sort_index = np.argsort(-values)
    keys_sorted = keys[sort_index].tolist()
    keys_sorted_full = keys_full[sort_index].tolist()
    label_dict = dict(zip(keys_sorted, range(len(keys_sorted))))
    clss_dict_short = dict(zip(range(len(keys_sorted)), keys_sorted))
    clss_dict_full = dict(zip(range(len(keys_sorted)), keys_sorted_full))

    y = np.array([label_dict[ls] for ls in labels_short])
    return y,clss_dict_short,clss_dict_full


def resize(img,IMG_SIZE=224,channels=1):
    with tf.device("/cpu:0"):
        img = tf.image.resize(img, [IMG_SIZE, IMG_SIZE])
        if channels==3:
            img = tf.image.grayscale_to_rgb(img)
        return img.numpy()

dataset='static_helium_nanodroplets'
tuner = "/scratch/dguthruf/tuner"
name = 'shn/7Classes2'
log_dir = os.path.join(tuner,dataset,name)
channels = 1
augmenter = "MySim"
data, info = sq.load(name=dataset,
                            split=["train"],
                            with_info=True,
                            batch_size=-1)
data = sq.as_numpy(data)
img_size = 224
data_x,data_y =data[0]["image"], data[0]["label"]
data_x = np.expand_dims(data_x,axis=3)
data_y,dict_short,dict_full = to_single_label(data_y)
_,data_y,dict_reduced = main_class(data_y)
main_classes = [*dict_short.values()]
num_classes=len(dict_short)
main_classes = [*dict_reduced.keys()]
num_classes=len(dict_reduced)

test_examples_per_class = 52
data_x = resize(data_x,channels=channels)
test_index = []
train_index=[]
for c in range(num_classes):
    c_index = [i for i, x in enumerate(data_y) if x == c]
    test_index.extend(c_index[0:test_examples_per_class])
    train_index.extend(c_index[test_examples_per_class:-1])
random.shuffle(test_index)
random.shuffle(train_index)
test_x,test_y = data_x[test_index],data_y[test_index]
train_x,train_y =data_x[train_index],data_y[train_index]
TRAIN_CLASSES = [*Counter(train_y).keys()]
TRAIN_CLASSES.sort()
NUM_TRAIN_CLASSES = len(TRAIN_CLASSES)
EXAMPLES_PER_CLASS =2
STEPS_PER_EPOCH =len(train_y)//(NUM_TRAIN_CLASSES*EXAMPLES_PER_CLASS)
class_list = TRAIN_CLASSES
train_ds = MultiShotMemorySampler(train_x, train_y,
                                 classes_per_batch=NUM_TRAIN_CLASSES,
                                 examples_per_class_per_batch=EXAMPLES_PER_CLASS,
                                 class_list=class_list,
                                 steps_per_epoch=STEPS_PER_EPOCH)






if channels ==3:
    augmentation_layers = tf.keras.Sequential([
        keras.layers.Rescaling(1/65536.),
        MySim(min_area=0.75, brightness=0.3, jitter=0.1),
        keras.layers.Rescaling(255),
    ])
elif channels==1:
    if augmenter =="Nux":
        nux_augmenter = operations.gpu_augmenter(in_shape=(224, 224, 1),
                                                       brightness=0.2,
                                                       jitter=0.1,
                                                       scale=(0.5, 1.0),
                                                       ratio=(3 / 4, 4 / 3),
                                                       fill_scale=(0.05, 0.15),
                                                       fill_ratio=(0.5, 1.5),
                                                       fill_value=0.,
                                                       strength=0.25,
                                                       )
    elif augmenter =="MySim":
        my_augmenter = MySim(0.75,0.5,0.2,target_size=224)


    augmentation_layers = tf.keras.Sequential([
        keras.layers.Rescaling(1/ 65536.),
        my_augmenter,
        keras.layers.Rescaling(255),
    ])




learning_rate=0.0001
gamma = 256
embedding_size=256
optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
loss = tfsim.losses.CircleLoss(gamma=gamma)
distance='cosine'

model = EfficientNetSim((224, 224, channels), embedding_size=embedding_size, variant="B0", trainable='partial',
                        augmentation=augmentation_layers)
model.compile(optimizer, loss=loss, distance=distance)
model.summary()
val_steps = 50
epochs=40
# queries_x = test_x[0:20*num_classes]
# queries_y = test_y[0:20*num_classes]
# targets_x = test_x[20*num_classes:-1]
# targets_y = test_y[20*num_classes:-1]
queries_x = test_x
queries_y = test_y


targets_x,targets_y = select_examples(train_x,train_y,num_examples_per_class=test_examples_per_class)
# index_ex_per_class = 20
# queries_x = x[index_ex_per_class:-1]
# queries_y = y[index_ex_per_class:-1]
# targets_x = x[0:index_ex_per_class]
# targets_y = y[0:index_ex_per_class]
# targets_x = test_x
# targets_y = test_y
# x_val, y_val = select_examples(val_ds._x, val_ds._y, num_examples_per_class=total_val_examples_per_class)
# queries_x = x_val[0:num_queries_per_class*NUM_CLASSES]
# queries_y = y_val[0:num_queries_per_class*NUM_CLASSES]
# targets_x = x_val[num_queries_per_class*NUM_CLASSES:-1]
# targets_y = y_val[num_queries_per_class*NUM_CLASSES:-1]
# test_x = x_val
# test_y = y_val


tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=1,
                   metrics=['f1score', 'binary_accuracy'],
                   #tb_logdir=log_dir + '/tsc'  # uncomment if you want to track in tensorboard
                   )
tbc = TensorBoard(log_dir=log_dir + '/tb')
esc = tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=1, patience=4)
val_loss = SplitValidationLoss(queries_x, queries_y, targets_x, targets_y,
                               metrics=['binary_accuracy'], known_classes=tf.constant(class_list),
                               k=1,
                               #tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                               )
callbacks = [tsc,esc,tbc]

history = model.fit(
    train_ds, epochs=epochs, validation_data=(test_x,test_y),
    steps_per_epoch=50, callbacks=callbacks
)

# Calibrate Model
model.reset_index()
model.index(targets_x, targets_y, data=targets_x)
calibration = model.calibrate(
    queries_x,
    queries_y,
    calibration_metric="f1",
    matcher="match_nearest",
    extra_metrics=["precision", "recall", "binary_accuracy"],
    verbose=1,
)

visualize(model,calibration,main_classes,queries_x,queries_y,log_dir)