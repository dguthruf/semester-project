import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"]="cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
from mpl_toolkits import axes_grid1
from matplotlib.patches import Ellipse
from sklearn.metrics import pairwise_distances,calinski_harabasz_score


import os
import io
import squirrel as sq
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import cv2
from tensorboard.plugins import projector
from collections import Counter
from tensorflow import keras
from sklearn.model_selection import train_test_split
import random
import tensorflow_similarity as tfsim
from typing import Mapping, Optional, Sequence, Tuple
from tensorflow_similarity.types import Tensor, Lookup
from matplotlib import pyplot as plt
from mpl_toolkits import axes_grid1
from umap.umap_ import nearest_neighbors
from matplotlib.patches import Ellipse
import umap.umap_ as umap
import umap.plot
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
tfsim.utils.tf_cap_memory()

def plot_to_image(figure):
  """Converts the matplotlib plot specified by 'figure' to a PNG image and
  returns it. The supplied figure is closed and inaccessible after this call."""
  # Save the plot to a PNG in memory.
  buf = io.BytesIO()

  plt.savefig(buf, format='png',bbox_inches='tight',pad_inches=0.2,dpi=200)

  # Closing the figure prevents it from being displayed directly inside
  # the notebook.
  plt.close(figure)
  buf.seek(0)
  # Convert PNG buffer to TF image
  image = tf.image.decode_png(buf.getvalue(), channels=4)
  # Add the batch dimension
  image = tf.expand_dims(image, 0)
  return image

def get_querie_data(class_mappings,train_method,datatype,examples_per_class):
    # Get max examples_per_class examples per class by getting the needed indexes
    queries_index = []
    train_class_list = [*Counter(class_mappings[train_method]["Y_"+datatype]).keys()]
    train_class_list.sort()
    num_classes = len(train_class_list)
    for class_y in range(num_classes):
        c_index = [i for i, y in enumerate(class_mappings[train_method]["Y_"+datatype]) if y == class_y]
        queries_index.extend(c_index[0:examples_per_class])
    queries_x = class_mappings[train_method]["X_"+datatype][queries_index]
    return queries_x,queries_index

def tensorboard_visualization(model, calibration, class_mappings, log_dir, train_method, datatype="train",examples_per_class=100,cmap="tab10",metric="cosine"):
    """
    :param model: model to generate embedding vector
    :param calibration: calibration from tfsim_function, to plot calibration
    :param class_mappings: contains all class_mappings for the diffrent groupings of the multiclasses
    :param log_dir:
    :param train_method: on which grouping of the multiclasses the training was done.
    :return:
    In tensorboard
    confusion matrix
    calibration images
    UMAP
    And neareast neighbours in embedding Space via cosine distance
    projection of embedding space
    """

    #Get balanced data for querrying
    queries_x,queries_y_index = get_querie_data(class_mappings,train_method,datatype,examples_per_class)
    queries_y  = class_mappings[train_method]["Y_"+datatype][queries_y_index]
    labels = [*class_mappings[train_method]["class_mapping_dictionary_to_label_names"].values()]
    #Add unkonw label if sample is not within cluster correspondance boundary
    labels.append('unknown')

    #Plot the calibration results
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))
    x = calibration.thresholds["distance"]

    ax1.plot(x, calibration.thresholds["precision"], label="precision")
    ax1.plot(x, calibration.thresholds["recall"], label="recall")
    ax1.plot(x, calibration.thresholds["f1"], label="f1 score")
    ax1.legend()
    ax1.set_title("Metric evolution as distance increase")
    ax1.set_xlabel("Distance")
    ax1.set_ylim((-0.05, 1.05))

    ax2.plot(calibration.thresholds["recall"], calibration.thresholds["precision"])
    ax2.set_title("Precision recall curve")
    ax2.set_xlabel("Recall")
    ax2.set_ylabel("Precision")
    ax2.set_ylim((-0.05, 1.05))
    metric_image = plot_to_image(fig)

    #Cutpoint from calibration was choosen optimal
    cutpoint = "optimal"

    #Classification via matching and looking what the neareste neighbours classes are
    matches = model.match(queries_x, cutpoint=cutpoint, no_match_label=len(labels)+1)

    #Create Confusion_matrix
    pt = tfsim.visualization.confusion_matrix(
        matches,
        queries_y,
        labels=labels,
        title="Confusion matrix for cutpoint:%s" % cutpoint,
        normalize=True,
        show=False
    )


    fig_size = (40,40)
    #Add plots to tensorboard
    umap_fig = my_umap(model, class_mappings, train_method=train_method, datatype=datatype, metric=metric, cmap=cmap, examples_per_class=300)
    umap_image = plot_to_image(umap_fig)
    # pca_fig = get_pca(model, class_mappings, train_method=train_method, examples_per_class=100, datatype=datatype, cmap=cmap)
    # pca_image=plot_to_image(pca_fig.Figure(figsize=fig_size))
    single_umap_fig = get_umap(model, class_mappings, metric=metric, train_method=train_method, datatype=datatype, cmap=cmap,examples_per_class=100)
    cm_image = plot_to_image(pt.Figure(figsize=fig_size))
    # single_umap_image = plot_to_image(single_umap_fig)

    with tf.summary.create_file_writer(log_dir + '/HParam').as_default():
        tf.summary.image("Confusion Matrix", cm_image, step=1)
        tf.summary.image("Calibrated Metric", metric_image, step=1)
        tf.summary.image("UMAP", umap_image, step=1)
        # tf.summary.image("Single UMAP", single_umap_image, step=1)
        # tf.summary.image("PCA", pca_image, step=1)

    get_viz_neighbours(class_mappings,log_dir,datatype=datatype)

    #Call Projection Function and visuallize in tensorboard
    project_embedding(model,class_mappings,log_dir,datatype=datatype)



def project_embedding(model,class_mappings,log_dir,examples_per_class=500,datatype="train"):
    """
    :param model: model needed for generating embedding vector of each image
    :param class_mappings: dicts for class label corresponance
    :param log_dir:
    :return:
    The Projection in tensorbaord
    """
    targets_index = []
    queries_index =[]
    train_class_list = [*Counter(class_mappings["high_balance"]["Y_"+datatype]).keys()]
    train_class_list.sort()
    num_classes = len(train_class_list)
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(class_mappings["high_balance"]["Y_"+datatype]) if y == c]
        targets_index.extend(c_index[0:examples_per_class])

        del c_index
    targets_x, targets_y_m = class_mappings["high_balance"]["X_"+datatype][targets_index], class_mappings["high_balance"]["Y_"+datatype][targets_index]
    targets_y_s = class_mappings["abbreviated"]["Y_"+datatype][targets_index]
    targets_y_f = class_mappings["written_out"]["Y_"+datatype][targets_index]
    targets_y_o = class_mappings["julian"]["Y_"+datatype][targets_index]
    targets_y_t = class_mappings["prolate_oblate"]["Y_"+datatype][targets_index]

    #maximal image size allowed for visuallizing in tensorboard
    size = int(7656/(np.sqrt(len(targets_y_m)+1)))
    img_data = []
    for img in targets_x:
        input_img_resize=cv2.resize(img,(size,size))
        img_data.append(input_img_resize)

    #Log dir of projection
    dir = 'tb/train'
    logdir = os.path.join(log_dir, dir)

    #genearte metadatafile needed to assign each dot /image in embedding space a lable
    metadata_file = open(os.path.join(logdir, 'metadata.tsv'), 'a+')

    metadata_file.write('written_outLabel\tabbreviatedLabel\tAllInt\thigh_balanceLabel\thigh_balanceInt\tprolate_oblateLabel\tprolate_oblateInt\tjulianLabel\tjulianInt\n')
    for i in range(len(targets_y_m)):
        vy_m = targets_y_m[i]
        vy_t = targets_y_t[i]
        vy_o = targets_y_o[i]

        vy_f = targets_y_f[i]
        vy_s = targets_y_s[i]

        cm_f = class_mappings["written_out"]["class_mapping_dictionary_to_label_names"][vy_f]
        cm_s = class_mappings["abbreviated"]["class_mapping_dictionary_to_label_names"][vy_s]
        cm_m = class_mappings["high_balance"]["class_mapping_dictionary_to_label_names"][vy_m]
        cm_t = class_mappings["prolate_oblate"]["class_mapping_dictionary_to_label_names"][vy_t]
        cm_o = class_mappings["julian"]["class_mapping_dictionary_to_label_names"][vy_o]

        metadata_file.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(cm_f,cm_s,vy_f,cm_m,vy_m,cm_t,vy_t,cm_o,vy_o))


    #generate sprites, for visuallization in tensorboard
    sprite = images_to_sprite(np.array(img_data))
    cv2.imwrite(os.path.join(logdir, 'sprite.png'), sprite)

    emb = model.predict(targets_x)
    # Save the weights we want to analyze as a variable.
    # The weights need to have the shape (Number of sample, Total Dimensions)
    # Hence why we flatten the Tensor
    weights = tf.Variable(tf.reshape(emb,(targets_x.shape[0],-1)), name="emb")
    # Create a checkpoint from embedding, the filename and key are the
    # name of the tensor.
    checkpoint = tf.train.Checkpoint(emb=weights)
    checkpoint.save(os.path.join(logdir, "embedding.ckpt"))

    # Set up config.
    config = projector.ProjectorConfig()
    embedding = config.embeddings.add()
    embedding.tensor_name = "emb/.ATTRIBUTES/VARIABLE_VALUE"
    embedding.metadata_path = 'metadata.tsv'
    embedding.sprite.image_path = 'sprite.png'
    embedding.sprite.single_image_dim.extend([size, size]) # image size = 28x28
    # The name of the tensor will be suffixed by `/.ATTRIBUTES/VARIABLE_VALUE`.
    projector.visualize_embeddings(logdir, config)


def images_to_sprite(data):
    """Creates the sprite image along with any necessary padding

    Args:
      data: NxHxW[x3] tensor containing the images.

    Returns:
      data: Properly shaped HxWx3 image with any necessary padding.
    """
    if len(data.shape) == 3:
        data = np.tile(data[..., np.newaxis], (1, 1, 1, 3))
    data = data.astype(np.float32)
    min = np.min(data.reshape((data.shape[0], -1)), axis=1)
    data = (data.transpose(1, 2, 3, 0) - min).transpose(3, 0, 1, 2)
    max = np.max(data.reshape((data.shape[0], -1)), axis=1)
    data = (data.transpose(1, 2, 3, 0) / max).transpose(3, 0, 1, 2)
    # Inverting the colors seems to look better for MNIST
    # data = 1 - data

    n = int(np.ceil(np.sqrt(data.shape[0])))
    padding = ((0, n ** 2 - data.shape[0]), (0, 0),
               (0, 0)) + ((0, 0),) * (data.ndim - 3)
    data = np.pad(data, padding, mode='constant',
                  constant_values=0)
    # Tile the individual thumbnails into an image.
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3)
                                                           + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
    data = (data * 255).astype(np.uint8)
    return data

def plot_25(x, y):
    """Plot the first 25 images."""
    num_cols = num_rows = 5
    fig = plt.figure(figsize=(6.0, 6.0))
    grid = axes_grid1.ImageGrid(
        fig=fig,
        rect=111,
        nrows_ncols=(num_cols, num_rows),
        axes_pad=0.1,
    )
    for ax, im, label in zip(grid, x, y):
        max_val = float(tf.math.reduce_max(im))
        ax.imshow(im / max_val)
        ax.axis("off")

def get_viz_neighbours(class_mappings, log_dir, train_method="high_balance", examples_per_class=100, datatype="train"):
    targets_index = []
    queries_index =[]
    train_class_list = [*Counter(class_mappings[train_method]["Y_" + datatype]).keys()]
    train_class_list.sort()
    num_classes = len(train_class_list)
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(class_mappings[train_method]["Y_" + datatype]) if y == c]
        queries_index.append(c_index[0])
        targets_index.extend(c_index[1:examples_per_class])

        del c_index
    targets_x, targets_y_high_balance = class_mappings[train_method]["X_" + datatype][targets_index], class_mappings[train_method]["Y_" + datatype][targets_index]
    targets_y_multi = class_mappings["abbreviated"]["Y_"+datatype][targets_index]

    num_neighbors = 6
    # select
    x_display = class_mappings[train_method]["X_" + datatype][queries_index]
    y_display_high_balance = class_mappings[train_method]["Y_" + datatype][queries_index]
    y_display_multi = class_mappings["abbreviated"]["Y_"+datatype][queries_index]

    model_2 = tf.keras.models.load_model(log_dir+"/model")
    model_2.reset_index()
    model_2.index(targets_x, targets_y_multi, data=targets_x)
    nns_multi = model_2.lookup(x_display, k=num_neighbors)

    # display
    viz_neigh_images = []
    for idx in np.argsort(y_display_high_balance):
        pt,viz_name = viz_neighbours(x_display[idx],
                                     y_display_high_balance[idx],
                                     y_display_multi[idx],
                                     nns_multi[idx],
                                     Mapping_dic=class_mappings[train_method]["class_mapping_dictionary_to_multiclasses"],
                                     high_balance_mapping=class_mappings[train_method]["class_mapping_dictionary_to_label_names"],
                                     multi_mapping=class_mappings["abbreviated"]["class_mapping_dictionary_to_label_names"],
                                     fig_size=(30, 15),
                                     show=False)
        viz_image=plot_to_image(pt.Figure(figsize=(30, 15)))
        viz_neigh_images.append(viz_image)
        with tf.summary.create_file_writer(log_dir + '/HParam').as_default():
            tf.summary.image(viz_name, viz_image, step=1)
    return viz_neigh_images

def viz_neighbours(example: Tensor,
                   example_class_high_balance: int,
                   example_class_multi: int,
                   neighbors_multi: Sequence[Lookup],
                   Mapping_dic: Optional[Mapping[int, str]] = None,
                   high_balance_mapping: Optional[Mapping[int, str]] = None,
                   multi_mapping: Optional[Mapping[int, str]] = None,
                   fig_size: Tuple[int, int] = (30, 6),
                   show: bool = True):
    """Display images nearest neighboors
    Args:
        example: The data used as query input.
        example_class: The class of the data used as query
        neighbors: The list of neighbors returned by the lookup()
        class_mapping: Mapping from class numerical ids to a class name. If not
        set, the plot will display the class numerical id instead.
        Defaults to None.
        fig_size: Size of the figure. Defaults to (24, 4).
        cmap: Default color scheme for black and white images e.g mnist.
        Defaults to 'viridis'.
        show: If the plot is going to be shown or not. Defaults to True.
    """
    num_cols = len(neighbors_multi) + 1
    fig, axs = plt.subplots(1, num_cols, figsize=fig_size)

    axs[0].imshow(example)
    axs[0].set_xticks([])
    axs[0].set_yticks([])
    high_balance_label = _get_class_label(example_class_high_balance, high_balance_mapping)
    multi_label = _get_class_label(example_class_multi, multi_mapping)
    axs[0].set_title(high_balance_label)
    axs[0].set_xlabel(multi_label)
    for ax,nbg_multi in zip(axs[1:],neighbors_multi):

        multi_val = _get_class_label(nbg_multi.label, multi_mapping)
        high_balance_val = [c for c in Mapping_dic if nbg_multi.label in Mapping_dic[c]][0]
        top = f"{high_balance_val} - {nbg_multi.distance:.5f}"
        bot = f"{multi_val}"
        ax.imshow(nbg_multi.data)
        ax.set_title(top)
        ax.set_xlabel(bot)
        ax.set_xticks([])
        ax.set_yticks([])
    if show:
        plt.show()
    else:
        return plt,high_balance_label


def _get_class_label(example_class, class_mapping):
    if example_class is None:
        return 'No Label'

    if class_mapping is None:
        return str(example_class)

    class_label = class_mapping.get(example_class)
    return class_label if class_label is not None else str(example_class)


def my_umap(model, class_mappings, train_method="high_balance", metric="Cosine", cmap='tab10', examples_per_class=100, datatype="train"):
    n_neighbors = [5,15,30,100,250]
    min_dists = [0,0.3,0.6, 0.9]
    data = class_mappings[train_method]["X_" + datatype]
    labels = class_mappings[train_method]["Y_" + datatype]


    classes = [*class_mappings[train_method]["class_mapping_dictionary_to_label_names"].values()]
    num_classes = len(class_mappings[train_method]["class_mapping_dictionary_to_label_names"])
    index = []
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(labels) if y == c]
        index.extend(c_index[0:examples_per_class])
    data = data[index]
    labels = labels[index]
    data = model.predict(data)
    pre_knn_emb = np.zeros((len(n_neighbors), len(min_dists), len(data), 2))

    for i, k in enumerate(n_neighbors):
        for j, dist in enumerate(min_dists):
            pre_knn_emb[i, j] = umap.UMAP(n_neighbors=k,
                                          min_dist=dist,
                                          random_state=42,
                                          # precomputed_knn=pre_knn,
                                          ).fit_transform(data)
    fig, axs = plt.subplots(len(n_neighbors), len(min_dists), figsize=(30, 30))
    for i, ax_row in enumerate(axs):
        for j, ax in enumerate(ax_row):
            scatter = ax.scatter(pre_knn_emb[i, j, :, 0],
                                 pre_knn_emb[i, j, :, 1],
                                 c=labels / num_classes,
                                 cmap=cmap,
                                 alpha=0.2,
                                 s=100,
                                 )
            leg = scatter.legend_elements()[0]
            for lh in leg:
                lh.set_alpha(1)

            ax.legend(handles=leg, labels=classes)

            ax.set_xticks([])
            ax.set_yticks([])
            if i == 0:
                ax.set_title("min_dist = {}".format(min_dists[j]), size=15)
            if j == 0:
                ax.set_ylabel("n_neighbors = {}".format(n_neighbors[i]), size=15)

    fig.suptitle("SHN || Categorized via High Balance|| Metric: " + metric, y=0.92, size=20)
    plt.subplots_adjust(wspace=0.05, hspace=0.05)
    return fig




def my_gaussian_umap(model, class_mappings, ty="high_balance", metric="Cosine",cmap='Spectral',examples_per_class=100,datatype="train"):
    def draw_simple_ellipse(position, width, height, angle,
                            ax=None, from_size=0.1, to_size=0.5, n_ellipses=3,
                            alpha=0.1, color=None,
                            **kwargs):
        ax = ax or plt.gca()
        angle = (angle / np.pi) * 180
        if width <0:
            width=0
        if height<0:
            height=0
        width, height = np.sqrt(width), np.sqrt(height)
        # Draw the Ellipse
        for nsig in np.linspace(from_size, to_size, n_ellipses):
            ax.add_patch(Ellipse(position, nsig * width, nsig * height,
                                 angle, alpha=alpha, lw=0, color=color, **kwargs))

    n_neighbors = [5,10,20,30,50,100,175,250]
    min_dists = [0,0.1,0.3,0.5,0.7, 0.9]
    data = class_mappings[ty]["X_"+datatype]
    labels = class_mappings[ty]["Y_"+datatype]
    classes = [*class_mappings[ty]["class_mapping_dictionary_to_label_names"].values()]
    num_classes = len(class_mappings[ty]["class_mapping_dictionary_to_label_names"])
    colors = plt.get_cmap('Spectral')(np.linspace(0, 1, num_classes))
    index = []
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(labels) if y == c]
        index.extend(c_index[0:examples_per_class])
    data = data[index]
    labels = labels[index]
    data = model.predict(data)
    n_components = 5
    gaussian_mapper = np.zeros((len(n_neighbors), len(min_dists), len(data), n_components))

    for i, k in enumerate(n_neighbors):
        for j, dist in enumerate(min_dists):
            gaussian_mapper[i, j] = umap.UMAP(output_metric='gaussian_energy',
                                              n_components=n_components,
                                              n_neighbors=k,
                                              min_dist=dist,
                                              random_state=42,
                                              # precomputed_knn=pre_knn,
                                              ).fit_transform(data)

    fig, axs = plt.subplots(len(n_neighbors), len(min_dists), figsize=(60, 60))
    for i, ax_row in enumerate(axs):
        for j, ax in enumerate(ax_row):
            print(i, j)
            for l in range(gaussian_mapper[i, j].shape[0]):
                pos = gaussian_mapper[i, j][l, :2]
                draw_simple_ellipse(pos, gaussian_mapper[i, j][l, 2],
                                    gaussian_mapper[i, j][l, 3],
                                    gaussian_mapper[i, j][l, 4],
                                    ax, n_ellipses=1,
                                    color=colors[labels[l]],
                                    from_size=1.0, to_size=1.0, alpha=0.01)
            scatter = ax.scatter(gaussian_mapper[i, j].T[0],
                                 gaussian_mapper[i, j].T[1],
                                 c=labels, cmap=cmap, s=75)
            ax.legend(handles=scatter.legend_elements()[0], labels=classes)

            ax.set_xticks([])
            ax.set_yticks([])
            if i == 0:
                ax.set_title("min_dist = {}".format(min_dists[j]), size=15)
            if j == 0:
                ax.set_ylabel("n_neighbors = {}".format(n_neighbors[i]), size=15)

    fig.suptitle("SHN || Classes:" + ty + "|| Metric: " + metric, y=0.92, size=20)
    plt.subplots_adjust(wspace=0.05, hspace=0.05)
    return fig

def get_pca(model, class_mappings, train_method="high_balance", examples_per_class=100, datatype="train", cmap ="tab10", figsize=(12, 12), alpha=0.2):
    data = class_mappings[train_method]["X_" + datatype]
    labels = class_mappings[train_method]["Y_" + datatype]
    classes = [*class_mappings[train_method]["class_mapping_dictionary_to_label_names"].values()]
    num_classes = len(class_mappings[train_method]["class_mapping_dictionary_to_label_names"])
    index = []
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(labels) if y == c]
        index.extend(c_index[0:examples_per_class])
    data = data[index]
    data = model.predict(data)
    labels = labels[index]
    pca = PCA(n_components=2,svd_solver="randomized",random_state=42)
    projected = pca.fit_transform(data)
    # Percentage of variance explained for each components
    print(
        "explained variance ratio (first prolate_oblate components): %s"
        % str(pca.explained_variance_ratio_)
    )
    fig = plt.figure(figsize=figsize)
    scatter = plt.scatter(projected[:, 0], projected[:, 1],
                c=labels / num_classes, edgecolor='none', alpha=alpha,
                cmap=cmap,
                s = 100,
                          )
    plt.xlabel('component 1')
    plt.ylabel('component 2')
    leg = scatter.legend_elements()[0]
    for lh in leg:
        lh.set_alpha(1)
    plt.legend(handles=leg, labels=classes)
    plt.axis('off')
    return plt

def get_tsne(model, class_mappings, metric ="cosine", train_method="high_balance", examples_per_class=100, datatype="train", cmap ="tab10", figsize=(12, 12), alpha=0.2):
    data = class_mappings[train_method]["X_" + datatype]
    labels = class_mappings[train_method]["Y_" + datatype]


    classes = [*class_mappings[train_method]["class_mapping_dictionary_to_label_names"].values()]
    num_classes = len(class_mappings[train_method]["class_mapping_dictionary_to_label_names"])
    index = []
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(labels) if y == c]
        index.extend(c_index[0:examples_per_class])
    data = data[index]
    data = model.predict(data)
    labels = labels[index]
    tsne = TSNE(n_components=2,learning_rate='auto',metric=metric,random_state=42)
    projected = tsne.fit_transform(data)
    # Percentage of variance explained for each components

    fig = plt.figure(figsize=figsize)
    scatter = plt.scatter(projected[:, 0], projected[:, 1],
                c=labels / num_classes, edgecolor='none', alpha=alpha,
                cmap=cmap,
                s =100,
                          )
    plt.xlabel('component 1')
    plt.ylabel('component 2')
    leg = scatter.legend_elements()[0]
    for lh in leg:
        lh.set_alpha(1)
    plt.legend(handles=leg, labels=classes)
    plt.axis('off')
    return plt


def get_umap(model, class_mappings, metric ="cosine", train_method="high_balance", examples_per_class=100, datatype="train", cmap ="tab10", n_neighbors=15, min_dist=0.1, figsize=(12, 12), alpha=0.2):
    data = class_mappings[train_method]["X_" + datatype]
    labels = class_mappings[train_method]["Y_" + datatype]


    classes = [*class_mappings[train_method]["class_mapping_dictionary_to_label_names"].values()]
    num_classes = len(class_mappings[train_method]["class_mapping_dictionary_to_label_names"])
    index = []
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(labels) if y == c]
        index.extend(c_index[0:examples_per_class])
    data = data[index]
    data = model.predict(data)
    labels = labels[index]
    labels_str = []
    for label in labels:
        labels_str.append(class_mappings[train_method]["class_mapping_dictionary_to_label_names"][label])
    labels_str = np.array(labels_str)
    mapper = umap.UMAP(
        n_neighbors=n_neighbors,
        min_dist=min_dist,
        metric=metric,
        random_state=41).fit(data)

    fig = umap.plot.points(mapper, labels=labels_str,color_key_cmap=cmap,alpha=alpha)
    return fig.figure

def get_umap_embedding(model, class_mappings, train_method="high_balance", examples_per_class=100, datatype="train",n_neighbors=15,min_dist=0.9):

    querie_x,querie_y_index = get_querie_data(class_mappings, train_method, datatype, examples_per_class)

    x_embedded = model.predict(querie_x)


    x_umapped = umap.UMAP(n_neighbors=n_neighbors,
                  min_dist=min_dist,
                  random_state=42,
                  ).fit_transform(x_embedded)
    return x_umapped,querie_y_index,train_method,examples_per_class,datatype,n_neighbors,min_dist

def umap_single(class_mappings,x_umapped,querie_y_index, train_method,
                 datatype,n_neighbors,min_dist,cmap='tab10'):
    querie_y  = class_mappings[train_method]["Y_"+datatype][querie_y_index]
    classes = [*class_mappings[train_method]["class_mapping_dictionary_to_label_names"].values()]
    num_classes = len(class_mappings[train_method]["class_mapping_dictionary_to_label_names"])
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))
    fig.suptitle('UMAP || Categorized via High Balance', fontsize=22)

    scatter = ax1.scatter(x_umapped[:, 0],
                                x_umapped[:, 1],
                                c=querie_y / num_classes,
                                cmap=cmap,
                                alpha=0.2,
                                s=100,
                                )
    leg = scatter.legend_elements()[0]
    for lh in leg:
        lh.set_alpha(1)

    ax1.legend(handles=leg, labels=classes,frameon=False)
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax1.text(0.5, 0.02, "min_dist={}, n_neighbors={}".format(min_dist,n_neighbors), size=15, color='black',transform=ax1.transAxes)

    scatter = ax2.scatter(x_umapped[:, 0],
                                x_umapped[:, 1],
                                c=querie_y / num_classes,
                                cmap=cmap,
                                alpha=0.6,
                                s=200,
                                )
    ax2.set_ylim([-4, 10])
    ax2.set_xlim([-6, 10])
    ax2.set_xticks([])
    ax2.set_yticks([])
    leg = scatter.legend_elements()[0]
    for lh in leg:
        lh.set_alpha(1)

    ax2.legend(handles=leg, labels=classes,frameon=False)

    # scatter = axs[1, 0].scatter(x_umapped[:, 0],
    #                             x_umapped[:, 1],
    #                             c=querie_y / num_classes,
    #                             cmap=cmap,
    #                             alpha=0.4,
    #                             s=100,
    #                             )
    # axs[1, 0].set_ylim([-10, 5])
    # axs[1, 0].set_xlim([-7, 8])
    # axs[1, 0].set_xticks([])
    # axs[1, 0].set_yticks([])
    #
    # scatter = axs[1, 1].scatter(x_umapped[:, 0],
    #                             x_umapped[:, 1],
    #                             c=querie_y / num_classes,
    #                             cmap=cmap,
    #                             alpha=0.4,
    #                             s=100,
    #                             )
    # axs[1, 1].set_ylim([4, 12])
    # axs[1, 1].set_xlim([16, 27])
    # axs[1, 1].set_xticks([])
    # axs[1, 1].set_yticks([])

    return plt



def get_cluster_info(model, class_mappings, train_method, metric="cosine", datatype="train", examples_per_class=100):


    querie_x,querie_y = get_querie_data(class_mappings, train_method, datatype, examples_per_class)

    embedding_vector = model.predict(querie_x)

    pairwise_distance_matrix = pairwise_distances(embedding_vector,metric=metric)
    max_pairwise_distance = np.max(pairwise_distance_matrix)


def umap_specified(class_mappings,x_umapped,querie_y_index, datatype,n_neighbors,min_dist, train_method="written_out", ):

    querie_y  = class_mappings[train_method]["Y_"+datatype][querie_y_index]

    classes = [*class_mappings[train_method]["class_mapping_dictionary_to_label_names"].values()]
    num_classes = len(class_mappings[train_method]["class_mapping_dictionary_to_label_names"])
    labels = [class_mappings[train_method]["class_mapping_dictionary_to_label_names"][y] for y in querie_y]
    main_features = np.array(["spherical_oblate","round","elliptical","prolate","streak","bent","asymmetric","newtonrings","doublerings","layered","empty"])


    fig, axs = plt.subplots(4, 3, figsize=(4 * 6, 3 * 6))
    fig.suptitle('UMAP || Feature specifically marked || min_dist={}, n_neighbors={}'.format(min_dist,n_neighbors), fontsize=22)

    j = 0
    for i, feature in enumerate(main_features):
        boolean_querie_y = np.array([feature in label for label in labels])
        feature_scatter = axs[i % 4, j].scatter(x_umapped[:, 0][boolean_querie_y],
                                                x_umapped[:, 1][boolean_querie_y],
                                                c="teal",
                                                alpha=0.3,
                                                s=100,
                                                label=feature
                                                )
        other_scatter = axs[i % 4, j].scatter(x_umapped[:, 0][~boolean_querie_y],
                                              x_umapped[:, 1][~boolean_querie_y],
                                              c="coral",
                                              alpha=0.1,
                                              s=100,
                                              label="Others"

                                              )
        leg = axs[i % 4, j].legend()
        for lh in leg.legendHandles:
            lh.set_alpha(1)
        axs[i % 4, j].set_xticks([])
        axs[i % 4, j].set_yticks([])
        if i % 4 == 3:
            j = j + 1
    axs[3,2].axis("off")

    return fig


def my_nn_visualization(model,class_mappings, train_method, datatype, examples_per_class,metric="cosine"):

    querie_x,querie_y_indexes = get_querie_data(class_mappings, train_method, datatype, examples_per_class)
    data_y  = class_mappings[train_method]["Y_"+datatype][querie_y_indexes]

    embedding_vectors = model.predict(querie_x)
    pairwise_distance_matrix_all = pairwise_distances(embedding_vectors, metric=metric)

    num_rows = len([*Counter(data_y).keys()])
    num_cols = 6
    fig, axs = plt.subplots(num_rows, num_cols, figsize=(36, 48))
    for i, feature in enumerate([*Counter(data_y).keys()]):
        index_feature = data_y == feature
        pairwise_feature_distance = pairwise_distances(embedding_vectors[index_feature], metric=metric)
        minimum_index = np.argmin(np.sum(pairwise_feature_distance.transpose(), axis=1))
        min_index_all = [i for i, index in enumerate(index_feature) if int(index) == 1][minimum_index]
        minimum_pairwise_feature_distance_vector = pairwise_distance_matrix_all.transpose()[min_index_all]
        sorted_indexes = minimum_pairwise_feature_distance_vector.argsort()
        sorted_minimum_pairwise_feature_distance_vector = minimum_pairwise_feature_distance_vector[sorted_indexes]
        nn_index = np.array([querie_y_indexes[index] for index in sorted_indexes[0:num_cols]])
        nn_data = np.array([querie_x[index] for index in sorted_indexes[0:num_cols]])
        nn_high_balance_labels = class_mappings["high_balance"]["Y_" + datatype][nn_index]
        nn_abbreviated_labels = class_mappings["abbreviated"]["Y_" + datatype][nn_index]
        nn_abbreviated_names = [class_mappings["abbreviated"]["class_mapping_dictionary_to_label_names"][label] for
                                label in
                                nn_abbreviated_labels]
        nn_high_balance_names = [class_mappings["high_balance"]["class_mapping_dictionary_to_label_names"][label] for
                                 label
                                 in nn_high_balance_labels]
        for j in range(len(nn_abbreviated_labels)):
            im = nn_data[j]
            nn_high_balance_name = nn_high_balance_names[j]
            nn_abbreviated_name = nn_abbreviated_names[j]
            distance = sorted_minimum_pairwise_feature_distance_vector[j]
            distance = round(distance, 4)
            max_val = float(tf.math.reduce_max(im))
            axs[i, j].imshow(im / max_val)
            axs[i, j].text(0.5, -0.1, nn_abbreviated_name, transform=axs[i, j].transAxes,horizontalalignment="center")
            axs[i, j].axis("off")
            if j > 0:
                axs[i, j].set_title("{} - {:.5f}".format(nn_high_balance_name, distance))
            else:
                axs[i, j].set_title("{}".format(nn_high_balance_name))
    fig.tight_layout()
    plt.show()
