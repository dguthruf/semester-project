import os
import tensorflow as tf

os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)

import squirrel as sq
import tensorflow as tf
import numpy as np
from collections import Counter
from tensorflow import keras
from sklearn.model_selection import train_test_split
import random
import tensorflow_similarity as tfsim
tfsim.utils.tf_cap_memory()


class RandomColorAffine(tf.keras.layers.Layer):
    """
    Adjusted to fit for 1-Channel images
    """

    def __init__(self, brightness=0., jitter=0., **kwargs):
        super().__init__(**kwargs)

        self.brightness = brightness
        self.jitter = jitter

    def call(self, images, training=True):
        if training:
            batch_size = tf.shape(images)[0]

            brightness_scales = 1 + tf.random.uniform(
                (batch_size, 1, 1, 1), minval=-self.brightness, maxval=self.brightness
            )
            jitter_matrices = tf.random.uniform(
                (batch_size, 1, 1, 1), minval=-self.jitter, maxval=self.jitter
            )

            color_transforms = (
                    tf.eye(1, batch_shape=[batch_size, 1]) * brightness_scales
                    + jitter_matrices
            )
            images = tf.clip_by_value(tf.matmul(images, tf.cast(color_transforms, images.dtype)), 0, 1)
        return images


def MySim(min_area=0.75, brightness=0.3, jitter=0.1):
    """
    Important: Augmentationlayers contain Rescaling layers adjsuted to static_helium_nanodroplet data and
    Efficientnetbackbone!
    Cropping: forces the model to encode different parts of the same image similarly, we implement it with the
    RandomTranslation and RandomZoom layers
    Color jitter: prevents a trivial color histogram-based solution to the task by distorting color histograms.
    A principled way to implement that is by affine transformations in color space.
    :param min_area: range(0,1) defines zoom-factor
    :param brightness: range(0,1) defines brightness change
    :param jitter: range(0,1) diorts color histogramm
    :return:
    A sequential keras model ready to be eaten by EfficientnetSim, architecture from tensorflow_similarity
    """
    zoom_factor = 1.0 - tf.sqrt(min_area)
    augmentation_layers = keras.Sequential(
        [
            keras.layers.RandomFlip("horizontal"),
            keras.layers.RandomTranslation(zoom_factor / 2, zoom_factor / 2),
            keras.layers.RandomZoom((-zoom_factor, 0.0), (-zoom_factor, 0.0)),
            # Only takes one channel images image_shape = (num_samples,img_size,img_size,channels=1)
            RandomColorAffine(brightness, jitter),
        ]
    )
    return augmentation_layers


def resize(img, img_size):
    """
    Done with tf.image.resize and CPU
    :param img: (num_samples,size,size,channels)
    :param img_size: wanted final image size
    :return: resized images (num_samples,img_size,img_size,channels)
    """
    with tf.device("/cpu:0"):
        # Resize according to needed img_size for backbone Efficinetnet
        img = tf.image.resize(img, [img_size, img_size])
        # Rescale to [0,255]
        img = tf.keras.layers.Rescaling(255 / 65536)(img)
        return img.numpy()


def get_class_mappings(y, x):
    """
    assings each single label to a high_balance_class defined in the dict below

    :param y: labels array with all single classes
    :return:
    new_high_balance class labels as array y = (int,int,int,...)
    new_high_balance labels as strings ("Round","Asymmetric",....)
    high_balance_dict containg classes and corresponding labels
    """
    # Each multiclass label [0,1,...,42] is not only the label but also the order of number of samples within each
    # class, therefore we have the most examples of class 0 and the least amount of examples in class 42

    # julian multiclass_mapping_high_balance is the grouping of the multiclasses according to the paper of:
    # Julian Zimmeramann
    # Deep neural neprolate_oblaterks for classifying complex features in diffraction images
    # Balanced dataset
    multiclass_mapping_julian = {}
    multiclass_mapping_julian["Round"] = [0, 2, 34, 37]
    multiclass_mapping_julian["Elliptical"] = [1, 10, 31, 40]
    multiclass_mapping_julian["Bent"] = [5, 23, 24, 27, 35, 36]
    multiclass_mapping_julian["Asymmetric"] = [3, 14, 16, 8]
    multiclass_mapping_julian["DoubleRings"] = [7, 9, 15, 17, 19, 22, 39, 41, 42]
    multiclass_mapping_julian["Streak"] = [6, 12, 13, 18, 21, 26, 28, 29, 30, 32, 33]
    multiclass_mapping_julian["Layered"] = [25, 38]
    multiclass_mapping_julian["Prolate"] = [20]
    multiclass_mapping_julian["Empty"] = [4]

    # My grouping of the multiclasses with all 43 multiclasses, where I created 10 high_balance classes, each describing
    # the most prominent feature. The multiclasses are into the high_balance classes sucht tha i achive a good balance of samples
    # for each high_balanceclass
    multiclass_mapping_high_balance = {}
    multiclass_mapping_high_balance["Round"] = [0]
    multiclass_mapping_high_balance["Elliptical"] = [1, 10]
    multiclass_mapping_high_balance["NewtonRings"] = [2]
    multiclass_mapping_high_balance["Asymmetric"] = [3, 14, 16, 37]
    multiclass_mapping_high_balance["Empty"] = [4]
    multiclass_mapping_high_balance["Bent"] = [5, 8, 23, 24, 27]
    multiclass_mapping_high_balance["Streak"] = [6, 11, 12, 18, 26, 29, 30]
    multiclass_mapping_high_balance["DoubleRings"] = [7, 9, 15, 17, 19, 22, 39, 42]
    multiclass_mapping_high_balance["Layered"] = [13, 21, 25, 28, 31, 32, 33, 34, 35, 36, 38, 40, 41]
    multiclass_mapping_high_balance["Prolate"] = [20]

    multiclass_mapping_multi_occurence = {}
    multiclass_mapping_multi_occurence["Round"] = [0, 2, 7, 9, 34, 37, 41]
    multiclass_mapping_multi_occurence["Elliptical"] = [1, 3, 10, 14, 15, 17, 19, 22, 31, 40]
    multiclass_mapping_multi_occurence["NewtonRings"] = [2, 9, 10, 14, 18, 19, 22, 23, 27, 31, 32, 33, 36, 39, 40, 41,
                                                         42]
    multiclass_mapping_multi_occurence["Asymmetric"] = [3, 8, 14, 16, 17, 22, 26, 27, 37, 40]
    multiclass_mapping_multi_occurence["Empty"] = [4]
    multiclass_mapping_multi_occurence["Bent"] = [5, 6, 8, 11, 13, 18, 23, 24, 27, 28, 30, 32, 35, 36, 39]
    multiclass_mapping_multi_occurence["Streak"] = [6, 11, 12, 13, 18, 21, 26, 28, 29, 30, 32, 33]
    multiclass_mapping_multi_occurence["DoubleRings"] = [7, 9, 15, 17, 19, 22, 24, 39, 41, 42]
    multiclass_mapping_multi_occurence["Layered"] = [13, 21, 25, 28, 31, 32, 33, 34, 35, 36, 38, 40, 41]
    multiclass_mapping_multi_occurence["Prolate"] = [5, 6, 8, 11, 12, 13, 16, 18, 20, 21, 23, 24, 26, 27, 32, 33, 35,
                                                     36, 39, 42]
    multiclass_mapping_multi_occurence["SphericalOblate"] = [0, 1, 2, 3, 7, 9, 10, 14, 15, 17, 19, 22, 31, 34, 37, 38,
                                                             40, 41]

    # Grouping of the multiclasses into spherical oblate and prolate. Also three other classes as they are not within
    # the prolate_oblate high_balance classes. But very low number of examples compared to the prolate_oblate high_balance classes.
    multi_class_mapping_prolate_oblate = {}
    multi_class_mapping_prolate_oblate["Spherical Oblate"] = [0, 1, 2, 3, 7, 9, 10, 14, 15, 17, 19, 22, 31, 34, 37, 38,
                                                              40, 41]
    multi_class_mapping_prolate_oblate["Prolate"] = [5, 6, 8, 11, 12, 13, 16, 18, 20, 21, 23, 24, 26, 27, 32, 33, 35,
                                                     36, 39, 42]
    multi_class_mapping_prolate_oblate["Empty"] = [4]
    multi_class_mapping_prolate_oblate["Layered"] = [25]
    multi_class_mapping_prolate_oblate["Streak"] = [28, 29, 30]

    # Creatin dics in dics, maybe would be better sovled with pandas
    class_mappings = {"high_balance": {}, "prolate_oblate": {}, "julian": {}, "multi_occurence": {}}
    class_mappings["high_balance"]["class_mapping_dictionary_to_multiclasses"] = multiclass_mapping_high_balance
    class_mappings["prolate_oblate"]["class_mapping_dictionary_to_multiclasses"] = multi_class_mapping_prolate_oblate
    class_mappings["julian"]["class_mapping_dictionary_to_multiclasses"] = multiclass_mapping_julian
    class_mappings["multi_occurence"]["class_mapping_dictionary_to_multiclasses"] = multiclass_mapping_multi_occurence

    # Create Class Dics that give the corresponding label written out as str to the class as int
    for mapping in class_mappings:
        new_labels_defined_by_mapping = []
        # Lists below are needed to create new x and y data which contains examples multiple times as class mapping is done
        # with multilcass multi occurence method newdatax is about 12k instead of 7k samples
        multi_occurence_data_y = []
        multi_occurence_data_x = []

        for i, label_i in enumerate(y):
            for j, mapping_class_name_j in enumerate(
                    [*class_mappings[mapping]["class_mapping_dictionary_to_multiclasses"].keys()]):
                if label_i in class_mappings[mapping]["class_mapping_dictionary_to_multiclasses"][mapping_class_name_j]:
                    new_mapping_defined_label = j
                    if mapping == "multi_occurence":
                        multi_occurence_data_x.append(x[i])
                        multi_occurence_data_y.append(j)
            new_labels_defined_by_mapping.append(new_mapping_defined_label)

        mapping_labels = np.array([*class_mappings[mapping]["class_mapping_dictionary_to_multiclasses"].keys()])
        new_dict = {c_id: c_lbl for c_id, c_lbl in zip(range(len(mapping_labels)), mapping_labels)}
        class_mappings[mapping]["class_mapping_dictionary_to_label_names"] = new_dict
        class_mappings[mapping]["Y"] = np.array(new_labels_defined_by_mapping)

    return class_mappings, np.array(multi_occurence_data_x), np.array(multi_occurence_data_y)


def categorical_to_integer(y):
    """
    Labeling the categorical labels according to your wishes(info from squirrel load dataset)
    abbreviated and written_out description possible
    :param y: np.array of catecorical lables array([0,0,1,2....],[1,1,0,....],...)
    :return:
    Dicts with classes corresponding to their labels(long and abbreviated version)
    And array of single_y int label for each class sorted according to their sample frequency.
    dict_written_out   = {
                     0:spherical_oblate_round_...
                     1:spherical_oblate_elliptical_...
                     2: ....}
    dict_abbreviated  = {
                     0:spherical_oblate_round_...
                     1:spherical_oblate_elliptical_...
                     2: ....}
    y           = array(0,1,3,22,3,0,38,...)

    """
    # Define abbreviated_label_names for dicts
    labels_written_out = np.array(
        ["spherical_oblate", "round", "elliptical", "prolate", "streak", "bent", "asymmetric", "newtonrings",
         "doublerings", "layered", "empty"])
    labels_abbreviated = np.array(["SO", "R", "E", "P", "S", "B", "A", "NR", "DR", "L", "EY"])
    list_written_out = []
    list_abbreviated = []

    # Go through each categrical 11-Dim label
    for single_y in y:
        # convert to bool
        single_y = single_y.astype(dtype=bool)
        # Transform bool to combination of classes
        # Example: y =   [1,1,1,0,0,0,0,0,1,0,0]
        #         Apply y-bool to dictionary:labels_written_out
        #         label="spherical_oblate-round-elliptical-doublerings"
        label_written_out = '-'.join(labels_written_out[single_y])
        label_abbreviated = '-'.join(labels_abbreviated[single_y])
        list_written_out.append(label_written_out)
        list_abbreviated.append(label_abbreviated)

    # Get all class combinations label_names
    abbreviated_label_names = np.array([*Counter(list_abbreviated).keys()])
    written_out_label_names = np.array([*Counter(list_written_out).keys()])

    # Get frequency of each class and sort according to their frequency
    abbreviated_label_frequency = np.array([*Counter(list_abbreviated).values()])
    sorted_index_by_frequency = np.argsort(-abbreviated_label_frequency)
    sorted_abbreviated_label_names = abbreviated_label_names[sorted_index_by_frequency].tolist()
    sorted_written_out_label_names = written_out_label_names[sorted_index_by_frequency].tolist()

    # Create dicts with class label correspondance
    y_dict = dict(zip(sorted_abbreviated_label_names, range(len(sorted_abbreviated_label_names))))
    dict_abbreviated = dict(zip(range(len(sorted_abbreviated_label_names)), sorted_abbreviated_label_names))
    dict_written_out = dict(zip(range(len(sorted_abbreviated_label_names)), sorted_written_out_label_names))

    # create single_y class int array to be able to workt with tensorflow_similarity
    y = np.array([y_dict[ls] for ls in list_abbreviated])

    class_mappings = {"written_out": {}, "abbreviated": {}}
    class_mappings["written_out"]["class_mapping_dictionary_to_label_names"] = dict_written_out
    class_mappings["abbreviated"]["class_mapping_dictionary_to_label_names"] = dict_abbreviated
    class_mappings["abbreviated"]["Y"] = y
    class_mappings["written_out"]["Y"] = y

    return y, class_mappings


def add_augmentation_low_frequency_class(all_data_x, all_data_y, split_x, split_y, number_of_minimum_samples_per_class):
    """
    :param all_data_x: (num_samples,img_size,img_size,channels) all samples
    :param all_data_y: array(int,int,int...) all samples
    :param split_x: (num_samples,img_size,img_size,channels)
    :param split_y: array(int,int,int...) with maybe missing classes i.e test split after stratify split
    :param number_of_minimum_samples_per_class: number of max augmentations
    :return:
    split_x,split_y with at least num_augs samples per class
    """
    # Array with all classes and found_classes
    all_possible_y = [*Counter(all_data_y).keys()]

    # Define Augmentations that should be applied to increase samples from <num_augs to = num_augs
    augmentation_layers = keras.Sequential(
        [
            keras.layers.RandomFlip("horizontal_and_vertical"),
            keras.layers.RandomContrast(0.5),
            keras.layers.RandomTranslation(0.05, 0.05),
            keras.layers.RandomZoom(0.05),
        ]
    )
    additional_augmented_x = []
    additional_augmented_y = []

    for y in all_possible_y:
        indexes_of_y_occurences = [i for i, y_single in enumerate(split_y) if y_single == y]
        if len(indexes_of_y_occurences) == 0:
            y_index_from_all = [i for i, ys in enumerate(all_data_y) if ys == y]
            for j in range(number_of_minimum_samples_per_class):
                additional_augmented_x.append(augmentation_layers(random.choice(all_data_x[y_index_from_all])))
                additional_augmented_y.append(y)
        elif len(indexes_of_y_occurences) < number_of_minimum_samples_per_class and len(indexes_of_y_occurences) > 0:
            y_index_from_all = [i for i, ys in enumerate(all_data_y) if ys == y]
            for j in range(number_of_minimum_samples_per_class - len(indexes_of_y_occurences)):
                additional_augmented_x.append(augmentation_layers(random.choice(all_data_x[y_index_from_all])))
                additional_augmented_y.append(y)
    try:
        split_x_with_augmentations = np.concatenate((np.array(additional_augmented_x), split_x))
        split_y_with_augmentations = np.concatenate((np.array(additional_augmented_y), split_y))
    except:
        pass
    return split_x_with_augmentations, split_y_with_augmentations


def get_data(
        img_size=224,
        number_of_minimum_samples_per_class=2,
        training_with_top_n_frequency_classes=10,
        train_method="Top",

):
    """
    Loads the unbalanced static helium_nanodroplets dataset with squirrel as X_train,y_train,X_test,y_test
    It is guaranteed that at least one example per class exists in test train split.
    --- Basic Information about dataset raw loaded with squirrel:

        Default Image data shape is (1035,1035) with 16 bit scaling (2^16=65536).
        11 Dimensional categorical labels
        43 Single Classes
        More info in "info" when loaded

    Parameters
    ----------
    img_size:
        Efficientnet B0 needs (224,224,3) with [0,255]
        Therefore we resize to (224,224)
    channels:
        Efficientnet B0 needs (224,224,3) with [0,255]
        Therefore we expand to (224,224,1). Not to (224,224,3) as
        we do not have color data. It still works with 1 Channel but we get a warning.
        One could use tf.image.grayscale_to_rgb to get (224,224,3) if the performance is better
        is unclear, but the computational cost will be greater for sure.
        We do not Rescale here as this shall be done with GPU in augmentation layers
    number_of_minimum_samples_per_class:
        num_augs defines the least number of samples that one class will have over all data points
        There exists classes with only 1 example. To achive at least num_aug of samples per class
        We apply num_augs augmentations to the existing samples.
        At least 2 examples per class are needed to apply contrastive learning.
    training_with_top_n_frequency_classes:
        top defines on which top classes the training will be done. top = 10 would mean you will train
        your model with the 10 classes that have the most unique samples to train from.
        top = 43 would mean you train with every class, but there are classes which have only one
        example + one augmented example, which is problematic for the result.
    train_method:
        Says on what grouping we train on:
        train="high_balance"->we do the training on the grouping of multiclasses in the high_balance classes described above in the function
        get_high_balance_classes.
    Returns
    ----------
    numpy Arrays of X_train,y_train,X_test,y_test
    X = array(num_samples,img_size,img_size,channels)
    y = array(int,int,int...)
    """
    # Load data wiht squirrel into one dataset with info and batch_size=-1 to load as one batch
    data, info = sq.load(name='static_helium_nanodroplets',
                         split=["train"],
                         with_info=True,
                         batch_size=-1)

    # transform to numpy for convinience and split into data and labels
    data = sq.as_numpy(data)
    all_data_x, all_data_y = data[0]["image"], data[0]["label"]

    # Resize and expand according to description above
    all_data_x = np.expand_dims(all_data_x, axis=3)
    all_data_x = resize(all_data_x, img_size)

    # Transform categorcial labels to single labelstring to make it usable for tensorflow similarity
    # Both dicts are essentially the same and contain all classes and their corresponding labels written out and abbreviated
    # Classes are also sorted according to their number of samples: Class 0 has most samples Class 1 has second most samples etc
    all_data_y, class_mappings = categorical_to_integer(all_data_y)

    # Add augmenations to classes with less than num_augs samples (more info about num_augs above)
    all_data_x, all_data_y = add_augmentation_low_frequency_class(all_data_x, all_data_y, all_data_x, all_data_y,
                                                                  number_of_minimum_samples_per_class)

    # stratify split data into train test
    X_train, X_test, y_train, y_test = train_test_split(all_data_x, all_data_y, test_size=0.2, stratify=all_data_y)

    # Include another augmentation for test classes with less than 2 samples, for validation dataset
    X_test, y_test = add_augmentation_low_frequency_class(all_data_x, all_data_y, X_test, y_test,
                                                          number_of_minimum_samples_per_class)

    # Get all classes in y_train and sort them, to get [0,1,2,3,4....,41,42] Class zero has most samples class 42 has the least.
    classes = [*Counter(y_train).keys()]
    classes.sort()

    # Get class_mapping dictionaries and mutli_occurence train and test arrays
    class_mappings_train, multi_occurence_x_train, multi_occurence_y_train = get_class_mappings(y_train, X_train)
    class_mappings_test, multi_occurence_x_test, multi_occurence_y_test = get_class_mappings(y_test, X_test)

    # Add train test dict entrys for each mapping
    new_class_mappings = {"written_out": {}, "abbreviated": {}, "high_balance": {}, "multi_occurence": {},
                          "prolate_oblate": {}, "julian": {}}
    for mapping in new_class_mappings:
        if mapping == "multi_occurence":
            new_class_mappings[mapping]["Y_train"] = multi_occurence_y_train
            new_class_mappings[mapping]["X_train"] = multi_occurence_x_train
            new_class_mappings[mapping]["X_test"] = multi_occurence_x_test
            new_class_mappings[mapping]["Y_test"] = multi_occurence_y_test
            new_class_mappings[mapping]["class_mapping_dictionary_to_multiclasses"] = class_mappings_train[mapping][
                "class_mapping_dictionary_to_multiclasses"]
            new_class_mappings[mapping]["class_mapping_dictionary_to_label_names"] = class_mappings_train[mapping][
                "class_mapping_dictionary_to_label_names"]
        elif mapping == "high_balance" or mapping == "prolate_oblate" or mapping == "julian":
            new_class_mappings[mapping]["Y_train"] = class_mappings_train[mapping]["Y"]
            new_class_mappings[mapping]["X_train"] = X_train
            new_class_mappings[mapping]["X_test"] = X_test
            new_class_mappings[mapping]["Y_test"] = class_mappings_test[mapping]["Y"]
            new_class_mappings[mapping]["class_mapping_dictionary_to_multiclasses"] = class_mappings_train[mapping][
                "class_mapping_dictionary_to_multiclasses"]
            new_class_mappings[mapping]["class_mapping_dictionary_to_label_names"] = class_mappings_train[mapping][
                "class_mapping_dictionary_to_label_names"]
        else:
            new_class_mappings[mapping]["Y_train"] = y_train
            new_class_mappings[mapping]["X_train"] = X_train
            new_class_mappings[mapping]["X_test"] = X_test
            new_class_mappings[mapping]["Y_test"] = y_test
            new_class_mappings[mapping]["class_mapping_dictionary_to_label_names"] = class_mappings[mapping][
                "class_mapping_dictionary_to_label_names"]

        if train_method == mapping:
            y_train_new = new_class_mappings[mapping]["Y_train"]
            y_test_new = new_class_mappings[mapping]["Y_test"]
            X_train_new = new_class_mappings[mapping]["X_train"]
            X_test_new = new_class_mappings[mapping]["X_test"]
            train_class_list = [*Counter(y_train_new).keys()]
            if train_method == "Top":
                train_class_list = classes[0:training_with_top_n_frequency_classes]

    train_class_list.sort()

    return X_train_new, X_test_new, y_train_new, y_test_new, train_class_list, new_class_mappings
