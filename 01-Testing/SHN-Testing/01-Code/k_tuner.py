import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
import keras_tuner as kt
from tensorflow.keras.callbacks import TensorBoard
from collections import Counter
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
import tensorflow_similarity as tfsim
FLAGS = tf.compat.v1.flags.FLAGS
FLAGS.__delattr__("wikipedia_auto_select_flume_mode")
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.samplers import MultiShotMemorySampler  # sample data
from shn_operations import get_data,get_main_class,visualize,MySim
from augmenters import RandAugment,AutoAugment,SimCLRAugmenter,RandomErasing
from multiprocessing import cpu_count
from sklearn.model_selection import train_test_split
from nn import gpu_augmenter

tfsim.utils.tf_cap_memory()

"""
-------------------------------------------------------------------------------------------------------------------
-----------------------------------------------Define Overall Things-----------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
dataset='static_helium_nanodroplets'
tuner = "/scratch/dguthruf/tuner/shn/small"
#Train on the top most frequent classes
augmenter_source = "tfsim" #nux or tfsim
channels = 1
train = "Main"
name = "SimCLR"
log_dir = os.path.join(tuner,name)

#Examples per class per batch for train and val
#Defines batchsize and is also dependent on "top" parameter
EXAMPLES_PER_CLASS_TRAIN = 2
EXAMPLES_PER_CLASS_VAL = 2



"""
-------------------------------------------------------------------------------------------------------------------
----------------------------------------------------Prepare Data---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
img_size = 224
top=10
X_train,X_val,y_train,y_val,train_class_list,class_mappings = get_data(training_with_top_n_frequency_classes=top,
                                                                       img_size=img_size,
                                                                       train_method=train,
                                                                       )
X_train, _, y_train, _ = train_test_split(X_train, y_train, test_size=0.5, random_state=1, stratify=y_train)

num_classes = len([*Counter(y_val).keys()])
num_train_classes = len(train_class_list)

#Steps per epoch choosen such, that in one epoch whole data is run
STEPS_PER_EPOCH_TRAIN = len(y_train) // (num_train_classes * EXAMPLES_PER_CLASS_TRAIN)
STEPS_PER_EPOCH_VAL = len(y_val) // (num_train_classes * EXAMPLES_PER_CLASS_VAL)

#Perpare train and val data with MultishotMemorySampler from tensorflow_similarity
#With that it is guaranteed that each batch contains at least 2 samples per class per batch
#If EXAMPLES_PER_CLASS >2 we have EXAMPLES_PER_CLASS per Batch Batchsize = EXAMPLES_PER_CLASS * NumClasses
train_ds = MultiShotMemorySampler(X_train, y_train,
                                 classes_per_batch=num_train_classes,
                                 examples_per_class_per_batch=EXAMPLES_PER_CLASS_TRAIN,
                                 class_list=train_class_list,
                                 steps_per_epoch=STEPS_PER_EPOCH_TRAIN,
                                 )

val_ds = MultiShotMemorySampler(X_val, y_val,
                                classes_per_batch=num_train_classes,
                                examples_per_class_per_batch=EXAMPLES_PER_CLASS_VAL,
                                steps_per_epoch=STEPS_PER_EPOCH_VAL)




class MyHyperModel(kt.HyperModel):
    def build(self, hp):

        variant = hp.Choice("variant", ["B0", "B1", "B2", "B3"], default="B0")
        trainable = hp.Choice("trainable", ["frozen", "partial", "full"], default='partial')
        # min_area = hp.Float("min_area", min_value=0.5, max_value=1, sampling="linear", default=0.8)
        # brightness = hp.Float("brightness", min_value=0.1, max_value=0.5, sampling="linear", default=0.45)
        # jitter = hp.Float("jitter", min_value=0.01, max_value=0.3, sampling="linear", default=0.3)
        optimizer = hp.Choice("optimizer", ["adam", "sgd", "adamax", "adadelta"], default='adam')
        learning_rate = hp.Choice("lr",
                                  values=[0.01, 0.005, 0.001, 0.0005, 0.0001, 0.00005, 0.00001, 0.000005, 0.000001],
                                  default=0.00001)
        embedding_size = hp.Choice("emb_size", values=[28, 56, 128, 192, 256, 320, 384, 448, 512, 1024], default=512)
        distance = hp.Choice("distance", ["Cosine", "L2", "L1"], default='Cosine')
        loss = hp.Choice("loss", ["circle", "multi", "triplet"], default='circle')
        num_layers = hp.Choice("num_layers",
                               values=[0, 1, 2, 3, 4, 5,],
                               default=2)

        magnitude = hp.Choice("magnitude",
                              values=[0,2, 4,6,8,10,12],
                              default=10)
        simclr = hp.Choice("simclr",
                           values=["v2", "v1"],
                           default="v1")
        jitter_stength = hp.Choice("jitter_stength",
                           values=[0.01, 0.1, 0.3,0.5,0.8],
                           default=0.1)
        random_erasing_probability=hp.Choice("random_erasing_probability",
                                             values=[0.0,0.1,0.2,0.3,0.4,0.5],
                                             default=0.3)
        nux_strength=hp.Choice("nux_strength",
                                             values=[0.1,0.2,0.3,0.4,0.5],
                                             default=0.3)
        autoaugment = hp.Choice("autoaugment",
                           values=["No","v0","reduced_cifar10","svhn","reduced_imagenet"],
                           default="No")
        if variant == 'B0':
            size = 224
        elif variant == "B1":
            size = 240
        elif variant == "B2":
            size = 260
        elif variant == "B3":
            size = 300
        elif variant == "B4":
            size = 380
        elif variant == "B5":
            size = 456
        elif variant == "B6":
            size = 528
        elif variant == "B7":
            size = 600
        decay_steps = hp.Choice("decay_steps",
                              values=[100,500,1000,5000],
                              default=500)
        decay_rate = hp.Choice("decay_rate",
                              values=[0.92,0.96,0.99],
                              default=0.92)
        lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
            learning_rate,
            decay_steps=decay_steps,
            decay_rate=decay_rate,
            staircase=True)

        if optimizer == 'adam':
            optimizer = keras.optimizers.Adam(learning_rate=lr_schedule)
        elif optimizer == 'sgd':
            optimizer = keras.optimizers.SGD(learning_rate=learning_rate)
        elif optimizer == 'adamax':
            optimizer = keras.optimizers.Adamax(learning_rate=learning_rate)
        elif optimizer == 'adadelta':
            optimizer = keras.optimizers.Adadelta(learning_rate=learning_rate)

        if loss == 'circle':
            with hp.conditional_scope("loss", ['circle']):
                gamma = hp.Choice("gamma", values=[64, 128, 256, 512, 1024], default=128)
                loss = tfsim.losses.CircleLoss(gamma=gamma)
        elif loss == 'multi':
            with hp.conditional_scope("loss", ['multi']):
                distance_m = hp.Choice("distance_m", ["Cosine", "L2", "L1"], default='Cosine')
                loss = tfsim.losses.MultiSimilarityLoss(distance=distance_m)
        elif loss == 'triplet':
            with hp.conditional_scope("loss", ['triplet']):
                p_mining = hp.Choice("p_mining", ["hard", "easy"], default="hard")
                n_mining = hp.Choice("n_mining", ["hard", "semi-hard", "easy"], default="semi-hard")
                distance_t = hp.Choice("distance_t", ["Cosine", "L2", "L1"], default='Cosine')
                loss = tfsim.losses.TripletLoss(distance=distance_t, positive_mining_strategy=p_mining,
                                                negative_mining_strategy=n_mining)

        model = EfficientNetSim((size, size, 1), embedding_size=embedding_size, variant=variant, trainable=trainable,
                                augmentation=None)
        model.compile(optimizer, loss=loss, distance=distance)

        return model

    def fit(self, hp, model, train_ds, val_ds, callbacks, **kwargs):

        variant = hp.get("variant")
        if variant == 'B0':
            size = 224
        elif variant == "B1":
            size = 240
        elif variant == "B2":
            size = 260
        elif variant == "B3":
            size = 300
        elif variant == "B4":
            size = 380
        elif variant == "B5":
            size = 456
        elif variant == "B6":
            size = 528
        elif variant == "B7":
            size = 600

        # def resize(img, label):
        #     with tf.device("/cpu:0"):
        #         img = tf.cast(img, dtype="int32")
        #         img = tf.image.resize(img, size, size)
        #         return img, label
        # train_ds.preprocess_fn = resize
        # val_ds.preprocess_fn = resize

        tsc = callbacks[0]
        tsc.targets = tf.image.resize(tsc.targets, (size, size))
        tsc.queries = tf.image.resize(tsc.queries, (size, size))
        callbacks[0] = tsc

        if augmenter_source =="tfsim":
            magnitude=hp.get("magnitude")
            num_layers=hp.get("num_layers")
            simclr=hp.get("simclr")
            jitter_stength = hp.get("jitter_stength")
            random_erasing_probability=hp.get("random_erasing_probability")
            autoaugment=hp.get("autoaugment")

            # augmenter = RandAugment(num_layers=num_layers, magnitude=magnitude)

            if simclr != "No":
                augmenter = SimCLRAugmenter(size, size,version=simclr,color_distort=False,jitter_stength=jitter_stength)
                # augmenter = RandomErasing(probability=random_erasing_probability)
                # augmenter = AutoAugment(augmentation_name=autoaugment)
            else:
                augmenter=2


            @tf.function()
            def process(img):
                img = tf.image.grayscale_to_rgb(img)

                img = tf.keras.layers.Rescaling(1/255)(img)
                img = augmenter.distort(img)
                img = tf.keras.layers.Rescaling(255)(img)
                img = tf.image.rgb_to_grayscale(img)
                return img

            def loader(x, y, *args):
                imgs = tf.stack(x)
                imgs = tf.map_fn(process, imgs, parallel_iterations=cpu_count(), dtype="float32")
                return imgs, y
        elif augmenter_source == "nux":
            nux_strength=hp.get("nux_strength")

            _augmentation = {"brightness": 0.2, "jitter": 0.1,
                             "scale": (0.5, 1.0), "fill_scale": (0.05, 0.15),
                             "fill_ratio": (0.5, 1.5)}

            nux_augmenter = gpu_augmenter(in_shape=(size, size, 1), ratio=(3 / 4, 4 / 3), strength=nux_strength, fill_value=0.,
                                   **_augmentation, model="efficient")

            def loader(x, y, *args):
                imgs = tf.stack(x)
                imgs = tf.keras.layers.Rescaling(1 / 255)(imgs)
                imgs = nux_augmenter(imgs)
                return imgs, y

        if simclr!="No":
            train_ds.augmenter = loader

        # train_ds.augmenter = loader


        history = model.fit(
            train_ds,
            validation_data=val_ds,
            callbacks=callbacks,
            **kwargs,
        )

        return {"f1_score": history.history["f1_score"][-1],
                "binary_accuracy": history.history["binary_accuracy"][-1],
                "val_loss":history.history["binary_accuracy"][-1],
                }


num_queries = int(len(y_val) / 3)
queries_x = X_val[0:num_queries]
queries_y = y_val[0:num_queries]
targets_x = X_val[num_queries:-1]
targets_y = y_val[num_queries:-1]

tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=1,
                   metrics=['f1_score', 'binary_accuracy'],
                   )
tbc = TensorBoard(log_dir=log_dir + '/tb')
esc = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=8, restore_best_weights=True)
callbacks = [esc, tbc, tsc]

hp = kt.HyperParameters()
# loss = hp.Choice("loss", ["circle", "multi", "triplet"], default='multi')
# with hp.conditional_scope("loss", ['circle']):
#     gamma = hp.Choice("gamma", values=[64, 128, 256, 512, 1024], default=512)
# with hp.conditional_scope("loss", ['multi']):
#     distance_m = hp.Choice("distance_m", ["Cosine", "L2", "L1"], default='Cosine')
# with hp.conditional_scope("loss", ['triplet']):
#     p_mining = hp.Choice("p_mining", ["hard", "easy"], default="hard")
#     n_mining = hp.Choice("n_mining", ["hard", "semi-hard", "easy"], default="semi-hard")
#     distance_t = hp.Choice("distance_t", ["Cosine", "L2", "L1"], default='Cosine')
# trainable = hp.Choice("trainable", ["frozen", "partial"], default='frozen')

# min_area = hp.Float("min_area", min_value=0.5, max_value=1, sampling="linear", default=0.8)
# brightness = hp.Float("brightness", min_value=0.1, max_value=0.5, sampling="linear", default=0.45)
# jitter = hp.Float("jitter", min_value=0.01, max_value=0.3, sampling="linear", default=0.3)
# num_layers = hp.Choice("num_layers",
#                        values=[0, 1, 2, 3, 4, 5, ],
#                        default=2)
#
# magnitude = hp.Choice("magnitude",
#                       values=[0, 2, 4, 6, 8, 10, 12],
#                       default=10)
# embedding_size = hp.Choice("emb_size", values=[512, 1024,2048,4096,8192], default=512)

# learning_rate = hp.Float("lr",
#                          min_value=0.00005,
#                          max_value=0.0005,
#                          sampling="log",
#                          default=0.0001)



# variant = hp.Choice("variant", ["B0", "B1", "B2", "B3","B4"], default="B0")
# random_erasing_probability = hp.Choice("random_erasing_probability",
#                                        values=[0.0,0.05,0.1, 0.2, 0.3, 0.4, 0.5],
#                                        default=0.3)
# autoaugment = hp.Choice("autoaugment",
#                         values=["No", "v0", "reduced_cifar10", "svhn", "reduced_imagenet"],
#                         default="v0")
# learning_rate = hp.Choice("lr",
#                           values=[0.001, 0.0001, 0.00001, 0.000001],
#                           default=0.0001)
# decay_steps = hp.Choice("decay_steps",
#                         values=[100, 500, 1000, 5000],
#                         default=1000)
# decay_rate = hp.Choice("decay_rate",
#                        values=[0.8, 0.91, 0.96],
#                        default=0.96)
# nux_strength = hp.Choice("nux_strength",
#                          values=[0.1, 0.2, 0.3, 0.4, 0.5],
#                          default=0.3)
simclr = hp.Choice("simclr",
                   values=["v2", "v1"],
                   default="v0")
jitter_stength = hp.Choice("jitter_stength",
                           values=[0.01, 0.1, 0.3, 0.5, 0.8],
                           default=0.1)
tuner = kt.RandomSearch(
    MyHyperModel(),
    objective=kt.Objective("f1_score", "max"),
    seed=42,
    max_trials=200,
    hyperparameters=hp,
    tune_new_entries=False,
    executions_per_trial=2,
    #distribution_strategy=tf.distribute.MirroredStrategy(),
    overwrite=True,
    directory=log_dir + '/model',
    project_name=name,
)

# #
# tuner = kt.Hyperband(
#     MyHyperModel(),
#     objective=kt.Objective("val_loss", "min"),
#     seed=42,
#     max_epochs=200,
#     hyperband_iterations=1,
#     hyperparameters=hp,
#     factor=3,
#     tune_new_entries=False,
#     executions_per_trial=2,
#     #distribution_strategy=tf.distribute.MirroredStrategy(),
#     overwrite=True,
#     directory=log_dir + '/model',
#     project_name=name,
# )


# tuner = kt.BayesianOptimization(
#     MyHyperModel(),
#     objective=kt.Objective("val_loss", "min"),
#     seed=42,
#     max_trials=300,
#     hyperparameters=hp,
#     tune_new_entries=False,
#     executions_per_trial=2,
#     #distribution_strategy=tf.distribute.MirroredStrategy(),
#     overwrite=True,
#     directory=log_dir + '/model',
#     project_name=name,
# )



tuner.search_space_summary()
tuner.search(train_ds, epochs=100, val_ds=val_ds, callbacks=[tsc,tbc,esc])

