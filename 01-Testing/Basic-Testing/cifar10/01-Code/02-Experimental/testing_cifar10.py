import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
import cv2
from tensorflow import keras
import tensorflow_similarity as tfsim
tfsim.utils.tf_cap_memory()
import multiprocessing
print("TensorFlow:", tf.__version__)
print("TensorFlow Similarity:", tfsim.__version__)
from itertools import repeat
import random
import glob
import argparse
import json
def resize(img, IMG_SIZE = 300):
    with tf.device("/cpu:0"):
        img = tf.cast(img, dtype="int32")
        img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
        return img
def process_image(image, size):
    image = resize(image, size)
    return image
def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def worker_tf_write(images,labels, tf_record_path, size, image_quality, tf_record_options):
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), image_quality]
    tf_record_options = tf.io.TFRecordOptions(compression_type=tf_record_options)

    with tf.io.TFRecordWriter(tf_record_path, tf_record_options) as tf_writer:
        for i, image in enumerate(images):
            image = process_image(image, size)
            is_success, im_buf_arr = cv2.imencode(".jpg", image.numpy(), encode_param)

            if is_success:


                label_number = labels.take(i)

                image_raw = im_buf_arr.tobytes()
                row = tf.train.Example(features=tf.train.Features(feature={
                    'label': _int64_feature(label_number),
                    'image_raw': _bytes_feature(image_raw)
                }))

                tf_writer.write(row.SerializeToString())
            else:
                print("Error processing ")



def master_tf_write(images, labels,record_path, size, image_quality, tf_record_options):
    cpu_core = multiprocessing.cpu_count()

    p = multiprocessing.Pool(cpu_core)
    worker_tf_write(images,labels,record_path,size,image_quality,tf_record_options)
'''
    results = p.starmap(worker_tf_write,
                        zip(images, labels, repeat(record_path),repeat(size), repeat(image_quality),
                            repeat(tf_record_options),
                            ))
    p.close()
    p.join()
'''


def create_tf_record(images,labels, record_path, identifier, size=300, image_quality=90,
                     tf_record_options=None):
    print("creating " + identifier + " records")

    record_path = (record_path + identifier + "-" + ".tfrecord")

    master_tf_write(images, labels,record_path, size, image_quality, tf_record_options)
IMAGE_SIZE = [300, 300]

def decode_image(image):
    image = tf.image.decode_jpeg(image, channels=3)
    image = tf.cast(image, tf.float32)
    image = tf.reshape(image, [*IMAGE_SIZE, 3])
    return image
def read_tfrecord(example, labeled=True):
    tfrecord_format = (
        {
            "label": tf.io.FixedLenFeature([], tf.int64),
            "image_raw": tf.io.FixedLenFeature([], tf.string),

        }
        if labeled
        else {"image_raw": tf.io.FixedLenFeature([], tf.string),}
    )
    example = tf.io.parse_single_example(example, tfrecord_format)
    image = decode_image(example["image_raw"])
    if labeled:
        label = tf.cast(example["label"], tf.int32)
        return image, label
    return image


(x_train, y_train), (x_test, y_test) = keras.datasets.cifar10.load_data()
record_path = '/scratch/dguthruf/downloads/cifar10_300x_300/'
identifier='test'
#create_tf_record(x_test,y_test,record_path,identifier)



