import os
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
import subprocess
import random
import tensorflow as tf
import tensorflow_datasets as tfds
from tensorboard.plugins.hparams import api as hp
from tensorflow.keras import layers
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss # evaluate validation loss on known and unknown classes
from tensorflow_similarity.losses import CircleLoss,MultiSimilarityLoss # specialized similarity loss
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.visualization import projector  # allows to interactively explore the samples in 2D space
from tensorflow_similarity.visualization import viz_neigbors_imgs  # Neigboors vizualisation
from tensorflow_similarity.visualization import confusion_matrix  # matching performance
from tensorflow_similarity.utils import tf_cap_memory
import tensorflow_model_analysis as tfma
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
def resize(img, label):
    IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
    with tf.device("/cpu:0"):
        img = tf.cast(img, dtype="int32")
        img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
        return img, label


def get_data(num_classes=10,examples_per_class = 4,dataset='cifar10'):
    print(num_classes)
    print(examples_per_class)
    ds = tfds.load(dataset, split='train', shuffle_files=True, with_info=True)
    labels = ds[1].features['label'].names
    my_dict = dict(zip(range(len(labels)),labels))
    total_classes = len(labels)
    train_cls = random.sample(range(total_classes), k=num_classes)
    classes_per_batch = num_classes
    class_mappings = {k: my_dict[k] for k in train_cls}

    train_ds = TFDatasetMultiShotMemorySampler(dataset,
                                               splits='train',
                                               examples_per_class_per_batch=examples_per_class,
                                               classes_per_batch=classes_per_batch,
                                               preprocess_fn=resize,
                                               class_list=train_cls)  # We filter train data to only keep the train classes.

    # use the test split for indexing and querying
    test_ds = TFDatasetMultiShotMemorySampler(dataset,
                                              splits='test',
                                              total_examples_per_class=400,
                                              classes_per_batch=total_classes,
                                              preprocess_fn=resize)
    data = [train_ds,test_ds,class_mappings,train_cls,total_classes,num_classes]
    return data

def get_callbacks(data, log_dir,num_targets=200,num_queries = 400, k = 4):
    train_ds,test_ds,class_mappings,train_cls,total_classes,num_classes = data

    # Setup EvalCallback by splitting the test data into targets and queries.
    queries_x, queries_y = test_ds.get_slice(0, num_queries)
    targets_x, targets_y = test_ds.get_slice(num_queries, num_targets)

    tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=k,
                       metrics=['f1score','binary_accuracy'],
                        tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                      )

    # Setup SplitValidation callback.
    val_loss = SplitValidationLoss(queries_x, queries_y, targets_x, targets_y,
                                   metrics=['binary_accuracy'], known_classes=tf.constant(train_cls),
                                   k=k,
                                   tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                                   )

    # Adding the Tensorboard callback to track metrics in tensorboard.
    tbc = TensorBoard(log_dir=log_dir) # uncomment if you want to track in tensorboard
    if total_classes==num_classes:
        callbacks = [
            #val_loss,
            tsc,
            tbc, # uncomment if you want to track in tensorboard
        ]
    else:
        callbacks = [
            val_loss,
            tsc,
            tbc, # uncomment if you want to track in tensorboard
        ]
    return callbacks

def get_model(data,callbacks,embedding_size=128,val_steps=50,epochs = 5,steps_per_epoch=100, loss = CircleLoss(gamma = 256),learning_rate = 0.0001):
    train_ds, test_ds, class_mappings, train_cls, total_classes, num_classes = data
    # building model
    model = EfficientNetSim(train_ds.example_shape, embedding_size)

    # compiling and training
    model.compile(optimizer=Adam(learning_rate), loss=loss)
    history = model.fit(train_ds,
                        epochs=epochs,
                        steps_per_epoch=steps_per_epoch,
                        validation_data=test_ds,
                        validation_steps=val_steps,
                        callbacks=callbacks)
    return history
def obvioius(dataset='cifar10'):
    HP_NUM_CLASSES = hp.HParam('num_classes', hp.Discrete([10,9]))
    HP_EX_PER_CLASS = hp.HParam('ex_per_class', hp.Discrete([4,8]))

    HP_NUM_TARGETS = hp.HParam('num_targets', hp.Discrete([200, 400]))
    HP_NUM_NEIGHBOURS = hp.HParam('num_neighbours', hp.Discrete([2, 4]))

    session_num = 0
    HPs = [HP_NUM_CLASSES, HP_EX_PER_CLASS,HP_NUM_TARGETS,HP_NUM_NEIGHBOURS]
    for hp0 in HPs[0].domain.values:
        for hp1 in HPs[1].domain.values:
            for hp2 in HPs[2].domain.values:
                for hp3 in HPs[3].domain.values:
                        hparams = {
                            HPs[0]: hp0,
                            HPs[1]: hp1,
                            HPs[2]: hp2,
                            HPs[3]: hp3,

                        }

                        run_name = "obvious/run-{}".format(session_num)
                        print('--- Starting trial: %s' % run_name)
                        print({h.name: hparams[h] for h in hparams})
                        with tf.summary.create_file_writer('logs/'+run_name+'/hparam_tuning/').as_default():
                            hp.hparams(hparams)  # record the values used in this trial
                            data = subprocess.check_output(['python','/tmp/pycharm_project_58/testing/cifar10/RunGS.py', '%d'%hp0, '%d'%hp1, '%s'%dataset])
                            callbacks = get_callbacks(data,'logs/'+run_name+'/metrics/',num_targets=hp2,k=hp3)
                            history = get_model(data,callbacks)
                            tf.summary.scalar('B-Accuracy Known', max(history.history['binary_accuracy']), step=1)
                            tf.summary.scalar('F1-Score', max(history.history['f1score']), step=1)
                            try:
                                tf.summary.scalar('B-Accuracy Unknown', max(history.history['binary_accuracy_unknown_classes']), step=1)
                            except:
                                pass
                        tf.keras.backend.clear_session()
                        del data,callbacks,history
                        session_num += 1


# class MyTuner(kt.Tuner):
#     def run_trial(self, trial, train_ds):
#         hp = trial.hyperparameters
#
#         # Hyperparameters can be added anywhere inside `run_trial`.
#         # When the first trial is run, they will take on their default values.
#         # Afterwards, they will be tuned by the `Oracle`.
#         train_ds = train_ds.batch(hp.Int("batch_size", 32, 128, step=32, default=64))
#         embedding_size = hp.Int("embedding_size",64,128,256,default=128)
#         model = self.hypermodel.build(trial.hyperparameters)
#         loss = hp.Discrete('loss',CircleLoss(gamma=256),MultiSimilarityLoss(),default=CircleLoss(gamma=256))
#         lr = hp.Float("learning_rate", 1e-4, 1e-2, sampling="log", default=1e-3)
#         optimizer = tf.keras.optimizers.Adam(lr)
#         epoch_loss_metric = tf.keras.metrics.Mean()
#         loss_fn = loss
#
#         # @tf.function
#         def run_train_step(data):
#             images = tf.dtypes.cast(data[0], "float32") / 255.0
#             labels = data[1]
#             with tf.GradientTape() as tape:
#                 logits = model(images)
#                 loss = loss_fn(labels, logits)
#                 # Add any regularization losses.
#                 if model.losses:
#                     loss += tf.math.add_n(model.losses)
#             gradients = tape.gradient(loss, model.trainable_variables)
#             optimizer.apply_gradients(zip(gradients, model.trainable_variables))
#             epoch_loss_metric.update_state(loss)
#             return loss
#
#         # `self.on_epoch_end` reports results to the `Oracle` and saves the
#         # current state of the Model. The other hooks called here only log values
#         # for display but can also be overridden. For use cases where there is no
#         # natural concept of epoch, you do not have to call any of these hooks. In
#         # this case you should instead call `self.oracle.update_trial` and
#         # `self.oracle.save_model` manually.
#         for epoch in range(2):
#             print("Epoch: {}".format(epoch))
#
#             self.on_epoch_begin(trial, model, epoch, logs={})
#             for batch, data in enumerate(train_ds):
#                 self.on_batch_begin(trial, model, batch, logs={})
#                 batch_loss = float(run_train_step(data))
#                 self.on_batch_end(trial, model, batch, logs={"loss": batch_loss})
#
#                 if batch % 100 == 0:
#                     loss = epoch_loss_metric.result().numpy()
#                     print("Batch: {}, Average Loss: {}".format(batch, loss))
#
#             epoch_loss = epoch_loss_metric.result().numpy()
#             self.on_epoch_end(trial, model, epoch, logs={"loss": epoch_loss})
#             epoch_loss_metric.reset_states()
#
#
# tuner = MyTuner(
#     oracle=kt.oracles.BayesianOptimization(
#         objective=kt.Objective("loss", "min"), max_trials=2
#     ),
#     hypermodel=build_model,
#     directory="results",
#     project_name="mnist_custom_training",
# )

if __name__ == '__main__':
    exit(obvioius())