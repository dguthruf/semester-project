import gc
import os
import sys
import numpy as np
import tensorflow as tf
import sys
sys.path.append( '/home/dguthruf/NUX-Regressor/nn')
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
import random
import tensorflow_datasets as tfds
import pickle as pkl
from tensorboard.plugins.hparams import api as hp
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
from tensorflow_similarity.losses import CircleLoss, MultiSimilarityLoss  # specialized similarity loss
from tensorflow_similarity.samplers import MultiShotMemorySampler
from time import time

from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog


def train(log_dir, ds_info, hparams, train_ds, test_ds,val_steps=100,num_queries=400):
    total_classes = ds_info.features["label"].num_classes
    train_cls = random.sample(range(total_classes), k=hparams['HP_NUM_CLASSES'])

    # Setup EvalCallback by splitting the test data into targets and queries.
    queries_x, queries_y = test_ds.get_slice(0, num_queries)
    targets_x, targets_y = test_ds.get_slice(num_queries, hparams['HP_NUM_TARGETS'])

    tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=hparams['HP_NUM_NEIGHBOURS'],
                       metrics=['f1score', 'binary_accuracy'],
                       tb_logdir=log_dir + '/tsc'  # uncomment if you want to track in tensorboard
                       )

    # Setup SplitValidation callback.
    val_loss = SplitValidationLoss(queries_x, queries_y, targets_x, targets_y,
                                   metrics=['binary_accuracy'], known_classes=tf.constant(train_cls),
                                   k=hparams['HP_NUM_NEIGHBOURS'],
                                   tb_logdir=log_dir + '/val_loss'  # uncomment if you want to track in tensorboard
                                   )

    # Adding the Tensorboard callback to track metrics in tensorboard.
    tbc = TensorBoard(log_dir=log_dir+'/tbc',
                      histogram_freq=1,
                      profile_batch=(100,200))  # uncomment if you want to track in tensorboard

    if total_classes == hparams['HP_NUM_CLASSES']:
        callbacks = [
            # val_loss,
            tsc,
            tbc,  # uncomment if you want to track in tensorboard
        ]
    else:
        callbacks = [
            val_loss,
            tsc,
            tbc,  # uncomment if you want to track in tensorboard
        ]

    if hparams['HP_LOSS'] == 'Circle':
        loss = CircleLoss(gamma=hparams['HP_GAMMA'])
    elif hparams['HP_LOSS'] == 'Multi':
        loss = MultiSimilarityLoss(distance=hparams['HP_DISTANCE'])

    if hparams['HP_AUGMENTER']=='Basic':
        model = EfficientNetSim(train_ds.example_shape, hparams['HP_EMBEDDING_SIZE'], trainable=hparams['HP_MODE'])
    else:
        model = EfficientNetSim(train_ds.example_shape, hparams['HP_EMBEDDING_SIZE'], trainable=hparams['HP_MODE'],augmentation=None)

    '''
    elif hparams['HP_AUGMENTER']=='gpu':
        aug = operations.gpu_augmenter(in_shape=train_ds.example_shape,brightness=0.8,jitter=0.6,scale=0.9,
                                 ratio=0.8,fill_scale=0.8,fill_ratio=0.4,strength=1)
        model = EfficientNetSim(train_ds.example_shape, hparams['HP_EMBEDDING_SIZE'], trainable=hparams['HP_MODE'],augmentation=aug)
    '''




    # compiling and training
    model.compile(optimizer=Adam(hparams['HP_LEARNING_RATE']), loss=loss,steps_per_execution=10)
    model.reset_index()
    history = model.fit(train_ds,
                        epochs=hparams['HP_EPOCHS'],
                        steps_per_epoch=hparams['HP_STEPS_PER_EPOCH'],
                        validation_data=test_ds,
                        validation_steps=val_steps,
                        callbacks=callbacks)
    del queries_x,queries_y,targets_y,targets_x,tsc,val_loss,tbc,model,callbacks,train_ds,test_ds
    gc.collect()
    tf.keras.backend.clear_session()
    return history


def get_data(ds_info, num_classes=2, examples_per_class=4, steps_per_epoch=100):
    total_classes = ds_info.features["label"].num_classes
    labels = ds_info.features["label"].names
    my_dict = dict(zip(range(len(labels)), labels))
    train_cls = random.sample(range(total_classes), k=num_classes)

    with open("/scratch/dguthruf/downloads/cifar10/train.pkl", "rb") as f:
        x_train, y_train = pkl.load(f)
    with open("/scratch/dguthruf/downloads/cifar10/test.pkl", "rb") as f:
        x_test, y_test = pkl.load(f)


    train_ds = MultiShotMemorySampler(x_train, y_train,
                                      classes_per_batch=min(total_classes,num_classes),
                                      examples_per_class_per_batch=examples_per_class,
                                      class_list=train_cls,  # Only use the first 6 classes for training.
                                      steps_per_epoch=steps_per_epoch)
    # We filter train data to only keep the train classes.
    test_ds = MultiShotMemorySampler(x_test, y_test,
                                     classes_per_batch=total_classes,
                                     total_examples_per_class=100,
                                     )
    return train_ds, test_ds


def hparam_wrap(ds_info,hparams_dict,name,train_ds, test_ds, dataset):
    session_num = 0
    for ez in hparams_dict['HP_EMBEDDING_SIZE'].domain.values:
        for nc in hparams_dict['HP_NUM_CLASSES'].domain.values:
            for epc in hparams_dict['HP_EXAMPLES_PER_CLASS'].domain.values:
                for spe in hparams_dict['HP_STEPS_PER_EPOCH'].domain.values:
                    for nt in hparams_dict['HP_NUM_TARGETS'].domain.values:
                        for l in hparams_dict['HP_LOSS'].domain.values:
                            for m in hparams_dict['HP_MODE'].domain.values:
                                for a in hparams_dict['HP_AUGMENTER'].domain.values:
                                    for g in hparams_dict['HP_GAMMA'].domain.values:
                                        for lr in hparams_dict['HP_LEARNING_RATE'].domain.values:
                                            for e in hparams_dict['HP_EPOCHS'].domain.values:
                                                for nn in hparams_dict['HP_NUM_NEIGHBOURS'].domain.values:
                                                    for d in hparams_dict['HP_DISTANCE'].domain.values:
                                                        hparams = {
                                                            'HP_EMBEDDING_SIZE': ez,
                                                            'HP_NUM_CLASSES': nc,
                                                            'HP_EXAMPLES_PER_CLASS': epc,
                                                            'HP_STEPS_PER_EPOCH': spe,
                                                            'HP_NUM_TARGETS': nt,
                                                            'HP_LOSS': l,
                                                            'HP_MODE': m,
                                                            'HP_AUGMENTER': a,
                                                            'HP_GAMMA': g,
                                                            'HP_LEARNING_RATE': lr,
                                                            'HP_EPOCHS': e,
                                                            'HP_NUM_NEIGHBOURS': nn,
                                                            'HP_DISTANCE': d

                                                        }
                                                        run_name = "run-%d" % session_num
                                                        print('--- Starting trial: %s' % run_name)
                                                        print({h: hparams[h] for h in hparams})

                                                        log_dir = os.path.join(name, dataset,
                                                                               run_name,
                                                                               )
                                                        if len(hparams_dict['HP_NUM_CLASSES'].domain.values)>1 or len(hparams_dict['HP_EXAMPLES_PER_CLASS'].domain.values)>1:
                                                            del train_ds,test_ds
                                                            gc.collect()
                                                            train_ds,test_ds= get_data(ds_info,num_classes=nc,examples_per_class=epc,steps_per_epoch=spe)

                                                        run_hparam(log_dir, hparams, hparams_dict,
                                                                   train_ds,
                                                                   test_ds,
                                                                   ds_info)
                                                        session_num += 1
                                                        tf.keras.backend.clear_session()

def run_hparam(log_dir, hparams, hparams_dict, train_ds, test_ds,ds_info):
    with tf.summary.create_file_writer(log_dir+'/HParam').as_default():
        hp.hparams_config(
            hparams=list(hparams_dict.values()),
            metrics=[hp.Metric('f1score', display_name='F1Score'),
                     hp.Metric('binary_accuracy', display_name='Binary Accuracy'),
                     hp.Metric('binary_accuracy_unknown_classes',display_name='Binary Accuracy Unkown Classes')]
        )
        # hp.hparams(hparams)  # record the values used in this trial
        hp.hparams({hparams_dict[h]: hparams[h] for h in hparams_dict.keys()})
        history = train(log_dir, ds_info, hparams, train_ds, test_ds)
        tf.summary.scalar('binary_accuracy', history.history['binary_accuracy'][-1], step=1)
        tf.summary.scalar('f1score', history.history['f1score'][-1], step=1)
        try:
            tf.summary.scalar('binary_accuracy_unknown_classes', history.history['binary_accuracy_unknown_classes'][-1], step=1)
        except:
            pass
        del history,hparams,hparams_dict,train_ds,test_ds,ds_info
        gc.collect()
        tf.keras.backend.clear_session()

def get_params():
    hparams_dict = {
        'HP_EMBEDDING_SIZE': hp.HParam('embedding_size', hp.Discrete([256])),
        'HP_NUM_CLASSES': hp.HParam('num_classes', hp.Discrete([10])),
        'HP_EXAMPLES_PER_CLASS': hp.HParam('examples_per_class', hp.Discrete([8])),
        'HP_STEPS_PER_EPOCH': hp.HParam('steps_per_epoch', hp.Discrete([4000])),
        'HP_NUM_TARGETS': hp.HParam('num_targets', hp.Discrete([200])),
        'HP_LOSS': hp.HParam('loss', hp.Discrete(['Circle'])),
        'HP_DISTANCE': hp.HParam('distance', hp.Discrete(['cosine'])),
        'HP_GAMMA': hp.HParam('gamma', hp.Discrete([256])),
        'HP_MODE': hp.HParam('mode', hp.Discrete(['partial'])),
        'HP_AUGMENTER': hp.HParam('augmenter', hp.Discrete(['None','Basic'])),
        'HP_NUM_NEIGHBOURS': hp.HParam('num_neighbours', hp.Discrete([2])),
        'HP_LEARNING_RATE': hp.HParam('learning_rate', hp.Discrete([0.001,0.0001,0.01,0.00001])),
        'HP_EPOCHS': hp.HParam('num_epochs', hp.Discrete([10])),
    }
    return hparams_dict

def main(name='%d'%(time()),dataset='cifar10'):
    _, ds_info = tfds.load(
        dataset, split=["train", "test"], with_info=True, as_supervised=True)
    hparams_dict = get_params()
    if len(hparams_dict['HP_NUM_CLASSES'].domain.values)==1 and len(hparams_dict['HP_EXAMPLES_PER_CLASS'].domain.values)==1:
        train_ds,test_ds=get_data(ds_info,num_classes= hparams_dict['HP_NUM_CLASSES'].domain.values[0],
                                  examples_per_class=hparams_dict['HP_EXAMPLES_PER_CLASS'].domain.values[0],
                                  steps_per_epoch=hparams_dict['HP_STEPS_PER_EPOCH'].domain.values[0])

    else:
        train_ds=0
        test_ds =0
    hparam_wrap(ds_info,hparams_dict,name,train_ds,test_ds,dataset)
if __name__ == '__main__':
    exit(main())
