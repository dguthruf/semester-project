import random

from matplotlib import pyplot as plt
from mpl_toolkits import axes_grid1
import numpy as np
import os
import tensorflow as tf
from tensorflow import keras
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
from tensorflow.keras.models import Sequential
from tensorflow.keras import layers
import tensorflow_similarity as tfsim
from tensorflow.keras.applications import EfficientNetB0
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
from tensorflow_similarity.types import FloatTensor, IntTensor
from typing import Tuple

tfsim.utils.tf_cap_memory()

print("TensorFlow:", tf.__version__)
print("TensorFlow Similarity:", tfsim.__version__)
strategy = tf.distribute.MirroredStrategy()
# This determines the number of classes used during training.
# Here we are using all the classes.

num_known_classes = 10
class_list = random.sample(population=range(10), k=num_known_classes)

classes_per_batch = 10
# Passing multiple examples per class per batch ensures that each example has
# multiple positive pairs. This can be useful when performing triplet mining or
# when using losses like `MultiSimilarityLoss` or `CircleLoss` as these can
# take a weighted mix of all the positive pairs. In general, more examples per
# class will lead to more information for the positive pairs, while more classes
# per batch will provide more varied information in the negative pairs. However,
# the losses compute the pairwise distance between the examples in a batch so
# the upper limit of the batch size is restricted by the memory.
examples_per_class_per_batch = 8

print(
    "Batch size is: "
    f"{min(classes_per_batch, num_known_classes) * examples_per_class_per_batch}"
)
aug_num_examples_per_batch = 18  #@param {type:"slider", min:18, max:512}
aug_num_augmentations_per_example = 1 #@param {type:"slider", min:1, max:3}

data_augmentation = tf.keras.Sequential([
    layers.experimental.preprocessing.RandomRotation(0.12),
    layers.experimental.preprocessing.RandomZoom(0.25),
])


def augmenter(x:FloatTensor,y:IntTensor,
        examples_per_class: int,
        is_warmup: bool,
        stddev=0.025)-> Tuple[FloatTensor, IntTensor]:
    """Image augmentation function.

    Args:
        X: FloatTensor representing the example features.
        y: IntTensor representing the class id. In this case
           the example index will be used as the class id.
        examples_per_class: The number of examples per class.
           Not used here.
        is_warmup: If True, the training is still in a warm
           up state. Not used here.
        stddev: Sets the amount of gaussian noise added to
           the image.
    """
    _ = examples_per_class
    _ = is_warmup
    inputs = keras.layers.Input((32, 32, 3))
    aug = (data_augmentation(inputs))
    aug = aug + tf.random.normal(tf.shape(aug), stddev=stddev)
    x = tf.concat((x, aug), axis=0)
    y = tf.concat((y, y), axis=0)
    idxs = tf.range(start=0, limit=tf.shape(x)[0])
    idxs = tf.random.shuffle(idxs)
    x = tf.gather(x, idxs)
    y = tf.gather(y, idxs)
    return x, y


print(" Create Training Data ".center(34, "#"))
train_ds = tfsim.samplers.TFDatasetMultiShotMemorySampler(
    "cifar10",
    classes_per_batch=min(classes_per_batch, num_known_classes),
    splits="train",
    steps_per_epoch=4000,
    augmenter=augmenter,
    examples_per_class_per_batch=examples_per_class_per_batch,
    class_list=class_list,
)

print("\n" + " Create Validation Data ".center(34, "#"))
val_ds = tfsim.samplers.TFDatasetMultiShotMemorySampler(
    "cifar10",
    classes_per_batch=classes_per_batch,
    splits="test",
    augmenter=augmenter,
    total_examples_per_class=100,
)
queries_x, queries_y = val_ds.get_slice(0, 400)
targets_x, targets_y = val_ds.get_slice(400, 400)

tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=2,
                   metrics=['f1score', 'binary_accuracy'] # uncomment if you want to track in tensorboard
                   )
val_loss = SplitValidationLoss(queries_x, queries_y, targets_x, targets_y,
                               metrics=['binary_accuracy'], known_classes=tf.constant(class_list),
                               k=2 # uncomment if you want to track in tensorboard
                               )
if classes_per_batch == num_known_classes:
    callbacks = [
        # val_loss,
        tsc,
    ]
else:
    callbacks = [
        val_loss,
        tsc,
    ]
num_cols = num_rows = 5
# Get the first 25 examples.
x_slice, y_slice = train_ds.get_slice(begin=0, size=num_cols * num_rows)

# fig = plt.figure(figsize=(6.0, 6.0))
# grid = axes_grid1.ImageGrid(fig, 111, nrows_ncols=(num_cols, num_rows), axes_pad=0.1)
#
# for ax, im, label in zip(grid, x_slice, y_slice):
#     ax.imshow(im)
#     ax.axis("off")

img_augmentation = Sequential(
    [
        layers.RandomRotation(factor=0.15),
        layers.RandomTranslation(height_factor=0.1, width_factor=0.1),
        layers.RandomFlip(),
        layers.RandomContrast(factor=0.1),
    ],
    name="img_augmentation",
)
def unfreeze_model(model):
    # We unfreeze the top 20 layers while leaving BatchNorm layers frozen
    for layer in model.layers[-20:]:
        if not isinstance(layer, layers.BatchNormalization):
            layer.trainable = True

embedding_size = 256
mode = 'basic'
inputs = keras.layers.Input((32, 32, 3))
if mode == 'basic':
    epochs = 3
    learning_rate = 0.002
    val_steps = 50
    x = keras.layers.Rescaling(scale=1.0 / 255)(inputs)
    x = keras.layers.Conv2D(64, 3, activation="relu")(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.Conv2D(128, 3, activation="relu")(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.MaxPool2D((4, 4))(x)
    x = keras.layers.Conv2D(256, 3, activation="relu")(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.Conv2D(256, 3, activation="relu")(x)
    x = keras.layers.GlobalMaxPool2D()(x)
elif mode == 'basic_aug':
    epochs = 3
    learning_rate = 0.002
    val_steps = 50

    x = keras.layers.Rescaling(scale=1.0 / 255)(inputs)
    x = img_augmentation(x)
    x = keras.layers.Conv2D(64, 3, activation="relu")(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.Conv2D(128, 3, activation="relu")(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.MaxPool2D((4, 4))(x)
    x = keras.layers.Conv2D(256, 3, activation="relu")(x)
    x = keras.layers.BatchNormalization()(x)
    x = keras.layers.Conv2D(256, 3, activation="relu")(x)
    x = keras.layers.GlobalMaxPool2D()(x)
elif mode == 'eff_aug':
    epochs = 40
    learning_rate = 0.001
    x = img_augmentation(inputs)
    x = EfficientNetB0(include_top=True,input_shape=train_ds.example_shape, weights=None)(x)
elif mode =='eff':
    epochs = 40
    learning_rate = 0.001
    x = keras.layers.Rescaling(scale=1.0 / 255)(inputs)
    x = EfficientNetB0(include_top=True,input_shape=train_ds.example_shape, weights=None)(x)
elif mode =='eff_weights':
    epochs = 25
    learning_rate = 0.01
    x = img_augmentation(inputs)
    model = EfficientNetB0(include_top=False, input_tensor=x, weights="imagenet")
    model.trainable = False
    x = layers.GlobalAveragePooling2D(name="avg_pool")(model.output)
    x = layers.BatchNormalization()(x)
elif mode =='eff_unfreeze':
    epochs = 10
    learning_rate = 0.0001
    x = img_augmentation(inputs)
    model = EfficientNetB0(include_top=False, input_tensor=x, weights="imagenet")
    model.trainable = False
    x = layers.GlobalAveragePooling2D(name="avg_pool")(model.output)
    unfreeze_model(model)

outputs = tfsim.layers.MetricEmbedding(embedding_size)(x)

# building model
model = tfsim.models.SimilarityModel(inputs, outputs)
model.summary()
val_steps = 50

# init similarity loss
loss = tfsim.losses.MultiSimilarityLoss()

# compiling and training
model.compile(
    optimizer=keras.optimizers.Adam(learning_rate), loss=loss,
)
history = model.fit(
    train_ds, epochs=epochs, validation_data=val_ds, validation_steps=val_steps,callbacks=callbacks
)

x_index, y_index = val_ds.get_slice(begin=0, size=200)
model.reset_index()
model.index(x_index, y_index, data=x_index)


x_train, y_train = train_ds.get_slice(begin=0, size=1000)
calibration = model.calibrate(
    x_train,
    y_train,
    calibration_metric="f1",
    matcher="match_nearest",
    extra_metrics=["precision", "recall", "binary_accuracy"],
    verbose=1,
)

num_neighbors = 5
labels = [
    "Airplane",
    "Automobile",
    "Bird",
    "Cat",
    "Deer",
    "Dog",
    "Frog",
    "Horse",
    "Ship",
    "Truck",
    "Unknown",
]
class_mapping = {c_id: c_lbl for c_id, c_lbl in zip(range(11), labels)}

x_display, y_display = val_ds.get_slice(begin=200, size=10)
# lookup nearest neighbors in the index
nns = model.lookup(x_display, k=num_neighbors)

# # display
# for idx in np.argsort(y_display):
#     tfsim.visualization.viz_neigbors_imgs(
#         x_display[idx],
#         y_display[idx],
#         nns[idx],
#         class_mapping=class_mapping,
#         fig_size=(16, 2),
#     )

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))
x = calibration.thresholds["distance"]

ax1.plot(x, calibration.thresholds["precision"], label="precision")
ax1.plot(x, calibration.thresholds["recall"], label="recall")
ax1.plot(x, calibration.thresholds["f1"], label="f1 score")
ax1.legend()
ax1.set_title("Metric evolution as distance increase")
ax1.set_xlabel("Distance")
ax1.set_ylim((-0.05, 1.05))

ax2.plot(calibration.thresholds["recall"], calibration.thresholds["precision"])
ax2.set_title("Precision recall curve")
ax2.set_xlabel("Recall")
ax2.set_ylabel("Precision")
ax2.set_ylim((-0.05, 1.05))
plt.show()

cutpoint = "optimal"

# This yields 100 examples for each class.
# We defined this when we created the val_ds sampler.
x_confusion, y_confusion = val_ds.get_slice(0, -1)

matches = model.match(x_confusion, cutpoint=cutpoint, no_match_label=10)
tfsim.visualization.confusion_matrix(
    matches,
    y_confusion,
    labels=labels,
    title="Confusion matrix for cutpoint:%s" % cutpoint,
    normalize=False,
)
def plot_hist(hist):
    plt.plot(hist.history["binary_accuracy"])
    plt.plot(hist.history["f1score"])
    try:
        plt.plot(hist.history["binary_accuracy_unknown_classes"])
        plt.legend(["binary_accuracy", "f1","binary_accuracy_unknown"], loc="upper left")
    except:
        plt.legend(["binary_accuracy", "f1"], loc="upper left")

    plt.title("model loss")
    plt.ylabel("metrics")
    plt.xlabel("epoch")
    plt.show()


plot_hist(history)

idx_no_match = np.where(np.array(matches) == 10)
no_match_queries = x_confusion[idx_no_match]
if len(no_match_queries):
    plt.imshow(no_match_queries[0])
else:
    print("All queries have a match below the distance threshold.")


# # Each class in val_ds was restricted to 100 examples.
# num_examples_to_clusters = 1000
# thumb_size = 96
# plot_size = 800
# vx, vy = val_ds.get_slice(0, num_examples_to_clusters)
#
# tfsim.visualization.projector(
#  model.predict(vx),
#  labels=vy,
#  images=vx,
#  class_mapping=class_mapping,
#  image_size=thumb_size,
#  plot_size=plot_size,
# )