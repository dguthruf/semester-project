import matplotlib.pyplot as plt

import keras
import keras.backend as K
from keras.datasets import cifar100
from keras.models import Sequential
from keras.layers import Dense, Dropout, BatchNormalization, Flatten, GlobalMaxPooling2D, GlobalAveragePooling2D
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from sklearn.metrics import accuracy_score
import cv2
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss # evaluate validation loss on known and unknown classes
from tensorflow_similarity.losses import CircleLoss # specialized similarity loss
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.visualization import projector  # allows to interactively explore the samples in 2D space
from tensorflow_similarity.visualization import viz_neigbors_imgs  # Neigboors vizualisation
from tensorflow_similarity.visualization import confusion_matrix  # matching performance
from tensorflow_similarity.utils import tf_cap_memory
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras import layers
import tensorflow_datasets as tfds
from imgaug import augmenters as iaa
import imgaug as ia
from get_tfrecords import get_sampler
import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"]="cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
from tensorflow.keras.callbacks import TensorBoard
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
import tensorflow_similarity as tfsim
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from imgaug import augmenters as iaa
import random
tfsim.utils.tf_cap_memory()
from mpl_toolkits import axes_grid1
import random
from time import time
from matplotlib import pyplot as plt
from IPython.display import display, Markdown
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss # evaluate validation loss on known and unknown classes
from tensorflow_similarity.losses import CircleLoss # specialized similarity loss
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.visualization import projector  # allows to interactively explore the samples in 2D space
from tensorflow_similarity.visualization import viz_neigbors_imgs  # Neigboors vizualisation
from tensorflow_similarity.visualization import confusion_matrix  # matching performance
from tensorflow_similarity.utils import tf_cap_memory
import tensorflow_similarity as tfsim
tfsim.utils.tf_cap_memory() # Avoid GPU memory blow up
IMG_SIZE = 300 #@param {type:"integer"}

# preprocessing function that resizes images to ensure all images are the same shape
def resize(img, label):
    size = 300  # slightly larger than EfficienNetB0 size to allow random crops.
    with tf.device("/cpu:0"):
        img = tf.cast(img, dtype="int32")
        img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
        return img, label

training_classes = 10  # @param {type:"slider", min:1, max:37}
examples_per_class_per_batch = 2  # @param {type:"integer"}
train_cls = random.sample(range(10), k=training_classes)
classes_per_batch = max(10, training_classes)

print(f'Class IDs seen during training {train_cls}')

# use the train split for training
train_ds = TFDatasetMultiShotMemorySampler('cifar10',
                                       splits='train',
                                       examples_per_class_per_batch=examples_per_class_per_batch,
                                       classes_per_batch=classes_per_batch,
                                       preprocess_fn=resize,
                                       class_list=train_cls)  # We filter train data to only keep the train classes.

# use the test split for indexing and querying
test_ds = TFDatasetMultiShotMemorySampler('cifar10',
                                      splits='test',
                                      total_examples_per_class=20,
                                      classes_per_batch=classes_per_batch,
                                      preprocess_fn=resize)


num_targets = 200 #@param {type:"integer"}
num_queries = 300 #@param {type:"integer"}
k = 3 #@param {type:"integer"}
log_dir = 'logs/%d/' % (time())

# Setup EvalCallback by splitting the test data into targets and queries.
queries_x, queries_y = test_ds.get_slice(0, num_queries)
targets_x, targets_y = test_ds.get_slice(num_queries, num_targets)
tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, metrics=['f1'], k=k,
                   # tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                  )

# Setup SplitValidation callback.
val_loss = SplitValidationLoss(queries_x, queries_y, targets_x, targets_y,
                               metrics=['binary_accuracy'], known_classes=tf.constant(train_cls),
                               k=k,
                               #tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                               )

# Adding the Tensorboard callback to track metrics in tensorboard.
# tbc = TensorBoard(log_dir=log_dir) # uncomment if you want to track in tensorboard

callbacks = [
    #val_loss,
    tsc,
    # tbc # uncomment if you want to track in tensorboard
]


embedding_size = 128 #@param {type:"integer"}

# building model
model = EfficientNetSim(train_ds.example_shape, embedding_size,trainable='full')
#
# def plot_25(x, y):
#     """Plot the first 25 images."""
#     num_cols = num_rows = 5
#     fig = plt.figure(figsize=(6.0, 6.0))
#     grid = axes_grid1.ImageGrid(
#         fig=fig,
#         rect=111,
#         nrows_ncols=(num_cols, num_rows),
#         axes_pad=0.1,
#     )
#
#     for ax, im, label in zip(grid, x, y):
#         max_val = float(tf.math.reduce_max(im))
#         ax.imshow(im / max_val)
#         ax.axis("off")

epochs = 5 #@param {type:"integer"}
LR = 0.0001 #@param {type:"number"}
gamma = 256 #@param {type:"integer"} # Loss hyper-parameter. 256 works well here.
steps_per_epoch = 100
val_steps = 50


# init similarity loss
loss = CircleLoss(gamma=gamma)

# compiling and training
model.compile(optimizer=Adam(LR), loss=loss)
history = model.fit(train_ds,
                    epochs=epochs,
                    steps_per_epoch=steps_per_epoch,
                    validation_data=test_ds,
                    validation_steps=val_steps,
                    callbacks=callbacks)

































# shard_path = '/scratch/dguthruf/tensorflow_datasets/cifar10/3.0.2/sharded/'
# train_ds = get_sampler(shard_path,'train')
# test_ds = get_sampler(shard_path,'test')
#
# embedding_size = 128 #@param {type:"integer"}
#
# # building model
# model = EfficientNetSim((300,300,3), embedding_size)
#
# epochs = 5 #@param {type:"integer"}
# LR = 0.0001 #@param {type:"number"}
# gamma = 256 #@param {type:"integer"} # Loss hyper-parameter. 256 works well here.
# steps_per_epoch = 100
# val_steps = 50
#
#
# # init similarity loss
# loss = CircleLoss(gamma=gamma)
#
# # compiling and training
# model.compile(optimizer=Adam(LR), loss=loss)
# history = model.fit(train_ds,
#                     epochs=epochs,
#                     steps_per_epoch=steps_per_epoch,
#                     validation_data=test_ds,
#                     validation_steps=val_steps,
#                     )









# # parameters for data
# height = 224
# width = 224
# channels = 3
# input_shape = (height, width, channels)
# n_classes = 100
#
# # parameters for optimizers
# lr = 1e-3
#
# # Parameters for training
# epochs = 25
# batch_size = 8
#
# # parameters for callback functions
# es_patience = 10
# rlrop_patience = 5
# decay_rate = 0.5
#
# (X, y), (X_test, y_test) = keras.datasets.cifar100.load_data()
#
#
# print("The shape of X_train : ", X.shape)
# print("The shape of X_test : ", X_test.shape)
#
# fine_label_list =  ['apple', 'aquarium_fish', 'baby', 'bear', 'beaver', 'bed', 'bee', 'beetle', 'bicycle', 'bottle', 'bowl', 'boy', 'bridge', 'bus', 'butterfly',
#                     'camel', 'can', 'castle', 'caterpillar', 'cattle', 'chair', 'chimpanzee', 'clock', 'cloud', 'cockroach', 'couch', 'crab', 'crocodile', 'cup',
#                     'dinosaur', 'dolphin', 'elephant', 'flatfish', 'forest', 'fox', 'girl', 'hamster', 'house', 'kangaroo', 'computer_keyboard',
#                     'lamp', 'lawn_mower', 'leopard', 'lion', 'lizard', 'lobster', 'man', 'maple_tree', 'motorcycle', 'mountain', 'mouse', 'mushroom',
#                     'oak_tree', 'orange', 'orchid', 'otter', 'palm_tree', 'pear', 'pickup_truck', 'pine_tree', 'plain', 'plate', 'poppy', 'porcupine', 'possum',
#                     'rabbit', 'raccoon', 'ray', 'road', 'rocket', 'rose',
#                     'sea', 'seal', 'shark', 'shrew', 'skunk', 'skyscraper', 'snail', 'snake', 'spider', 'squirrel', 'streetcar', 'sunflower', 'sweet_pepper',
#                     'table', 'tank', 'telephone', 'television', 'tiger', 'tractor', 'train', 'trout', 'tulip', 'turtle',
#                     'wardrobe', 'whale', 'willow_tree', 'wolf', 'woman', 'worm']
#
# class_plotted = np.random.choice(range(n_classes), 5, replace = False)
# for i in range(len(class_plotted)):
#     image_samples = X[y.reshape(-1) == class_plotted[i]][:5]
#     fig, ax = plt.subplots(nrows = 1, ncols = 5,figsize = (8,8))
#     fig.suptitle("label : %d, class : %s" % (class_plotted[i], fine_label_list[class_plotted[i]]), y = .6)
#     for j in range(5):
#         ax[j].imshow(image_samples[j])
#         ax[j].axis('off')
#     fig.tight_layout()
# plt.show()
#
# # Spliting the training data into a training data and a validation data.
# st = StratifiedShuffleSplit(n_splits=2, test_size=0.2, random_state=1)
# for train_index, val_index in st.split(X, y):
#     X_train, X_val, y_train, y_val = X[train_index], X[val_index], y[train_index], y[val_index]
#
# print("The number of training data : ", X_train.shape[0])
# print("The number of validation data : ", X_val.shape[0])
#
# del X, y
#
#
# def np_resize(img, shape):
#     return cv2.resize(img, (shape[1], shape[0]), interpolation = cv2.INTER_CUBIC)