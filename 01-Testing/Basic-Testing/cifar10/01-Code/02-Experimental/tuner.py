import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"]="cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
from tensorflow.keras.callbacks import TensorBoard
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
import tensorflow_similarity as tfsim
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from imgaug import augmenters as iaa
from augmenters.img_augments import RandAugment
from augmenters.simclr import SimCLRAugmenter
from multiprocessing import cpu_count
import random
import tensorflow_datasets as tfds
import keras_tuner as kt
from tensorflow_similarity.samplers import select_examples  # select n example per class
from operations import get_augmentation_layers,MySim,TfsimRand

tfsim.utils.tf_cap_memory()
dataset='cifar10'
examples_per_class_per_batch=2
num_known_classes = 10
_, ds_info = tfds.load(
    dataset, split=["train", "test"], with_info=True, as_supervised=True)
class_list = random.sample(population=range(ds_info.features['label'].num_classes), k=num_known_classes)
tuner = "/scratch/dguthruf/tuner"
name = 'CorruptedTfsimRand'
log_dir = os.path.join(tuner,dataset,name)
train_ds = TFDatasetMultiShotMemorySampler(dataset,
                                           splits='train',
                                           examples_per_class_per_batch=examples_per_class_per_batch,
                                           classes_per_batch=num_known_classes,
                                           class_list=class_list[
                                                      0:num_known_classes])

# use the test split for indexing and querying
num_queries_per_class  = 30
num_index_per_class = 10
total_val_examples_per_class = num_queries_per_class+num_index_per_class
val_ds = TFDatasetMultiShotMemorySampler('cifar10_corrupted',
                                         splits='test',
                                         total_examples_per_class=total_val_examples_per_class,
                                         classes_per_batch=num_known_classes)


def build_model(hp):
    optimizer = hp.Choice("optimizer", ["adam", "sgd"],default='adam')
    learning_rate = hp.Float("lr", min_value=0.0001, max_value=0.0007, sampling="log",default=0.0001)
    embedding_size = hp.Choice("emb_size", values = [128,256,512],default=256)
    loss = hp.Choice("loss", ["circle", "multi"],default='circle')
    gamma = hp.Choice("gamma", values = [128,256,512],default=256)
    distance = hp.Choice("distance", ["Cosine", "L2"],default='Cosine')

    # augmenter = hp.Choice("augmenter", [
    #                                     'None',
    #                                     'TfsimRandAug0',#'TfsimRandAug1','TfsimRandAug2','TfsimRandAug3','TfsimRandAug4',#'TfsimRandAug5','TfsimRandAug6','TfsimRandAug7','TfsimRandAug8','TfsimRandAug9'
    #                                     'TfsimSimCLR0', #'TfsimSimCLR1','TfsimSimCLR2','TfsimSimCLR3','TfsimSimCLR4',#
    #                                    ],default='None')
    #
    # augmentation_layers = get_augmentation_layers(aug_type=augmenter)
    num_layers = hp.Choice("num_layers", values = [0,1,2,3,4,5,6,7,8,9,10], default=2)
    magnitude = hp.Choice("magnitude", values = [0,1,2,3,4,5,6,7,8,9,10], default=5)
    augmentation_layers = TfsimRand(num_layers=num_layers, magnitude=magnitude)

    #min_area = hp.Float("min_area", min_value=0.5, max_value=1, sampling="linear", default=0.75)
    #brightness = hp.Float("brightness", min_value=0.1, max_value=0.5, sampling="linear", default=0.3)
    #jitter = hp.Float("jitter", min_value=0.01, max_value=0.3, sampling="linear", default=0.1)
    #augmentation_layers = MySim(min_area=min_area, brightness=brightness, jitter=jitter)

    if optimizer =='adam':
      with hp.conditional_scope("optimizer",['adam']):
        optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
    elif optimizer =='sgd':
      with hp.conditional_scope("optimizer",['sgd']):
        optimizer = keras.optimizers.SGD(learning_rate=learning_rate)

    if loss =='circle':
      with hp.conditional_scope("loss",['circle']):
        loss = tfsim.losses.CircleLoss(gamma=gamma)
    elif loss =='multi':
        with hp.conditional_scope("loss",['multi']):
          loss = tfsim.losses.MultiSimilarityLoss(distance=distance)

    model = EfficientNetSim((224,224,3), embedding_size=embedding_size, variant="B0",trainable='partial',
                            augmentation=augmentation_layers)
    model.compile(optimizer,loss=loss,distance=distance)
    return model

resize = get_augmentation_layers('None')

x_val, y_val = select_examples(val_ds._x, val_ds._y, num_examples_per_class=total_val_examples_per_class)
queries_x = x_val[0:num_queries_per_class*num_known_classes]
queries_y = y_val[0:num_queries_per_class*num_known_classes]
targets_x = x_val[num_queries_per_class*num_known_classes:-1]
targets_y = y_val[num_queries_per_class*num_known_classes:-1]
queries_x = resize(queries_x)
targets_x = resize(targets_x)
tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=1,
                   metrics=['f1score', 'binary_accuracy'],
                   #tb_logdir=log_dir + '/tsc'  # uncomment if you want to track in tensorboard
                   )
tbc = TensorBoard(log_dir=log_dir + '/tb')
esc = tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=1, patience=3)
callbacks = [tsc,esc,tbc]


#
hp = kt.HyperParameters()
# augmenter = hp.Choice("augmenter", ['None',
#                                     'TfsimRandAug0', 'TfsimRandAug1','TfsimRandAug2','TfsimRandAug3','TfsimRandAug4','TfsimRandAug5','TfsimRandAug6',#'TfsimRandAug5','TfsimRandAug6','TfsimRandAug7','TfsimRandAug8','TfsimRandAug9'
#                                     #'TfsimSimCLR0', 'TfsimSimCLR1',#'TfsimSimCLR2','TfsimSimCLR3','TfsimSimCLR4',#
#                                     'MySimCLR0','MySimCLR1','MySimCLR2','MySimCLR3','MySimCLR4','MySimCLR5','MySimCLR6',
#                                     ], default='None')

#min_area = hp.Float("min_area", min_value=0.5, max_value=1, sampling="linear", default=0.75)
#brightness = hp.Float("brightness", min_value=0.1, max_value=0.5, sampling="linear", default=0.3)
#jitter = hp.Float("jitter", min_value=0.01, max_value=0.3, sampling="linear", default=0.1)

num_layers = hp.Choice("num_layers", values=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], default=2)
magnitude = hp.Choice("magnitude", values=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], default=5)
#
# tuner = kt.RandomSearch(
#     hypermodel=build_model,
#     objective=kt.Objective("f1score", "max"),
#     max_trials=80,
#     executions_per_trial=1,
#     hyperparameters=hp,
#     tune_new_entries=False,
#     overwrite=True,
#     directory=log_dir+'/model',
#     project_name="helloworld",
#
# )

tuner = kt.Hyperband(hypermodel=build_model,
                     objective=kt.Objective("f1score", "max"),
                     max_epochs=20,
                     factor=5,
                     hyperband_iterations=2,
                     directory=log_dir + '/model',
                     hyperparameters=hp,
                     tune_new_entries=False,
                     overwrite=True,
                     project_name='HyperbandTest')

tuner.search_space_summary()
x_val = resize(val_ds._x)
y_val = val_ds._y
tuner.search(train_ds,steps_per_epoch = 100, epochs=40, validation_data=(x_val,y_val),validation_steps = 50,callbacks=callbacks)






# augmenterR = get_rand_augmenter('TfsimRandAug0')
# augmenterS = get_simclr_augmenter('TfsimSimCLR0')
# augmentation_layersS = tf.keras.Sequential([
#     keras.layers.Rescaling(1 / 255.0),
#     keras.layers.Lambda(
#         lambda imgs: tf.map_fn(augmenterS.augment_img, imgs, parallel_iterations=cpu_count(), dtype="float32")),
#     # keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
#
# ])
# augmentation_layersR = tf.keras.Sequential([
#     keras.layers.Lambda(
#         lambda imgs: tf.map_fn(augmenterR.distort, imgs, parallel_iterations=cpu_count(), dtype="float32")),
#     # keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
#
# ])
# augmentation_layersN = tf.keras.Sequential([
#     keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
#
# ])
#

# num_queries = 300
# train_ds.augmenter = resize
# val_ds.augmenter = resize
# x_val, y_val = select_examples(val_ds._x, val_ds._y, num_examples_per_class=40)
# queries_x = x_val[0:num_queries]
# queries_y = y_val[0:num_queries]
# targets_x = x_val[num_queries:num_queries * 2]
# targets_y = y_val[num_queries:num_queries * 2]
# resize = augmentation_layersN
# queries_x = resize(queries_x)
# targets_x = resize(targets_x)
# tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=1,
#                    metrics=['f1score', 'binary_accuracy'],
#                    # tb_logdir=log_dir + '/tsc'  # uncomment if you want to track in tensorboard
#                    )
# optimizer = keras.optimizers.Adam(learning_rate=0.0001)
# loss = tfsim.losses.CircleLoss(gamma=256
#                                )
# model = EfficientNetSim((224, 224, 3), embedding_size=128
#                         , variant="B0",
#                         augmentation=augmentation_layersR)
# model.compile(optimizer, loss=loss, distance='cosine')
# model.fit(train_ds, steps_per_epoch=500, epochs=5, validation_data=val_ds, validation_steps=50, callbacks=[tsc])

