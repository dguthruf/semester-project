import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"]="cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
from tensorflow.keras.callbacks import TensorBoard
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
import tensorflow_similarity as tfsim
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from imgaug import augmenters as iaa
from augmenters.img_augments import RandAugment
from augmenters.simclr import SimCLRAugmenter
from multiprocessing import cpu_count
import random
tfsim.utils.tf_cap_memory()


class RandomColorAffine(keras.layers.Layer):
    def __init__(self, brightness=0, jitter=0, **kwargs):
        super().__init__(**kwargs)

        self.brightness = brightness
        self.jitter = jitter

    def call(self, images, training=True):
        if training:
            batch_size = tf.shape(images)[0]

            # Same for all colors
            brightness_scales = 1 + tf.random.uniform(
                (batch_size, 1, 1, 1), minval=-self.brightness, maxval=self.brightness
            )
            # Different for all colors
            jitter_matrices = tf.random.uniform(
                (batch_size, 1, 3, 3), minval=-self.jitter, maxval=self.jitter
            )

            color_transforms = (
                    tf.eye(3, batch_shape=[batch_size, 1]) * brightness_scales
                    + jitter_matrices
            )
            images = tf.clip_by_value(tf.matmul(images, color_transforms), 0, 1)
        return images
def get_sim_parameters(aug_type):
    if aug_type == 'MySimCLR0':
        classification_augmentation = {"min_area": 0.75, "brightness": 0.3, "jitter": 0.1}
    elif aug_type == 'MySimCLR1':
        classification_augmentation = {"min_area": 0.4, "brightness": 0.3, "jitter": 0.1}
    elif aug_type == 'MySimCLR2':
        classification_augmentation = {"min_area": 0.9, "brightness": 0.3, "jitter": 0.1}
    elif aug_type == 'MySimCLR3':
        classification_augmentation = {"min_area": 0.75, "brightness": 0.1, "jitter": 0.1}
    elif aug_type == 'MySimCLR4':
        classification_augmentation = {"min_area": 0.75, "brightness": 0.5, "jitter": 0.1}
    elif aug_type == 'MySimCLR5':
        classification_augmentation = {"min_area": 0.75, "brightness": 0.3, "jitter": 0.3}
    elif aug_type == 'MySimCLR6':
        classification_augmentation = {"min_area": 0.5, "brightness": 0.2, "jitter": 0.1}
    elif aug_type == 'MySimCLR7':
        classification_augmentation = {"min_area": 0.9, "brightness": 0.5, "jitter": 0.5}
    elif aug_type == 'MySimCLR8':
        classification_augmentation = {"min_area": 0.9, "brightness": 0.1, "jitter": 0.5}
    elif aug_type == 'MySimCLR9':
        classification_augmentation = {"min_area": 0.75, "brightness": 0.5, "jitter": 0.5}
    return classification_augmentation


def get_rand_augmenter(aug_type):
    if aug_type == 'TfsimRandAug0':
        augmenter = RandAugment(num_layers=0,magnitude=0)
    elif aug_type == 'TfsimRandAug1':
        augmenter = RandAugment(num_layers=1,magnitude=2)
    elif aug_type == 'TfsimRandAug2':
        augmenter = RandAugment(num_layers=1,magnitude=5)
    elif aug_type == 'TfsimRandAug3':
        augmenter = RandAugment(num_layers=1,magnitude=10)
    elif aug_type == 'TfsimRandAug4':
        augmenter = RandAugment(num_layers=2,magnitude=2)
    elif aug_type == 'TfsimRandAug5':
        augmenter = RandAugment(num_layers=2,magnitude=5)
    elif aug_type == 'TfsimRandAug6':
        augmenter = RandAugment(num_layers=2,magnitude=10)
    elif aug_type == 'TfsimRandAug7':
        augmenter = RandAugment(num_layers=3,magnitude=2)
    elif aug_type == 'TfsimRandAug8':
        augmenter = RandAugment(num_layers=3,magnitude=5)
    elif aug_type == 'TfsimRandAug9':
        augmenter = RandAugment(num_layers=3,magnitude=10)
    return augmenter

def get_simclr_augmenter(aug_type,IMG_SIZE=224):
    if aug_type == 'TfsimSimCLR0':
        augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,version="v2",jitter_stength=0.1)
    elif aug_type == 'TfsimSimCLR1':
        augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,version="v1",jitter_stength=0.1)
    elif aug_type == 'TfsimSimCLR2':
        augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,jitter_stength=0.5)
    elif aug_type == 'TfsimSimCLR3':
        augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,jitter_stength=0.1)
    elif aug_type == 'TfsimSimCLR4':
        augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,jitter_stength=0.8)
    return augmenter


def get_augmentation_layers(aug_type='None',target_size=224,image_channels=3):

    if aug_type[0:9] == 'TfsimRand':
        augmenter = get_rand_augmenter(aug_type)
        augmentation_layers = tf.keras.Sequential([
            # keras.layers.Lambda(
            #     lambda image: tf.py_function(augmenter.distort, [tf.cast(image, tf.uint8)], [tf.uint8])),
            keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),

            keras.layers.Lambda(
                         lambda imgs: tf.map_fn(augmenter.distort, tf.cast(imgs,tf.uint8), parallel_iterations=cpu_count(), dtype=tf.uint8)),

        ])
    elif aug_type[0:8] =='TfsimSim':
        augmenter = get_simclr_augmenter(aug_type)
        augmentation_layers = tf.keras.Sequential([
            keras.layers.Rescaling(1 / 255.0),
            keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
            keras.layers.Lambda(
                lambda imgs: tf.map_fn(augmenter._train_augment_img, tf.cast(imgs, tf.float32),
                                       parallel_iterations=cpu_count(), dtype=tf.float32)),
            keras.layers.Rescaling(255),
            keras.layers.Rescaling(1, dtype=tf.int32)
        ])
    elif aug_type[0:5] =='MySim':
        aug_params= get_sim_parameters(aug_type)
        zoom_factor = 1.0 - tf.sqrt(aug_params['min_area'])
        augmentation_layers = keras.Sequential(
            [
                keras.layers.Rescaling(1 / 255),
                keras.layers.RandomFlip("horizontal"),
                keras.layers.RandomTranslation(zoom_factor / 2, zoom_factor / 2),
                keras.layers.RandomZoom((-zoom_factor, 0.0), (-zoom_factor, 0.0)),
                RandomColorAffine(aug_params['brightness'], aug_params['jitter']),
                keras.layers.Rescaling(255 / 1),
                keras.layers.Rescaling(1, dtype=tf.int32),
                keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
            ]
        )
    elif aug_type == 'None':
        augmentation_layers = tf.keras.Sequential([
            keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
        ])

    return augmentation_layers


def MySim(min_area,brightness,jitter,target_size=224):
    zoom_factor = 1.0 - tf.sqrt(min_area)
    augmentation_layers = keras.Sequential(
        [
            keras.layers.Rescaling(1 / 255),
            keras.layers.RandomFlip("horizontal"),
            keras.layers.RandomTranslation(zoom_factor / 2, zoom_factor / 2),
            keras.layers.RandomZoom((-zoom_factor, 0.0), (-zoom_factor, 0.0)),
            RandomColorAffine(brightness, jitter),
            keras.layers.Rescaling(255 / 1),
            keras.layers.Rescaling(1, dtype=tf.int32),
            keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
        ]
    )
    return augmentation_layers


def TfsimRand(num_layers,magnitude,target_size = 224):
    augmenter = RandAugment(num_layers=num_layers, magnitude=magnitude)
    augmentation_layers = tf.keras.Sequential([
        # keras.layers.Lambda(
        #     lambda image: tf.py_function(augmenter.distort, [tf.cast(image, tf.uint8)], [tf.uint8])),
        keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
        keras.layers.Lambda(
            lambda imgs: tf.map_fn(augmenter.distort, tf.cast(imgs, tf.uint8), parallel_iterations=cpu_count(),
                                   dtype=tf.uint8)),

    ])
    return augmentation_layers
# x,y = train_ds.generate_batch(1)
import tensorflow as tf
from tensorflow.keras.datasets import cifar10
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

#
# augmentation_layers = tf.keras.Sequential([
#
#     keras.layers.Lambda(
#         lambda imgs: tf.map_fn(augmenter.distort, tf.cast(imgs,tf.uint8), parallel_iterations=cpu_count(), dtype=tf.uint8)),
# ])
# x2 = augmentation_layers(x.numpy())
# plot_25(x2.numpy(),y)
# plt.show()