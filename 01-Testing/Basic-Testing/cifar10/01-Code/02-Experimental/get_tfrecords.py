import tensorflow as tf
import os
import numpy as np
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
import tensorflow_similarity as tfsim
import tensorflow as tf
from functools import partial
import matplotlib.pyplot as plt
from imgaug import augmenters as iaa
AUTOTUNE = tf.data.AUTOTUNE



def decode_image(image):
    image = tf.image.decode_jpeg(image, channels=3)
    image = tf.cast(image, tf.float32)
    image = tf.reshape(image, [32,32, 3])
    return image


def resize(img):
    IMAGE_SIZE = 300
    with tf.device("/cpu:0"):
        img = tf.cast(img, dtype="int32")
        img = tf.image.resize_with_pad(img, IMAGE_SIZE, IMAGE_SIZE)
        return img


def augment(images):
    # Input to `augment()` is a TensorFlow tensor which
    # is not supported by `imgaug`. This is why we first
    # convert it to its `numpy` variant.
    images = tf.cast(images, tf.uint8)
    rand_aug = iaa.RandAugment(n=2, m=10)
    return rand_aug(images=images.numpy())

def read_tfrecord(example, labeled=True):
    tfrecord_format = (
        {
            "image": tf.io.FixedLenFeature([], tf.string),
            "label": tf.io.FixedLenFeature([], tf.int64),
        }
        if labeled
        else {"image": tf.io.FixedLenFeature([], tf.string),}
    )
    example = tf.io.parse_single_example(example, tfrecord_format)
    image = decode_image(example["image"])
    if labeled:
        label = tf.cast(example["label"], tf.int32)
        return image, label
    return image

def load_dataset(filenames, labeled=True):
    ignore_order = tf.data.Options()
    ignore_order.experimental_deterministic = False  # disable order, increase speed
    dataset = tf.data.TFRecordDataset(
        filenames
    )  # automatically interleaves reads from multiple files
    dataset = dataset.with_options(
        ignore_order
    )  # uses data as soon as it streams in, rather than in its original order
    dataset = dataset.map(
        partial(read_tfrecord, labeled=labeled), num_parallel_calls=AUTOTUNE
    )
    # returns a dataset of (image, label) pairs if labeled=True or just images if labeled=False
    return dataset

def get_dataset(filenames, labeled=True):
    dataset = load_dataset(filenames, labeled=labeled)
    return dataset

def get_sampler(shard_path,split,batch_size=40,example_per_class=2):
    if split == 'train':
        sampler = tfsim.samplers.TFRecordDatasetSampler(
                shard_path=shard_path,
                deserialization_fn=read_tfrecord,
                shard_suffix="*train*.*tfrecord",
                batch_size=batch_size,
                example_per_class=example_per_class,
            )
        sampler = sampler.map(resize)
        sampler = sampler.map(
            lambda x, y: (tf.py_function(augment, [x], [tf.float32])[0], y),
            num_parallel_calls=tf.data.AUTOTUNE
        )
    if split == 'test':
        sampler = tfsim.samplers.TFRecordDatasetSampler(
            shard_path=shard_path,
            deserialization_fn=read_tfrecord,
            shard_suffix="*test*",
            batch_size=batch_size,
            example_per_class=example_per_class,
        )
        sampler = sampler.map(resize)

    return sampler

def show_batch(image_batch, label_batch):
    plt.figure(figsize=(10, 10))
    for n in range(25):
        ax = plt.subplot(5, 5, n + 1)
        plt.imshow(image_batch[n] / 255.0)
        plt.axis("off")






























#
# TFRECORDS_FOLDER_PATH = "tmp/tfsim-test"
#
#
# def decode_fn(record_bytes):
#     return tf.io.parse_single_example(
#         # Data
#         record_bytes,
#         # Schema
#         {
#             "x": tf.io.FixedLenFeature([], dtype=tf.float32),
#             "y": tf.io.FixedLenFeature([], dtype=tf.int64)
#         })
#
#
# def generate_tfrecords_file(class_num: int, file_path: str):
#
#     x_tensors = tf.range(0, 100)
#
#     with tf.io.TFRecordWriter(file_path) as file_writer:
#         for x in x_tensors:
#
#             print(x)
#
#             record_bytes = tf.train.Example(features=tf.train.Features(
#                 feature={
#                     "x":
#                     tf.train.Feature(float_list=tf.train.FloatList(value=[x])),
#                     "y":
#                     tf.train.Feature(int64_list=tf.train.Int64List(
#                         value=[class_num])),
#                 })).SerializeToString()
#             file_writer.write(record_bytes)
#
#
# def generate_tfrecords_dataset():
#     os.makedirs("tmp/tfsim-test", exist_ok=True)
#     for i in range(10):
#         generate_tfrecords_file(i, f"{TFRECORDS_FOLDER_PATH}/r{i}.tfrecords")
#
#
# def read_tfrecord_file(file_path: str):
#     tfdata = tf.data.TFRecordDataset([file_path]).map(decode_fn)
#
#     for d in tfdata:
#         print(f"x= {d['x']}, y= {d['y']}")
#
#
# def read_using_tfsim():
#     sampler = tfsim.samplers.TFRecordDatasetSampler(
#         shard_path=TFRECORDS_FOLDER_PATH,
#         deserialization_fn=decode_fn,
#         shard_suffix="*.tfrecords",
#         batch_size=20,
#         example_per_class=2,
#     )
#
#     ITERATIONS = 10
#
#     count = 0
#     for d in sampler:
#         # print(f"x= {d['x']}")
#         print(f"y= {d['y']}")
#         count = count + 1
#
#         if count >= ITERATIONS:
#             break
#
#














#
# generate_tfrecords_dataset()
# read_tfrecord_file(f"{OUTPUT_FOLDER_PATH}/r1.tfrecords")
# read_using_tfsim()
#
# filenames = [filename]
# raw_dataset = tf.data.TFRecordDataset(filenames)
# IMAGE_SIZE = 300
# def decode_image(image):
#     image = tf.image.decode_jpeg(image, channels=3)
#     image = tf.cast(image, tf.float32)
#     image = tf.reshape(image, [300,300, 3])
#     return image
# feature_description = {
#     'image': tf.io.FixedLenFeature([],tf.string),
#     'label': tf.io.FixedLenFeature([], tf.int64),
# }
# def _parse_function(example_proto):
#   # Parse the input `tf.train.Example` proto using the dictionary above.
#   example_proto = tf.io.parse_single_example(example_proto, feature_description)
#   image = decode_image(example_proto["image"])
#   label = tf.cast(example_proto["label"], tf.int64)
#
#   return id,image, label
# parsed_dataset = raw_dataset.map(_parse_function)
# parsed_dataset
# for parsed_record in parsed_dataset.take(10):
#   print(repr(parsed_record))