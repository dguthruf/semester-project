import os
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)

import random
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
from tensorflow_similarity.losses import CircleLoss  # specialized similarity loss
from tensorflow_similarity.samplers import MultiShotMemorySampler
import tensorflow_datasets as tfds
import pickle as pkl
from time import time


def get_data(ds_info, num_classes=10, examples_per_class=4, steps_per_epoch=100):
    total_classes = ds_info.features["label"].num_classes
    labels = ds_info.features["label"].names
    with open("/scratch/dguthruf/downloads/cifar10/train.pkl", "rb") as f:
        x_train, y_train = pkl.load(f)
    with open("/scratch/dguthruf/downloads/cifar10/test.pkl", "rb") as f:
        x_test, y_test = pkl.load(f)
    my_dict = dict(zip(range(len(labels)), labels))
    train_cls = random.sample(range(total_classes), k=num_classes)

    train_ds = MultiShotMemorySampler(x_train, y_train,
                                      classes_per_batch=num_classes,
                                      examples_per_class_per_batch=examples_per_class,
                                      class_list=train_cls,  # Only use the first 6 classes for training.
                                      steps_per_epoch=steps_per_epoch)
    # We filter train data to only keep the train classes.
    test_ds = MultiShotMemorySampler(x_test, y_test,
                                     classes_per_batch=total_classes,
                                     total_examples_per_class=200,
                                     )
    logdir='logs/tuner/%d'%(time())
    return train_ds, test_ds, train_cls, total_classes, num_classes,logdir

def get_callbacks(data, num_targets=200, num_queries=400, k=4):
    train_ds, test_ds, train_cls, total_classes, num_classes,log_dir = data

    # Setup EvalCallback by splitting the test data into targets and queries.
    queries_x, queries_y = test_ds.get_slice(0, num_queries)
    targets_x, targets_y = test_ds.get_slice(num_queries, num_targets)

    tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=k,
                       metrics=['f1score', 'binary_accuracy'],
                       tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                       )

    # Setup SplitValidation callback.
    val_loss = SplitValidationLoss(queries_x, queries_y, targets_x, targets_y,
                                   metrics=['binary_accuracy'], known_classes=tf.constant(train_cls),
                                   k=k,
                                   tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                                   )

    # Adding the Tensorboard callback to track metrics in tensorboard.
    tbc = TensorBoard(log_dir=log_dir)  # uncomment if you want to track in tensorboard
    if total_classes == num_classes:
        callbacks = [
            # val_loss,
            tsc,
            tbc,  # uncomment if you want to track in tensorboard
        ]
    else:
        callbacks = [
            val_loss,
            tsc,
            tbc,  # uncomment if you want to track in tensorboard
        ]
    return callbacks


def build_model(hp):
    embedding_size = hp.Int("embedding_size", 64, 128, 5, default=128)
    model = EfficientNetSim((300, 300, 3), embedding_size)
    #optimizer = hp.Choice("optimizer", ["adam", "sgd"])
    # loss = hp.Discrete('loss', CircleLoss(gamma=256), MultiSimilarityLoss(), default=CircleLoss(gamma=256))
    loss = CircleLoss(gamma=256)
    model.compile(optimizer=Adam(0.0001), loss=loss, metrics=["accuracy"])
    return model


from keras_tuner import RandomSearch

tuner = RandomSearch(
    hypermodel=build_model,
    objective="val_accuracy",
    distribution_strategy=tf.distribute.MirroredStrategy(),
    directory="cifar_10_results",
    project_name="cifar10",
    overwrite=True,
    max_trials=2
)

# Reshape the images to have the channel dimension.
_, ds_info = tfds.load(
    'cifar10', split=["train", "test"], with_info=True, as_supervised=True
)
data = get_data(ds_info)
train_ds, test_ds, train_cls, total_classes, num_classes,logdir = data
callbacks = get_callbacks(data)

tuner.search((train_ds._x,train_ds._y),
    epochs=2,
    validation_data=(test_ds._x,test_ds._y),
    validation_steps=50
)

best_hps = tuner.get_best_hyperparameters()[0]
print(best_hps.values)

best_model = tuner.get_best_models()[0]
