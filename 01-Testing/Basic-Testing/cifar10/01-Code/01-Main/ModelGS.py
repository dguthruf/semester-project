import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"]="cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)
from tensorflow.keras.callbacks import TensorBoard
from tensorflow import keras
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
import tensorflow_similarity as tfsim
from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from imgaug import augmenters as iaa
from augmenters.img_augments import RandAugment
from augmenters.simclr import SimCLRAugmenter
from multiprocessing import cpu_count
import random
tfsim.utils.tf_cap_memory()

IMG_SIZE = 300 #@param {type:"integer"}

# preprocessing function that resizes images to ensure all images are the same shape
def resize(img, label):
    size = 300  # slightly larger than EfficienNetB0 size to allow random crops.
    with tf.device("/cpu:0"):
        img = tf.cast(img, dtype="int32")
        img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
        return img, label

def get_data(hparams,ds_info):
    num_known_classes = hparams['HP_NUM_CLASSES']
    classes_per_batch = ds_info.features['label'].num_classes
    dataset= ds_info.folder_name
    examples_per_class_per_batch = hparams['HP_EXAMPLES_PER_CLASS']
    class_list = random.sample(population=range(ds_info.features['label'].num_classes), k=num_known_classes)
    print(classes_per_batch,examples_per_class_per_batch)
    print(class_list)
    print(
        "Batch size is: "
        f"{min(classes_per_batch, num_known_classes) * examples_per_class_per_batch}"
    )
    if hparams['HP_MODE'] == 'EffNetSim224' or hparams['HP_MODE'] == 'EffNetSim224Part' or hparams[
        'HP_MODE'] == 'EffNetSim224Full':
        # if hparams['HP_AUGMENTER']=='None':
        #     steps_per_epoch = ds_info.splits['train'].num_examples / (
        #                 num_known_classes * examples_per_class_per_batch)
        #     if steps_per_epoch < 25:
        #         steps_per_epoch = 25
        #     elif steps_per_epoch > 250:
        #         steps_per_epoch = 250
        # else:
        #     steps_per_epoch = ds_info.splits['train'].num_examples / (
        #                 num_known_classes * examples_per_class_per_batch) * 2
        #     if steps_per_epoch < 50:
        #         steps_per_epoch = 50
        #     elif steps_per_epoch > 500:
        #         steps_per_epoch = 500
        # steps_per_epoch=100
        train_ds = TFDatasetMultiShotMemorySampler(dataset,
                                                   splits='train',
                                                   examples_per_class_per_batch=examples_per_class_per_batch,
                                                   classes_per_batch=num_known_classes,
                                                   preprocess_fn=resize,
                                                   class_list=class_list[
                                                              0:num_known_classes])

        # use the test split for indexing and querying
        val_ds = TFDatasetMultiShotMemorySampler(dataset,
                                                 splits='test',
                                                 examples_per_class_per_batch=examples_per_class_per_batch,
                                                 preprocess_fn=resize,
                                                 classes_per_batch=classes_per_batch)


    train_ds.augmenter = get_loader(hparams['HP_AUGMENTER'])
    val_ds.augmenter = get_loader('None')
    return train_ds,val_ds



def get_loader(aug_type,LARGER_IMG_SIZE = 300,IMG_SIZE=224):

    class RandomColorAffine(keras.layers.Layer):
        def __init__(self, brightness=0, jitter=0, **kwargs):
            super().__init__(**kwargs)

            self.brightness = brightness
            self.jitter = jitter

        def call(self, images, training=True):
            if training:
                batch_size = tf.shape(images)[0]

                # Same for all colors
                brightness_scales = 1 + tf.random.uniform(
                    (batch_size, 1, 1, 1), minval=-self.brightness, maxval=self.brightness
                )
                # Different for all colors
                jitter_matrices = tf.random.uniform(
                    (batch_size, 1, 3, 3), minval=-self.jitter, maxval=self.jitter
                )

                color_transforms = (
                        tf.eye(3, batch_shape=[batch_size, 1]) * brightness_scales
                        + jitter_matrices
                )
                images = tf.clip_by_value(tf.matmul(images, color_transforms), 0, 1)
            return images

    def get_tfsim_rand_augmenter(aug_type):
        if aug_type == 'TfsimRandAug0':
            augmenter = RandAugment(num_layers=0,magnitude=0)
        elif aug_type == 'TfsimRandAug1':
            augmenter = RandAugment(num_layers=1,magnitude=2)
        elif aug_type == 'TfsimRandAug2':
            augmenter = RandAugment(num_layers=1,magnitude=5)
        elif aug_type == 'TfsimRandAug3':
            augmenter = RandAugment(num_layers=1,magnitude=10)
        elif aug_type == 'TfsimRandAug4':
            augmenter = RandAugment(num_layers=2,magnitude=2)
        elif aug_type == 'TfsimRandAug5':
            augmenter = RandAugment(num_layers=2,magnitude=5)
        elif aug_type == 'TfsimRandAug6':
            augmenter = RandAugment(num_layers=2,magnitude=10)
        elif aug_type == 'TfsimRandAug7':
            augmenter = RandAugment(num_layers=3,magnitude=2)
        elif aug_type == 'TfsimRandAug8':
            augmenter = RandAugment(num_layers=3,magnitude=5)
        elif aug_type == 'TfsimRandAug9':
            augmenter = RandAugment(num_layers=3,magnitude=10)
        return augmenter
    def get_rand_augmenter(aug_type):
        if aug_type == 'RandAug0':
            augmenter = iaa.RandAugment(n=0, m=0)
        elif aug_type == 'RandAug1':
            augmenter = iaa.RandAugment(n=2, m=5)
        elif aug_type == 'RandAug2':
            augmenter = iaa.RandAugment(n=3, m=5)
        elif aug_type == 'RandAug3':
            augmenter = iaa.RandAugment(n=(0,2), m=2)
        elif aug_type == 'RandAug4':
            augmenter = iaa.RandAugment(n=(0,3), m=5)
        elif aug_type == 'RandAug5':
            augmenter = iaa.RandAugment(n=(0,3), m=(0,5))
        elif aug_type == 'RandAug6':
            augmenter = iaa.RandAugment(n=3, m=2)
        elif aug_type == 'RandAug7':
            augmenter = iaa.RandAugment(n=3, m=5)
        elif aug_type == 'RandAug8':
            augmenter = iaa.RandAugment(n=3, m=10)
        elif aug_type == 'RandAug9':
            augmenter = iaa.RandAugment(n=4, m=2)
        elif aug_type == 'RandAug10':
            augmenter = iaa.RandAugment(n=4, m=5)
        elif aug_type == 'RandAug11':
            augmenter = iaa.RandAugment(n=4, m=10)
        elif aug_type == 'RandAug12':
            augmenter = iaa.RandAugment(n=5, m=2)
        elif aug_type == 'RandAug13':
            augmenter = iaa.RandAugment(n=5, m=5)
        elif aug_type == 'RandAug14':
            augmenter = iaa.RandAugment(n=(2,5), m=10)
        elif aug_type == 'RandAug15':
            augmenter = iaa.RandAugment(n=2, m=(2,10))
        elif aug_type == 'RandAug16':
            augmenter = iaa.RandAugment(n=3, m=(2,10))
        elif aug_type == 'RandAug17':
            augmenter = iaa.RandAugment(n=4, m=(2,10))
        elif aug_type == 'RandAug18':
            augmenter = iaa.RandAugment(n=5, m=(2,10))
        elif aug_type == 'RandAug19':
            augmenter = iaa.RandAugment(n=(2,5), m=2)

        return augmenter

    def get_sim_parameters(aug_type):
        if aug_type == 'SimCLR0':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.3, "jitter": 0.1}
        elif aug_type == 'SimCLR1':
            classification_augmentation = {"min_area": 0.4, "brightness": 0.3, "jitter": 0.1}
        elif aug_type == 'SimCLR2':
            classification_augmentation = {"min_area": 0.9, "brightness": 0.3, "jitter": 0.1}
        elif aug_type == 'SimCLR3':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.1, "jitter": 0.1}
        elif aug_type == 'SimCLR4':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.5, "jitter": 0.1}
        elif aug_type == 'SimCLR5':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.3, "jitter": 0.3}
        elif aug_type == 'SimCLR6':
            classification_augmentation = {"min_area": 0.5, "brightness": 0.2, "jitter": 0.1}
        elif aug_type == 'SimCLR7':
            classification_augmentation = {"min_area": 0.9, "brightness": 0.5, "jitter": 0.5}
        elif aug_type == 'SimCLR8':
            classification_augmentation = {"min_area": 0.9, "brightness": 0.1, "jitter": 0.5}
        elif aug_type == 'SimCLR9':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.5, "jitter": 0.5}
        return classification_augmentation

    def get_tfsim_clr_augmenter(aug_type,IMG_SIZE=224):
        if aug_type == 'TfsimSimCLR0':
            augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE)
        elif aug_type == 'TfsimSimCLR1':
            augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,version="v1")
        elif aug_type == 'TfsimSimCLR2':
            augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,jitter_stength=0.5)
        elif aug_type == 'TfsimSimCLR3':
            augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,jitter_stength=0.1)
        elif aug_type == 'TfsimSimCLR4':
            augmenter = SimCLRAugmenter(IMG_SIZE, IMG_SIZE,jitter_stength=0.8)
        return augmenter


    @tf.function()
    def process(img):
        img = tf.keras.layers.Resizing(IMG_SIZE, IMG_SIZE)(img)
        #tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
        if aug_type[0:9]=='TfsimRand':
            # img = tf.image.random_crop(img, (IMG_SIZE, IMG_SIZE, 3))
            # img = tf.image.random_flip_left_right(img)
            augmenter = get_tfsim_rand_augmenter(aug_type)
            img = augmenter.distort(img)
        elif aug_type[0:10]=='TfsimSimCLR':
            augmenter = get_tfsim_clr_augmenter(aug_type)
            print(img.shape)
            print(img.dtype)
            print(img[0].shape)
            img = augmenter.augment_img(img)

        return img
    def loader(x, y,*args):
        imgs = tf.stack(x)
        imgs = tf.map_fn(process, imgs, parallel_iterations=cpu_count(), dtype="float32")
        if aug_type[0:6]=='SimCLR':
            aug_params = get_sim_parameters(aug_type)
            zoom_factor = 1.0 - tf.sqrt(aug_params['min_area'])
            augmentation_layers = keras.Sequential(
                [
                    keras.layers.Rescaling(1 / 255),
                    keras.layers.RandomFlip("horizontal"),
                    keras.layers.RandomTranslation(zoom_factor / 2, zoom_factor / 2),
                    keras.layers.RandomZoom((-zoom_factor, 0.0), (-zoom_factor, 0.0)),
                    RandomColorAffine(aug_params['brightness'], aug_params['jitter']),
                    keras.layers.Rescaling(255 / 1),
                ]
            )
            imgs = augmentation_layers(imgs)
        elif aug_type[0:4]=='Rand':
            augmenter = get_rand_augmenter(aug_type)
            imgs = tf.cast(imgs, tf.uint8)
            imgs = augmenter.augment(images=imgs.numpy())
            imgs = tf.cast(imgs,tf.uint8)
        return imgs,y

    return loader
















def get_model(hparams,x_train):
    inputs = keras.layers.Input(x_train[0].shape)
    mode = hparams['HP_MODE']
    #augmentation_layers = get_augmentation_layers(hparams['HP_AUGMENTER'])
    print(x_train[0].shape)
    if mode == 'Basic':
        x = keras.layers.Rescaling(scale=1.0 / 255)(inputs)
        x = keras.layers.Conv2D(64, 3, activation="relu")(x)
        x = keras.layers.BatchNormalization()(x)
        x = keras.layers.Conv2D(128, 3, activation="relu")(x)
        x = keras.layers.BatchNormalization()(x)
        x = keras.layers.MaxPool2D((4, 4))(x)
        x = keras.layers.Conv2D(256, 3, activation="relu")(x)
        x = keras.layers.BatchNormalization()(x)
        x = keras.layers.Conv2D(256, 3, activation="relu")(x)
        x = keras.layers.GlobalMaxPool2D()(x)
        outputs = tfsim.layers.MetricEmbedding(hparams['HP_EMBEDDING_SIZE'])(x)
        # building model
        model = tfsim.models.SimilarityModel(inputs, outputs)
    elif mode == 'EffNetSim224':
        model = EfficientNetSim(x_train[0].shape, hparams['HP_EMBEDDING_SIZE'],variant="B0",augmentation=None)
    elif mode == 'EffNetSim224Part':
        model = EfficientNetSim(x_train[0].shape,hparams['HP_EMBEDDING_SIZE'],variant="B0",trainable='partial',augmentation=None,
                                )
    elif mode == 'EffNetSim224Full':
        model = EfficientNetSim(x_train[0].shape, hparams['HP_EMBEDDING_SIZE'],variant="B0",augmentation=None,trainable='full')
    return model


def get_callbacks(queries_x,queries_y,targets_x,targets_y,hparams,log_dir,ds_info):
    class_list = random.sample(population=range(ds_info.features['label'].num_classes), k=hparams['HP_NUM_CLASSES'])
    total_classes = ds_info.features["label"].num_classes
    tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y, k=hparams['HP_NUM_NEIGHBOURS'],
                       metrics=['f1score', 'binary_accuracy'],
                       tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                       )

    # Setup SplitValidation callback.
    val_loss = SplitValidationLoss(queries_x, queries_y, targets_x, targets_y,
                                   metrics=['binary_accuracy'], known_classes=tf.constant(class_list),
                                   k=hparams['HP_NUM_NEIGHBOURS'],
                                   tb_logdir=log_dir  # uncomment if you want to track in tensorboard
                                   )

    # Adding the Tensorboard callback to track metrics in tensorboard.
    tbc = TensorBoard(log_dir=log_dir,
                        histogram_freq=1,
                        profile_batch=(100,200))  # uncomment if you want to track in tensorboard
    es = tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=1, patience=2,restore_best_weights=True)

    if total_classes == hparams['HP_NUM_CLASSES']:
        callbacks = [
            # val_loss,
            tsc,
            tbc,
            es,# uncomment if you want to track in tensorboard
        ]
    else:
        callbacks = [
            val_loss,
            tsc,
            tbc,
            es,# uncomment if you want to track in tensorboard
        ]

    return callbacks


    # init similarity loss
def get_loss(hparams):
    loss_ = hparams['HP_LOSS']
    if loss_ == 'Circle128':
        loss = tfsim.losses.CircleLoss(gamma=128)
    elif loss_ == 'Circle256':
        loss = tfsim.losses.CircleLoss(gamma=256)
    elif loss_ == 'Circle512':
        loss = tfsim.losses.CircleLoss(gamma=512)
    elif loss_ == 'MultiCosine':
        loss = tfsim.losses.MultiSimilarityLoss(distance='Cosine')
    elif loss_ == 'MultiL1':
        loss = tfsim.losses.MultiSimilarityLoss(distance='L1')
    elif loss_ == 'MultiL2':
        loss = tfsim.losses.MultiSimilarityLoss(distance='L2')

    return  loss







# Image augmentation module
def get_augmentation_layers(aug_type,target_size=224,image_channels=3):
    # Distorts the color distibutions of images
    class RandomColorAffine(keras.layers.Layer):
        def __init__(self, brightness=0, jitter=0, **kwargs):
            super().__init__(**kwargs)

            self.brightness = brightness
            self.jitter = jitter

        def call(self, images, training=True):
            if training:
                batch_size = tf.shape(images)[0]

                # Same for all colors
                brightness_scales = 1 + tf.random.uniform(
                    (batch_size, 1, 1, 1), minval=-self.brightness, maxval=self.brightness
                )
                # Different for all colors
                jitter_matrices = tf.random.uniform(
                    (batch_size, 1, 3, 3), minval=-self.jitter, maxval=self.jitter
                )

                color_transforms = (
                        tf.eye(3, batch_shape=[batch_size, 1]) * brightness_scales
                        + jitter_matrices
                )
                images = tf.clip_by_value(tf.matmul(images, color_transforms), 0, 1)
            return images

    def get_tfsim_rand_augmenter(aug_type):
        if aug_type == 'TfsimRandAug8':
            augmenter = RandAugment(num_layers=0,magnitude=0)
        elif aug_type == 'TfsimRandAug1':
            augmenter = RandAugment(num_layers=1,magnitude=2)
        elif aug_type == 'TfsimRandAug2':
            augmenter = RandAugment(num_layers=1,magnitude=5)
        elif aug_type == 'TfsimRandAug3':
            augmenter = RandAugment(num_layers=1,magnitude=10)
        elif aug_type == 'TfsimRandAug4':
            augmenter = RandAugment(num_layers=2,magnitude=2)
        elif aug_type == 'TfsimRandAug5':
            augmenter = RandAugment(num_layers=2,magnitude=5)
        elif aug_type == 'TfsimRandAug6':
            augmenter = RandAugment(num_layers=2,magnitude=10)
        elif aug_type == 'TfsimRandAug7':
            augmenter = RandAugment(num_layers=3,magnitude=2)
        elif aug_type == 'TfsimRandAug0':
            augmenter = RandAugment(num_layers=3,magnitude=5)
        elif aug_type == 'TfsimRandAug9':
            augmenter = RandAugment(num_layers=3,magnitude=10)
        return augmenter
    def get_rand_augmenter(aug_type):
        if aug_type == 'RandAug0':
            augmenter = iaa.RandAugment(n=0, m=0)
        elif aug_type == 'RandAug1':
            augmenter = iaa.RandAugment(n=(2,5), m=5)
        elif aug_type == 'RandAug2':
            augmenter = iaa.RandAugment(n=(2,5), m=(2,10))
        elif aug_type == 'RandAug3':
            augmenter = iaa.RandAugment(n=2, m=2)
        elif aug_type == 'RandAug4':
            augmenter = iaa.RandAugment(n=2, m=5)
        elif aug_type == 'RandAug5':
            augmenter = iaa.RandAugment(n=2, m=10)
        elif aug_type == 'RandAug6':
            augmenter = iaa.RandAugment(n=3, m=2)
        elif aug_type == 'RandAug7':
            augmenter = iaa.RandAugment(n=3, m=5)
        elif aug_type == 'RandAug8':
            augmenter = iaa.RandAugment(n=3, m=10)
        elif aug_type == 'RandAug9':
            augmenter = iaa.RandAugment(n=4, m=2)
        elif aug_type == 'RandAug10':
            augmenter = iaa.RandAugment(n=4, m=5)
        elif aug_type == 'RandAug11':
            augmenter = iaa.RandAugment(n=4, m=10)
        elif aug_type == 'RandAug12':
            augmenter = iaa.RandAugment(n=5, m=2)
        elif aug_type == 'RandAug13':
            augmenter = iaa.RandAugment(n=5, m=5)
        elif aug_type == 'RandAug14':
            augmenter = iaa.RandAugment(n=(2,5), m=10)
        elif aug_type == 'RandAug15':
            augmenter = iaa.RandAugment(n=2, m=(2,10))
        elif aug_type == 'RandAug16':
            augmenter = iaa.RandAugment(n=3, m=(2,10))
        elif aug_type == 'RandAug17':
            augmenter = iaa.RandAugment(n=4, m=(2,10))
        elif aug_type == 'RandAug18':
            augmenter = iaa.RandAugment(n=5, m=(2,10))
        elif aug_type == 'RandAug19':
            augmenter = iaa.RandAugment(n=(2,5), m=2)

        return augmenter

    def get_sim_parameters(aug_type):
        if aug_type == 'SimCLR0':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.3, "jitter": 0.1}
        elif aug_type == 'SimCLR1':
            classification_augmentation = {"min_area": 0.4, "brightness": 0.3, "jitter": 0.1}
        elif aug_type == 'SimCLR2':
            classification_augmentation = {"min_area": 0.9, "brightness": 0.3, "jitter": 0.1}
        elif aug_type == 'SimCLR3':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.1, "jitter": 0.1}
        elif aug_type == 'SimCLR4':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.5, "jitter": 0.1}
        elif aug_type == 'SimCLR5':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.3, "jitter": 0.3}
        elif aug_type == 'SimCLR6':
            classification_augmentation = {"min_area": 0.5, "brightness": 0.2, "jitter": 0.1}
        elif aug_type == 'SimCLR7':
            classification_augmentation = {"min_area": 0.9, "brightness": 0.5, "jitter": 0.5}
        elif aug_type == 'SimCLR8':
            classification_augmentation = {"min_area": 0.9, "brightness": 0.1, "jitter": 0.5}
        elif aug_type == 'SimCLR9':
            classification_augmentation = {"min_area": 0.75, "brightness": 0.5, "jitter": 0.5}
        return classification_augmentation

    def augment(images):
        # Input to `augment()` is a TensorFlow tensor which
        # is not supported by `imgaug`. This is why we first
        # convert it to its `numpy` variant.
        augmenter = get_rand_augmenter(aug_type)
        images = tf.cast(images, tf.uint8)
        return augmenter(images=images.numpy())


    if aug_type[0:4] == 'Rand':
        augmentation_layers = tf.keras.Sequential([
            keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
            keras.layers.Lambda(lambda image: tf.py_function(augment, [image], [tf.float32])),

        ])
    elif aug_type[0:9] == 'TfsimRand':
        augmenter = get_tfsim_rand_augmenter(aug_type)
        augmentation_layers = tf.keras.Sequential([
            keras.layers.Lambda(lambda image: tf.image.resize(image, (target_size, target_size))),
            keras.layers.Lambda(lambda image: tf.py_function(augmenter.distort, [image], [tf.float32])),


        ])
    elif aug_type[0:3] =='Sim':
        aug_params= get_sim_parameters(aug_type)
        zoom_factor = 1.0 - tf.sqrt(aug_params['min_area'])
        augmentation_layers = keras.Sequential(
            [
                keras.layers.Rescaling(1 / 255),
                keras.layers.RandomFlip("horizontal"),
                keras.layers.RandomTranslation(zoom_factor / 2, zoom_factor / 2),
                keras.layers.RandomZoom((-zoom_factor, 0.0), (-zoom_factor, 0.0)),
                RandomColorAffine(aug_params['brightness'], aug_params['jitter']),
                keras.layers.Rescaling(255/1),
                keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
            ]
        )
    elif aug_type == 'None':
        augmentation_layers = tf.keras.Sequential([
            keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
        ])

    return augmentation_layers

# import os
# import tensorflow as tf
# os.environ["CUDA_VISIBLE_DEVICES"] = "4"
# gpus = tf.config.experimental.list_physical_devices('GPU')
# os.environ["TF_GPU_ALLOCATOR"]="cuda_malloc_async"
# tf.config.experimental.set_memory_growth(gpus[0], True)
# from tensorflow.keras.callbacks import TensorBoard
# from tensorflow import keras
# from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
# from tensorflow_similarity.callbacks import SplitValidationLoss  # evaluate validation loss on known and unknown classes
# import tensorflow_similarity as tfsim
# from tensorflow_similarity.samplers import TFDatasetMultiShotMemorySampler  # Get and samples TF dataset catalog
# from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
# from imgaug import augmenters as iaa
# from augmenters.img_augments import RandAugment
# from augmenters.simclr import SimCLRAugmenter
# import random
# tfsim.utils.tf_cap_memory()
# import csv
# import os
# import random
# from collections import defaultdict
# from multiprocessing import cpu_count
# from pathlib import Path
# import numpy as np
# import tensorflow as tf
# from tabulate import tabulate
# from tqdm.auto import tqdm
#
#
# from matplotlib import pyplot as plt
# from mpl_toolkits import axes_grid1
# def plot_25(x, y):
#     """Plot the first 25 images."""
#     num_cols = num_rows = 5
#     fig = plt.figure(figsize=(6.0, 6.0))
#     grid = axes_grid1.ImageGrid(
#         fig=fig,
#         rect=111,
#         nrows_ncols=(num_cols, num_rows),
#         axes_pad=0.1,
#     )
#     for ax, im, label in zip(grid, x, y):
#         max_val = float(tf.math.reduce_max(im))
#         ax.imshow(im / max_val)
#         ax.axis("off")
# from tqdm.auto import tqdm
# train_ds = TFDatasetMultiShotMemorySampler('cifar10',
#                                            splits='train',
#                                            examples_per_class_per_batch=2,
#                                            steps_per_epoch=1000,
#                                            classes_per_batch=10)
# tfsim_sim = SimCLRAugmenter(224,224)
# x,y = train_ds.generate_batch(1)
# augmentation_layers = tf.keras.Sequential([
#     keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
#     keras.layers.Lambda(lambda image: tf.py_function(tfsim_sim._train_augment_img, image, [tf.float32]))
# ])
# x2 = augmentation_layers(x[0])
# plot_25(x2,y)
# plt.show()
# def get_augmentation_layers(aug_type,target_size=224,image_channels=3):
#
#     def AugType(aug_type):
#         if aug_type == 'RandAug0':
#             augmenter = RandAugment(num_layers=0, magnitude=0)
#         elif aug_type == 'RandAug1':
#             augmenter = RandAugment(num_layers=(0,2), magnitude=5)
#         elif aug_type == 'RandAug2':
#             augmenter = RandAugment(num_layers=2, magnitude=2)
#         elif aug_type == 'RandAug3':
#             augmenter = RandAugment(num_layers=2, magnitude=10)
#         elif aug_type == 'RandAug4':
#             augmenter = RandAugment(num_layers=5, magnitude=4)
#         elif aug_type == 'RandAug5':
#             augmenter = RandAugment(num_layers=5, magnitude=8)
#         elif aug_type == 'RandAug6':
#             augmenter = RandAugment(num_layers=5, magnitude=2)
#         elif aug_type == 'RandAug7':
#             augmenter = RandAugment(num_layers=6, magnitude=10)
#         elif aug_type == 'RandAug8':
#             augmenter = RandAugment(num_layers=10, magnitude=2)
#         elif aug_type == 'SimCLR0':
#             augmenter = SimCLRAugmenter(target_size,target_size)
#         elif aug_type == 'SimCLR1':
#             augmenter = SimCLRAugmenter(target_size,target_size,version='v2')
#
#         return augmenter
#
#
#     if aug_type[0:4] == 'Rand':
#         print('hi')
#         augmenter = AugType(aug_type)
#         augmentation_layers = tf.keras.Sequential([
#             keras.Input(shape=(target_size, target_size, image_channels)),
#             keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
#             keras.layers.Lambda(augmenter.distort)
#         ])
#     elif aug_type[0:3] =='Sim':
#         augmenter = AugType(aug_type)
#         augmentation_layers = tf.keras.Sequential([
#             keras.Input(shape=(target_size, target_size, image_channels)),
#             keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
#             keras.layers.Lambda(augmenter.augment_img)
#         ])
#     elif aug_type == 'None':
#         augmentation_layers = tf.keras.Sequential([
#             keras.Input(shape=(target_size, target_size, image_channels)),
#             keras.layers.Resizing(target_size, target_size, interpolation='bilinear'),
#         ])
#
#     return augmentation_layers








# def get_loader(hparams):
#
#     aug = hparams['HP_AUGMENTER']
#     if aug=='None':
#         return 'None'
#     elif aug=='Aug0':
#         def loader(x, y, sample_per_batch, is_warmup):
#             # imgs = tf.cast(x / 255.0, dtype='float32')
#             IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
#             with tf.device("/cpu:0"):
#                 img = tf.cast(x, dtype="int32")
#                 img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
#                 img = tf.cast(img, dtype="int32")
#                 img = tf.cast(img, tf.uint8)
#                 rand_aug = iaa.RandAugment(n=0, m=0)
#                 return rand_aug(images=img.numpy()), y
#         return loader
#     elif aug=='Aug1':
#         def loader(x, y, sample_per_batch, is_warmup):
#             # imgs = tf.cast(x / 255.0, dtype='float32')
#             IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
#             with tf.device("/cpu:0"):
#                 img = tf.cast(x, dtype="int32")
#                 img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
#                 img = tf.cast(img, dtype="int32")
#                 img = tf.cast(img, tf.uint8)
#                 rand_aug = iaa.RandAugment(n=2, m=10)
#                 return rand_aug(images=img.numpy()), y
#         return loader
#     elif aug=='Aug2':
#         def loader(x, y, sample_per_batch, is_warmup):
#             # imgs = tf.cast(x / 255.0, dtype='float32')
#             IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
#             with tf.device("/cpu:0"):
#                 img = tf.cast(x, dtype="int32")
#                 img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
#                 img = tf.cast(img, dtype="int32")
#                 img = tf.cast(img, tf.uint8)
#                 rand_aug = iaa.RandAugment(n=10, m=2)
#                 return rand_aug(images=img.numpy()), y
#         return loader
#     elif aug=='Aug3':
#         def loader(x, y, sample_per_batch, is_warmup):
#             # imgs = tf.cast(x / 255.0, dtype='float32')
#             IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
#             with tf.device("/cpu:0"):
#                 img = tf.cast(x, dtype="int32")
#                 img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
#                 img = tf.cast(img, dtype="int32")
#                 img = tf.cast(img, tf.uint8)
#                 rand_aug = iaa.RandAugment(n=10, m=10)
#                 return rand_aug(images=img.numpy()), y
#         return loader
#     elif aug=='Aug4':
#         def loader(x, y, sample_per_batch, is_warmup):
#             # imgs = tf.cast(x / 255.0, dtype='float32')
#             IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
#             with tf.device("/cpu:0"):
#                 img = tf.cast(x, dtype="int32")
#                 img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
#                 img = tf.cast(img, dtype="int32")
#                 img = tf.cast(img, tf.uint8)
#                 rand_aug = iaa.RandAugment(n=2, m=10)
#                 return rand_aug(images=img.numpy()), y
#         return loader
#     elif aug=='Aug5':
#         def loader(x, y, sample_per_batch, is_warmup):
#             # imgs = tf.cast(x / 255.0, dtype='float32')
#             IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
#             with tf.device("/cpu:0"):
#                 img = tf.cast(x, dtype="int32")
#                 img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
#                 img = tf.cast(img, dtype="int32")
#                 img = tf.cast(img, tf.uint8)
#                 rand_aug = iaa.RandAugment(n=10, m=20)
#                 return rand_aug(images=img.numpy()), y
#         return loader
#     elif aug=='Aug6':
#         def loader(x, y, sample_per_batch, is_warmup):
#             # imgs = tf.cast(x / 255.0, dtype='float32')
#             IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
#             with tf.device("/cpu:0"):
#                 img = tf.cast(x, dtype="int32")
#                 img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
#                 img = tf.cast(img, dtype="int32")
#                 img = tf.cast(img, tf.uint8)
#                 rand_aug = iaa.RandAugment(n=10, m=30)
#                 return rand_aug(images=img.numpy()), y
#         return loader
#     elif aug=='Aug7':
#         def loader(x, y, sample_per_batch, is_warmup):
#             # imgs = tf.cast(x / 255.0, dtype='float32')
#             IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
#             with tf.device("/cpu:0"):
#                 img = tf.cast(x, dtype="int32")
#                 img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
#                 img = tf.cast(img, dtype="int32")
#                 img = tf.cast(img, tf.uint8)
#                 rand_aug = iaa.RandAugment(n=20, m=30)
#                 return rand_aug(images=img.numpy()), y
#         return loader
#
