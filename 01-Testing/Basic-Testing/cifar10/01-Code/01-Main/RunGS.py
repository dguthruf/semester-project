import os
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
from tensorboard.plugins.hparams import api as hp
import sys
from tensorflow import keras
import tensorflow_datasets as tfds
import tensorflow_similarity as tfsim
import gc
import tensorflow as tf
from ModelGS import get_model,get_callbacks,get_data,get_loss,get_augmentation_layers
from PlotGS import visualize
from MainGS import get_params
from tensorflow_similarity.samplers import select_examples  # select n example per class
tfsim.utils.tf_cap_memory()

def run(log_dir,hparams):
    _, ds_info = tfds.load(
        dataset, split=["train", "test"], with_info=True, as_supervised=True)
    train_ds,val_ds = get_data(hparams,ds_info)
    augmentation_layers = get_augmentation_layers('None')
    num_queries = 1000
    min_ex = min([len(val_ds.index_per_class[i]) for i in val_ds.index_per_class])
    min_ex = 40
    print(min_ex)
    x_val,y_val = select_examples(val_ds._x,val_ds._y,num_examples_per_class=min_ex)
    queries_x = x_val[0:int(min_ex*0.25)*ds_info.features['label'].num_classes]
    queries_y = y_val[0:int(min_ex*0.25)*ds_info.features['label'].num_classes]
    targets_x = x_val[int(min_ex*0.25)*ds_info.features['label'].num_classes:int(min_ex*0.25)*ds_info.features['label'].num_classes+int(hparams['HP_NUM_TARGETS']*hparams['HP_NUM_CLASSES']/10)]
    targets_y = y_val[int(min_ex*0.25)*ds_info.features['label'].num_classes:int(min_ex*0.25)*ds_info.features['label'].num_classes+int(hparams['HP_NUM_TARGETS']*hparams['HP_NUM_CLASSES']/10)]
    print(len(targets_x))
    # queries_x,queries_y = val_ds.get_slice(0,num_queries)
    # targets_x,targets_y = val_ds.get_slice(num_queries,int(hparams['HP_NUM_TARGETS']*hparams['HP_NUM_CLASSES']/10))
    queries_x = augmentation_layers(queries_x)
    targets_x = augmentation_layers(targets_x)

    callbacks = get_callbacks(queries_x, queries_y, targets_x, targets_y, hparams, log_dir, ds_info)
    loss = get_loss(hparams)
    val_steps = 50
    #min_ex_train = min([len(train_ds.index_per_class[i]) for i in train_ds.index_per_class])
    # x_i,y_i = select_examples(train_ds._x,train_ds._y,num_examples_per_class=int(1000/hparams['HP_NUM_CLASSES']))
    # x_i = augmentation_layers(x_i)
    #
    # x_c,y_c = train_ds.get_slice(0,1000)
    # x_c = augmentation_layers(x_c)
    model = get_model(hparams, queries_x)
    # compiling and training
    model.compile(
        optimizer=keras.optimizers.Adam(learning_rate=hparams['HP_LEARNING_RATE']), loss=loss,
    )
    model.summary()

    history = model.fit(
        train_ds, epochs=hparams['HP_EPOCHS'], validation_data=val_ds,validation_steps=val_steps,steps_per_epoch=hparams['HP_STEPS_PER_EPOCH'],callbacks=callbacks
    )


    #Calibrate Model
    model.reset_index()
    model.index(targets_x, targets_y, data=targets_x)
    calibration = model.calibrate(
        queries_x,
        queries_y,
        calibration_metric="f1",
        matcher="match_nearest",
        extra_metrics=["precision", "recall", "binary_accuracy"],
        verbose=1,
    )

    cm_image,metric_image=visualize(hparams,model,calibration,ds_info,queries_x,queries_y,log_dir)


    del model,train_ds,val_ds,queries_y,queries_x,targets_x,targets_y,callbacks
    gc.collect()
    tf.keras.backend.clear_session()
    return history,cm_image,metric_image

def run_hparam(log_dir,ez,nc,epc,spe,nt,l,m,lr,e,nn,p):
    hparams_dict=get_params()
    hparams = {
        'HP_EMBEDDING_SIZE': ez,
        'HP_NUM_CLASSES': nc,
        'HP_EXAMPLES_PER_CLASS': epc,
        'HP_STEPS_PER_EPOCH': spe,
        'HP_NUM_TARGETS': nt,
        'HP_LOSS': l,
        'HP_MODE': m,
        'HP_AUGMENTER': a,
        'HP_LEARNING_RATE': lr,
        'HP_EPOCHS': e,
        'HP_NUM_NEIGHBOURS': nn,
        'HP_PARAMETER': p,

    }
    with tf.summary.create_file_writer(log_dir+'/HParam').as_default():
        hp.hparams_config(
            hparams=list(hparams_dict.values()),
            metrics=[hp.Metric('f1score', display_name='F1Score'),
                     hp.Metric('binary_accuracy', display_name='Binary Accuracy'),
                     hp.Metric('binary_accuracy_unknown_classes',display_name='Binary Accuracy Unkown Classes')]
        )
        hp.hparams({hparams_dict[h]: hparams[h] for h in hparams_dict.keys()},trial_id=run_name)
        history,cm_image,metric_image = run(log_dir, hparams)

        tf.summary.scalar('binary_accuracy', history.history['binary_accuracy'][-1], step=1)
        tf.summary.scalar('f1score', history.history['f1score'][-1], step=1)
        tf.summary.image("Confusion Matrix", cm_image, step=1)
        tf.summary.image("Calibrated Metric", metric_image, step=1)


        try:
            tf.summary.scalar('binary_accuracy_unknown_classes', history.history['binary_accuracy_unknown_classes'][-1], step=1)
        except:
            pass
        del history,hparams,hparams_dict
        gc.collect()
        tf.keras.backend.clear_session()



if __name__ == '__main__':
    name=str(sys.argv[1])
    ez = int(sys.argv[2])
    nc = int(sys.argv[3])
    epc = int(sys.argv[4])
    spe = int(sys.argv[5])
    nt = int(sys.argv[6])
    l = str(sys.argv[7])
    m = str(sys.argv[8])
    a = str(sys.argv[9])
    lr = float(sys.argv[10])
    e = int(sys.argv[11])
    nn = int(sys.argv[12])
    p = str(sys.argv[13])
    dataset=str(sys.argv[14])
    run_name =str(sys.argv[15])
    log_path = '/scratch/dguthruf/logs/'
    log_dir = os.path.join(log_path,dataset,name,
                           run_name,
                           )
    exit(run_hparam(log_dir,ez,nc,epc,spe,nt,l,m,lr,e,nn,p))