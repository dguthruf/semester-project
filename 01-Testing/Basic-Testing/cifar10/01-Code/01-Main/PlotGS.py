import os
import io
import tensorflow_similarity as tfsim
import tensorflow as tf
from tensorflow.keras import layers
import matplotlib.pyplot as plt
import numpy as np
from ModelGS import get_augmentation_layers
import cv2
from tensorboard.plugins import projector
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
tfsim.utils.tf_cap_memory()
from tensorflow_similarity.samplers import select_examples  # select n example per class


# preprocessing function that resizes images to ensure all images are the same shape
def resize(img, label):
    IMG_SIZE = 300  # slightly larger than EfficienNetB0 size to allow random crops.
    with tf.device("/cpu:0"):
        img = tf.cast(img, dtype="int32")
        img = tf.image.resize_with_pad(img, IMG_SIZE, IMG_SIZE)
        img = tf.cast(img, dtype="int32")

        return img, label

def plot_to_image(figure,type='No'):
  """Converts the matplotlib plot specified by 'figure' to a PNG image and
  returns it. The supplied figure is closed and inaccessible after this call."""
  # Save the plot to a PNG in memory.
  buf = io.BytesIO()
  if type == 'yes':
    plt.savefig(buf, format='png',bbox_inches='tight',pad_inches=0.2,dpi=100)
  else:
    plt.savefig(buf, format='png', bbox_inches='tight', pad_inches=0.2, dpi=100)
  # Closing the figure prevents it from being displayed directly inside
  # the notebook.
  plt.close(figure)
  buf.seek(0)
  # Convert PNG buffer to TF image
  image = tf.image.decode_png(buf.getvalue(), channels=4)
  # Add the batch dimension
  image = tf.expand_dims(image, 0)
  return image


def unfreeze_model(model):
    # We unfreeze the top 20 layers while leaving BatchNorm layers frozen
    for layer in model.layers[-20:]:
        if not isinstance(layer, layers.BatchNormalization):
            layer.trainable = True

def visualize(hparams,model,calibration,ds_info,queries_x,queries_y,log_dir):
    augmentation_layers = get_augmentation_layers('None')
    # queries_x,queries_y = select_examples(val_ds._x, val_ds._y, num_examples_per_class=int(1500/hparams['HP_NUM_CLASSES']))
    # queries_x = augmentation_layers(queries_x)

    labels = ds_info.features["label"].names
    labels.append('unknown')

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))
    x = calibration.thresholds["distance"]

    ax1.plot(x, calibration.thresholds["precision"], label="precision")
    ax1.plot(x, calibration.thresholds["recall"], label="recall")
    ax1.plot(x, calibration.thresholds["f1"], label="f1 score")
    ax1.legend()
    ax1.set_title("Metric evolution as distance increase")
    ax1.set_xlabel("Distance")
    ax1.set_ylim((-0.05, 1.05))

    ax2.plot(calibration.thresholds["recall"], calibration.thresholds["precision"])
    ax2.set_title("Precision recall curve")
    ax2.set_xlabel("Recall")
    ax2.set_ylabel("Precision")
    ax2.set_ylim((-0.05, 1.05))
    metric_image = plot_to_image(fig)

    cutpoint = "optimal"

    # This yields 100 examples for each class.
    # We defined this when we created the val_ds sampler.
    #x_confusion, y_confusion = select_examples(val_ds._x, val_ds._y, num_examples_per_class=100)
    print(len(queries_x))
    matches = model.match(queries_x, cutpoint=cutpoint, no_match_label=10)
    len(matches)
    pt = tfsim.visualization.confusion_matrix(
        matches,
        queries_y,
        labels=labels,
        title="Confusion matrix for cutpoint:%s" % cutpoint,
        normalize=True,
        show=False
    )
    cm_image = plot_to_image(pt.Figure(figsize=(40,40)),type='yes')
    pt.show()
    vx = queries_x
    vy = queries_y
    class_mapping = {c_id: c_lbl for c_id, c_lbl in zip(range(len(labels)), labels)}
    project_embedding(model,vx.numpy(),vy,class_mapping,log_dir)


    return  cm_image,metric_image
def project_embedding(model,vx,vy,class_mapping,logdir):
    size = 32
    img_data = []
    for img in vx:
        input_img_resize=cv2.resize(img,(size,size))
        img_data.append(input_img_resize)


    dir = 'train'
    logdir = os.path.join(logdir, dir)
    metadata_file = open(os.path.join(logdir, 'metadata.tsv'), 'a+')
    metadata_file.write('Class\tName\n')
    for i in range(len(vy)):
        c = class_mapping[vy[i]]
        metadata_file.write('{}\t{}\n'.format(vy[i], c))
    metadata_file.close()


    sprite = images_to_sprite(np.array(img_data))
    cv2.imwrite(os.path.join(logdir, 'sprite.png'), sprite)

    emb = model.predict(vx)
    # Save the weights we want to analyze as a variable.
    # The weights need to have the shape (Number of sample, Total Dimensions)
    # Hence why we flatten the Tensor
    weights = tf.Variable(tf.reshape(emb,(vx.shape[0],-1)), name="emb")
    # Create a checkpoint from embedding, the filename and key are the
    # name of the tensor.
    checkpoint = tf.train.Checkpoint(emb=weights)
    checkpoint.save(os.path.join(logdir, "embedding.ckpt"))

    # Set up config.
    config = projector.ProjectorConfig()
    embedding = config.embeddings.add()
    embedding.tensor_name = "emb/.ATTRIBUTES/VARIABLE_VALUE"
    embedding.metadata_path = 'metadata.tsv'
    embedding.sprite.image_path = 'sprite.png'
    embedding.sprite.single_image_dim.extend([size, size]) # image size = 28x28
    # The name of the tensor will be suffixed by `/.ATTRIBUTES/VARIABLE_VALUE`.
    projector.visualize_embeddings(logdir, config)

def images_to_sprite(data):
    """Creates the sprite image along with any necessary padding

    Args:
      data: NxHxW[x3] tensor containing the images.

    Returns:
      data: Properly shaped HxWx3 image with any necessary padding.
    """
    if len(data.shape) == 3:
        data = np.tile(data[..., np.newaxis], (1, 1, 1, 3))
    data = data.astype(np.float32)
    min = np.min(data.reshape((data.shape[0], -1)), axis=1)
    data = (data.transpose(1, 2, 3, 0) - min).transpose(3, 0, 1, 2)
    max = np.max(data.reshape((data.shape[0], -1)), axis=1)
    data = (data.transpose(1, 2, 3, 0) / max).transpose(3, 0, 1, 2)
    # Inverting the colors seems to look better for MNIST
    # data = 1 - data

    n = int(np.ceil(np.sqrt(data.shape[0])))
    padding = ((0, n ** 2 - data.shape[0]), (0, 0),
               (0, 0)) + ((0, 0),) * (data.ndim - 3)
    data = np.pad(data, padding, mode='constant',
                  constant_values=0)
    # Tile the individual thumbnails into an image.
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3)
                                                           + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
    data = (data * 255).astype(np.uint8)
    return data
