import os
import shutil
import tensorflow as tf
os.environ["CUDA_VISIBLE_DEVICES"] = "4"
gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(gpus[0], True)
from tensorboard.plugins.hparams import api as hp
import subprocess
import tensorflow_similarity as tfsim
from time import time
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
tfsim.utils.tf_cap_memory()
print("TensorFlow:", tf.__version__)
print("TensorFlow Similarity:", tfsim.__version__)



def get_params():
    hparams_dict = {
        'HP_EMBEDDING_SIZE': hp.HParam('embedding_size', hp.Discrete([512])),
        'HP_NUM_CLASSES': hp.HParam('num_classes', hp.Discrete([10])),
        'HP_EXAMPLES_PER_CLASS': hp.HParam('examples_per_class', hp.Discrete([2])),
        'HP_STEPS_PER_EPOCH': hp.HParam('steps_per_epoch', hp.Discrete([50])),
        'HP_NUM_TARGETS': hp.HParam('num_targets', hp.Discrete([300])),
        'HP_LOSS': hp.HParam('loss', hp.Discrete(['Circle512'])),
        'HP_MODE': hp.HParam('mode', hp.Discrete(['EffNetSim224Part'])),
        'HP_AUGMENTER': hp.HParam('augmenter', hp.Discrete([#'None',
                                                            #'RandAug0','RandAug1','RandAug2','RandAug3','RandAug4','RandAug5','RandAug6',#'RandAug7','RandAug8','RandAug9','RandAug10','RandAug11','RandAug12','RandAug13','RandAug14','RandAug15','RandAug16','RandAug17','RandAug18','RandAug19',
                                                            #'SimCLR0','SimCLR1','SimCLR2','SimCLR3','SimCLR4','SimCLR5','SimCLR6',#'SimCLR7','SimCLR8','SimCLR9',
                                                            #'TfsimRandAug0','TfsimRandAug1','TfsimRandAug2','TfsimRandAug3','TfsimRandAug4',#'TfsimRandAug5','TfsimRandAug6','TfsimRandAug7','TfsimRandAug8','TfsimRandAug9'
                                                            'TfsimSimCLR0', 'TfsimSimCLR1','TfsimSimCLR2','TfsimSimCLR3','TfsimSimCLR4',#
                                                           ])),
        'HP_PARAMETER': hp.HParam('parameter', hp.Discrete(['None'])),
        'HP_NUM_NEIGHBOURS': hp.HParam('num_neighbours', hp.Discrete([2])),
        'HP_LEARNING_RATE': hp.HParam('learning_rate', hp.Discrete([0.0001])),
        'HP_EPOCHS': hp.HParam('num_epochs', hp.Discrete([5])),
    }
    return hparams_dict

def hparam_wrap(name, dataset):
    hparams_dict = get_params()

    session_num = 0
    for ez in hparams_dict['HP_EMBEDDING_SIZE'].domain.values:
        for nc in hparams_dict['HP_NUM_CLASSES'].domain.values:
            for epc in hparams_dict['HP_EXAMPLES_PER_CLASS'].domain.values:
                for spe in hparams_dict['HP_STEPS_PER_EPOCH'].domain.values:
                    for nt in hparams_dict['HP_NUM_TARGETS'].domain.values:
                        for l in hparams_dict['HP_LOSS'].domain.values:
                            for m in hparams_dict['HP_MODE'].domain.values:
                                for a in hparams_dict['HP_AUGMENTER'].domain.values:
                                    for p in hparams_dict['HP_PARAMETER'].domain.values:
                                        for lr in hparams_dict['HP_LEARNING_RATE'].domain.values:
                                            for e in hparams_dict['HP_EPOCHS'].domain.values:
                                                for nn in hparams_dict['HP_NUM_NEIGHBOURS'].domain.values:
                                                        hparams = {
                                                            'HP_EMBEDDING_SIZE': ez,
                                                            'HP_NUM_CLASSES': nc,
                                                            'HP_EXAMPLES_PER_CLASS': epc,
                                                            'HP_STEPS_PER_EPOCH': spe,
                                                            'HP_NUM_TARGETS': nt,
                                                            'HP_LOSS': l,
                                                            'HP_MODE': m,
                                                            'HP_AUGMENTER': a,
                                                            'HP_LEARNING_RATE': lr,
                                                            'HP_EPOCHS': e,
                                                            'HP_NUM_NEIGHBOURS': nn,
                                                            'HP_PARAMETER': p,

                                                        }
                                                        print(hparams)
                                                        run_name = "run-%d" % session_num
                                                        print('--- Starting trial: %s' % run_name)
                                                        print({h: hparams[h] for h in hparams})

                                                        base_path = os.path.dirname((__file__))
                                                        path_subpro = os.path.join(base_path,
                                                                     'RunGS.py'
                                                                     )
                                                        subprocess.call(
                                                            ['python',
                                                             path_subpro,
                                                             '%s' % name,
                                                             '%d' % ez,
                                                             '%d' % nc,
                                                             '%d' % epc,
                                                             '%d' % spe,
                                                             '%d' % nt,
                                                             '%s' % l,
                                                             '%s' % m,
                                                             '%s' % a,
                                                             '%f' % lr,
                                                             '%d' % e,
                                                             '%d' % nn,
                                                             '%s' % p,
                                                             '%s' % dataset,
                                                             '%s' % run_name,
                                                             ])
                                                        session_num += 1
                                                        tf.keras.backend.clear_session()
def main(dataset='cifar10',name='Test'):
    hparam_wrap(name,dataset)
if __name__ == '__main__':
        exit(main(dataset='cifar10',name='WithAllAugs'))

