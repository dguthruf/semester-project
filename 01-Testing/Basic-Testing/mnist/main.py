import tensorflow as tf
from tensorboard.plugins.hparams import api as hp
from model import train_test_model
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "4"


def run(run_dir, hparams):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)  # record the values used in this trial
        accuracy, f1, cm_image = train_test_model(hparams)
        tf.summary.scalar('Accuracy', accuracy, step=1)
        tf.summary.scalar('F1-Score', f1, step=1)
        tf.summary.image('Confusion Matrix', cm_image, step=1)


def main():
    # HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([64, 128]))
    # HP_DROPOUT = hp.HParam('dropout', hp.RealInterval(0.3, 0.5))
    HP_NUM_CLASSES = hp.HParam('num_classes', hp.Discrete([1, 10]))
    # HP_STEP_EPOCH = hp.HParam('num_steps', hp.Discrete([500,1000]))
    # P_EXP_CLASS = hp.HParam('num_ex_classes', hp.Discrete([4,10]))
    HP_NUM_EPOCHS = hp.HParam('num_epochs', hp.Discrete([2, 3]))

    session_num = 0
    for num_classes in HP_NUM_CLASSES.domain.values:
        for num_epochs in HP_NUM_EPOCHS.domain.values:
            hparams = {
                HP_NUM_CLASSES: num_classes,
                HP_NUM_EPOCHS: num_epochs
            }

            run_name = "run-%d" % session_num
            print('--- Starting trial: %s' % run_name)
            print({h.name: hparams[h] for h in hparams})
            run('logs/hparam_tuning/' + run_name, hparams)
            session_num += 1


if __name__ == '__main__':
    exit(main())
