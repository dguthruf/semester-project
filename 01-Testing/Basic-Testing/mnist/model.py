import pandas as pd
import sklearn.metrics
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow_similarity.layers import MetricEmbedding  # row wise L2 norm
from tensorflow_similarity.losses import MultiSimilarityLoss  # specialized similarity loss
from tensorflow_similarity.models import SimilarityModel  # TF model with additional features
from tensorflow_similarity.samplers import MultiShotMemorySampler  # sample data
from tensorflow_similarity.samplers import select_examples  # select n example per class
import disarray
from visualizations import plot_to_image, plot_confusion_matrix

(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
CLASSES = [2, 3, 1, 7, 9, 6, 8, 5, 0, 4]


def get_model(hparams):
    NUM_CLASSES = list(hparams.values())[0]
    CLASSES_PER_BATCH = NUM_CLASSES
    EXAMPLES_PER_CLASS = 6
    STEPS_PER_EPOCH = 1000  # @param {type:"integer"}

    sampler = MultiShotMemorySampler(x_train, y_train,
                                     classes_per_batch=CLASSES_PER_BATCH,
                                     examples_per_class_per_batch=EXAMPLES_PER_CLASS,
                                     class_list=CLASSES[:NUM_CLASSES],  # Only use the first 6 classes for training.
                                     steps_per_epoch=STEPS_PER_EPOCH)

    inputs = layers.Input(shape=(28, 28, 1))
    x = layers.experimental.preprocessing.Rescaling(1 / 255)(inputs)
    x = layers.Conv2D(32, 3, activation='relu')(x)
    x = layers.Conv2D(32, 3, activation='relu')(x)
    x = layers.MaxPool2D()(x)
    x = layers.Conv2D(64, 3, activation='relu')(x)
    x = layers.Conv2D(64, 3, activation='relu')(x)
    x = layers.Flatten()(x)
    # smaller embeddings will have faster lookup times while a larger embedding will improve the accuracy up to a point.
    outputs = MetricEmbedding(64)(x)

    distance = 'cosine'  # @param ["cosine", "L2", "L1"]{allow-input: false}
    loss = MultiSimilarityLoss(distance=distance)

    return SimilarityModel(inputs, outputs), distance, loss, sampler


def train_test_model(hparams):
    model, distance, loss, sampler = get_model(hparams)
    model.compile(optimizer='adam', loss=loss)

    EPOCHS = list(hparams.values())[1]
    history = model.fit(sampler, epochs=EPOCHS, validation_data=(x_test, y_test))
    x_index, y_index = select_examples(x_train, y_train, CLASSES, 20)
    model.reset_index()
    model.index(x_index, y_index, data=x_index)

    num_calibration_samples = 1000  # @param {type:"integer"}
    calibration = model.calibrate(
        x_train[:num_calibration_samples],
        y_train[:num_calibration_samples],
        extra_metrics=['precision', 'recall', 'binary_accuracy'],
        verbose=1)

    labels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Unknown"]
    num_examples_per_class = 2000
    cutpoint = 'optimal'

    x_confusion, y_confusion = select_examples(x_test, y_test, CLASSES, num_examples_per_class)

    matches = model.match(x_confusion, cutpoint=cutpoint, no_match_label=10)
    cm = sklearn.metrics.confusion_matrix(y_confusion, matches)
    figure = plot_confusion_matrix(cm, class_names=labels)
    cm_image = plot_to_image(figure)
    cm = tf.math.confusion_matrix(y_confusion, matches)
    cm = tf.cast(cm, dtype='float')
    df = pd.DataFrame(cm.numpy().astype(int))
    df.da.export_metrics()
    accuracy = df.da.micro_accuracy
    f1 = df.da.micro_f1
    return accuracy, f1, cm_image
