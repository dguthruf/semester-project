import os
import tensorflow as tf
from sklearn.metrics import pairwise_distances

import os
import io
import tensorflow as tf
import numpy as np
import cv2
from tensorboard.plugins import projector
from collections import Counter

import tensorflow_similarity as tfsim
from typing import Mapping, Optional, Sequence, Tuple
from tensorflow_similarity.types import Tensor, Lookup
from matplotlib import pyplot as plt
from mpl_toolkits import axes_grid1
from matplotlib.patches import Ellipse
import umap.umap_ as umap
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
tfsim.utils.tf_cap_memory()
from utils import select_examples


def plot_to_image(figure):
  """Converts the matplotlib plot specified by 'figure' to a PNG image and
  returns it. The supplied figure is closed and inaccessible after this call."""
  # Save the plot to a PNG in memory.
  buf = io.BytesIO()

  plt.savefig(buf, format='png',bbox_inches='tight',pad_inches=0.2,dpi=200)

  # Closing the figure prevents it from being displayed directly inside
  # the notebook.
  plt.close(figure)
  buf.seek(0)
  # Convert PNG buffer to TF image
  image = tf.image.decode_png(buf.getvalue(), channels=4)
  # Add the batch dimension
  image = tf.expand_dims(image, 0)
  return image


def tensorboard_visualization(class_mappings,model_path,tensorboard_path,calibration):
    """
    :param model: model to generate embedding vector
    :param calibration: calibration from tfsim_function, to plot calibration
    :param class_mappings: contains all class_mappings for the diffrent groupings of the multiclasses
    :param log_dir:
    :param train_method: on which grouping of the multiclasses the training was done.
    :return:
    In tensorboard
    confusion matrix
    calibration images
    UMAP
    And neareast neighbours in embedding Space via cosine distance
    projection of embedding space
    """
    reloaded_model = tf.keras.models.load_model(
        model_path,
        custom_objects={"SimilarityModel": tfsim.models.SimilarityModel},
    )
    # reload the index
    reloaded_model.load_index(model_path)


    queries_x = class_mappings["high_balance"]["X_test"]
    queries_y = class_mappings["high_balance"]["Y_test"]
    labels = [*class_mappings["high_balance"]["index_to_label"].values()]
    labels.append('unknown')

    #Plot the calibration results
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))
    x = calibration.thresholds["distance"]

    ax1.plot(x, calibration.thresholds["precision"], label="precision")
    ax1.plot(x, calibration.thresholds["recall"], label="recall")
    ax1.plot(x, calibration.thresholds["f1"], label="f1 score")
    ax1.legend()
    ax1.set_title("Metric evolution as distance increase")
    ax1.set_xlabel("Distance")
    ax1.set_ylim((-0.05, 1.05))

    ax2.plot(calibration.thresholds["recall"], calibration.thresholds["precision"])
    ax2.set_title("Precision recall curve")
    ax2.set_xlabel("Recall")
    ax2.set_ylabel("Precision")
    ax2.set_ylim((-0.05, 1.05))
    metric_image = plot_to_image(fig)

    #Cutpoint from calibration was choosen optimal
    cutpoint = "optimal"

    #Classification via matching and looking what the nearest neighbours classes are
    matches = reloaded_model.match(queries_x, cutpoint=cutpoint, no_match_label=len(labels))

    #Create Confusion_matrix
    pt = tfsim.visualization.confusion_matrix(
        matches,
        queries_y,
        labels=labels,
        title="Confusion matrix for cutpoint:%s" % cutpoint,
        normalize=True,
        show=False
    )


    fig_size = (40,40)
    #Add plots to tensorboard
    umap_fig = my_umap(reloaded_model, class_mappings)
    umap_image = plot_to_image(umap_fig)
    umap_fig_val = my_umap(reloaded_model, class_mappings,data_split="test")
    umap_image_val = plot_to_image(umap_fig_val)

    cm_image = plot_to_image(pt.Figure(figsize=fig_size))

    with tf.summary.create_file_writer(tensorboard_path + '/Images').as_default():
        tf.summary.image("Confusion Matrix", cm_image, step=1)
        tf.summary.image("Calibrated Metric", metric_image, step=1)
        tf.summary.image("UMAP-Train", umap_image, step=1)
        tf.summary.image("UMAP-Val", umap_image_val, step=1)


    # Call Projection Function and visuallize in tensorboard
    project_embedding(reloaded_model, class_mappings, tensorboard_path)
    project_embedding(reloaded_model, class_mappings, tensorboard_path,sprite_imgs=0)
    get_viz_neighbours(class_mappings,reloaded_model,tensorboard_path)





def project_embedding(model,class_mappings,log_dir,examples_per_class=500,data_split="test",sprite_imgs=1):
    """
    :param model: model needed for generating embedding vector of each image
    :param class_mappings: dicts for class label corresponance
    :param log_dir:
    :return:
    The Projection in tensorbaord
    """
    targets_index = []
    queries_index =[]
    train_class_list = [*Counter(class_mappings["high_balance"]["Y_"+data_split]).keys()]
    train_class_list.sort()
    num_classes = len(train_class_list)
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(class_mappings["high_balance"]["Y_"+data_split]) if y == c]
        targets_index.extend(c_index[0:examples_per_class])

        del c_index
    targets_x, targets_y_m = class_mappings["high_balance"]["X_"+data_split][targets_index], class_mappings["high_balance"]["Y_"+data_split][targets_index]
    targets_y_s = class_mappings["abbreviated"]["Y_"+data_split][targets_index]
    targets_y_f = class_mappings["written_out"]["Y_"+data_split][targets_index]


    #maximal image size allowed for visuallizing in tensorboard
    size = int(7656/(np.sqrt(len(targets_y_m)+1)))
    img_data = []
    for img in targets_x:
        input_img_resize=cv2.resize(img,(size,size))
        img_data.append(input_img_resize)

    #Log dir of projection
    if sprite_imgs:
        dir = 'train'
    else:
        dir = "validation"
    logdir = os.path.join(log_dir, dir)

    #genearte metadatafile needed to assign each dot /image in embedding space a lable
    metadata_file = open(os.path.join(logdir, 'metadata.tsv'), 'a+')

    metadata_file.write('Label\tAbbreviation\tNumber\tHighBalance\tHighBalanceNumber\n')
    for i in range(len(targets_y_m)):
        vy_m = targets_y_m[i]

        vy_f = targets_y_f[i]
        vy_s = targets_y_s[i]

        cm_f = class_mappings["written_out"]["index_to_label"][vy_f]
        cm_s = class_mappings["abbreviated"]["index_to_label"][vy_s]
        cm_m = class_mappings["high_balance"]["index_to_label"][vy_m]


        metadata_file.write('{}\t{}\t{}\t{}\t{}\n'.format(cm_f,cm_s,vy_f,cm_m,vy_m))


    #generate sprites, for visuallization in tensorboard
    if sprite_imgs:
        sprite = images_to_sprite(np.array(img_data))
        cv2.imwrite(os.path.join(logdir, 'sprite.png'), sprite)

    emb = model.predict(targets_x)
    # Save the weights we want to analyze as a variable.
    # The weights need to have the shape (Number of sample, Total Dimensions)
    # Hence why we flatten the Tensor
    weights = tf.Variable(tf.reshape(emb,(targets_x.shape[0],-1)), name="emb")
    # Create a checkpoint from embedding, the filename and key are the
    # name of the tensor.
    checkpoint = tf.train.Checkpoint(emb=weights)
    checkpoint.save(os.path.join(logdir, "embedding.ckpt"))

    # Set up config.
    config = projector.ProjectorConfig()
    embedding = config.embeddings.add()
    embedding.tensor_name = "emb/.ATTRIBUTES/VARIABLE_VALUE"
    embedding.metadata_path = 'metadata.tsv'
    if sprite_imgs:
        embedding.sprite.image_path = 'sprite.png'
    embedding.sprite.single_image_dim.extend([size, size]) # image size = 28x28
    # The name of the tensor will be suffixed by `/.ATTRIBUTES/VARIABLE_VALUE`.
    projector.visualize_embeddings(logdir, config)


def images_to_sprite(data):
    """Creates the sprite image along with any necessary padding

    Args:
      data: NxHxW[x3] tensor containing the images.

    Returns:
      data: Properly shaped HxWx3 image with any necessary padding.
    """
    if len(data.shape) == 3:
        data = np.tile(data[..., np.newaxis], (1, 1, 1, 3))
    data = data.astype(np.float32)
    min = np.min(data.reshape((data.shape[0], -1)), axis=1)
    data = (data.transpose(1, 2, 3, 0) - min).transpose(3, 0, 1, 2)
    max = np.max(data.reshape((data.shape[0], -1)), axis=1)
    data = (data.transpose(1, 2, 3, 0) / max).transpose(3, 0, 1, 2)
    # Inverting the colors seems to look better for MNIST
    # data = 1 - data

    n = int(np.ceil(np.sqrt(data.shape[0])))
    padding = ((0, n ** 2 - data.shape[0]), (0, 0),
               (0, 0)) + ((0, 0),) * (data.ndim - 3)
    data = np.pad(data, padding, mode='constant',
                  constant_values=0)
    # Tile the individual thumbnails into an image.
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3)
                                                           + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
    data = (data * 255).astype(np.uint8)
    return data

def plot_25(x, y):
    """Plot the first 25 images."""
    num_cols = num_rows = 5
    fig = plt.figure(figsize=(6.0, 6.0))
    grid = axes_grid1.ImageGrid(
        fig=fig,
        rect=111,
        nrows_ncols=(num_cols, num_rows),
        axes_pad=0.1,
    )
    for ax, im, label in zip(grid, x, y):
        max_val = float(tf.math.reduce_max(im))
        ax.imshow(im / max_val)
        ax.axis("off")

def get_viz_neighbours(class_mappings, viz_model,tensorboard_path, train_method="high_balance", examples_per_class=100, datatype="train"):
    targets_index = []
    queries_index =[]
    train_class_list = [*Counter(class_mappings[train_method]["Y_" + datatype]).keys()]
    train_class_list.sort()
    num_classes = len(train_class_list)
    for c in range(num_classes):
        c_index = [i for i, y in enumerate(class_mappings[train_method]["Y_" + datatype]) if y == c]
        queries_index.append(c_index[0])
        targets_index.extend(c_index[1:examples_per_class])

        del c_index
    targets_x, targets_y_high_balance = class_mappings[train_method]["X_" + datatype][targets_index], class_mappings[train_method]["Y_" + datatype][targets_index]
    targets_y_multi = class_mappings["abbreviated"]["Y_"+datatype][targets_index]

    num_neighbors = 6
    # select
    x_display = class_mappings[train_method]["X_" + datatype][queries_index]
    y_display_high_balance = class_mappings[train_method]["Y_" + datatype][queries_index]
    y_display_multi = class_mappings["abbreviated"]["Y_"+datatype][queries_index]

    viz_model.reset_index()
    viz_model.index(targets_x, targets_y_multi, data=targets_x)
    nns_multi = viz_model.lookup(x_display, k=num_neighbors)

    # display
    viz_neigh_images = []
    for idx in np.argsort(y_display_high_balance):
        pt,viz_name = viz_neighbours(x_display[idx],
                                     y_display_high_balance[idx],
                                     y_display_multi[idx],
                                     nns_multi[idx],
                                     Mapping_dic=class_mappings[train_method]["multi_to_main"],
                                     high_balance_mapping=class_mappings[train_method]["index_to_label"],
                                     multi_mapping=class_mappings["abbreviated"]["index_to_label"],
                                     fig_size=(30, 15),
                                     show=False)
        viz_image=plot_to_image(pt.Figure(figsize=(30, 15)))
        viz_neigh_images.append(viz_image)
        with tf.summary.create_file_writer(tensorboard_path + '/Images').as_default():
            tf.summary.image(viz_name, viz_image, step=1)
    return viz_neigh_images

def viz_neighbours(example: Tensor,
                   example_class_high_balance: int,
                   example_class_multi: int,
                   neighbors_multi: Sequence[Lookup],
                   Mapping_dic: Optional[Mapping[int, str]] = None,
                   high_balance_mapping: Optional[Mapping[int, str]] = None,
                   multi_mapping: Optional[Mapping[int, str]] = None,
                   fig_size: Tuple[int, int] = (30, 6),
                   show: bool = True):
    """Display images nearest neighboors
    Args:
        example: The data used as query input.
        example_class: The class of the data used as query
        neighbors: The list of neighbors returned by the lookup()
        class_mapping: Mapping from class numerical ids to a class name. If not
        set, the plot will display the class numerical id instead.
        Defaults to None.
        fig_size: Size of the figure. Defaults to (24, 4).
        cmap: Default color scheme for black and white images e.g mnist.
        Defaults to 'viridis'.
        show: If the plot is going to be shown or not. Defaults to True.
    """
    num_cols = len(neighbors_multi) + 1
    fig, axs = plt.subplots(1, num_cols, figsize=fig_size)

    axs[0].imshow(example)
    axs[0].set_xticks([])
    axs[0].set_yticks([])
    high_balance_label = _get_class_label(example_class_high_balance, high_balance_mapping)
    multi_label = _get_class_label(example_class_multi, multi_mapping)
    axs[0].set_title(high_balance_label)
    axs[0].set_xlabel(multi_label)
    for ax,nbg_multi in zip(axs[1:],neighbors_multi):

        multi_val = _get_class_label(nbg_multi.label, multi_mapping)
        high_balance_val = [c for c in Mapping_dic if nbg_multi.label in Mapping_dic[c]][0]
        top = f"{high_balance_val} - {nbg_multi.distance:.5f}"
        bot = f"{multi_val}"
        ax.imshow(nbg_multi.data)
        ax.set_title(top)
        ax.set_xlabel(bot)
        ax.set_xticks([])
        ax.set_yticks([])
    if show:
        plt.show()
    else:
        return plt,high_balance_label


def _get_class_label(example_class, class_mapping):
    if example_class is None:
        return 'No Label'

    if class_mapping is None:
        return str(example_class)

    class_label = class_mapping.get(example_class)
    return class_label if class_label is not None else str(example_class)


def my_umap(model, class_mappings, grouping_method="high_balance", metric="Cosine", cmap='tab10', examples_per_class=100, data_split="train"):
    n_neighbors = [5,15,30,100,250]
    min_dists = [0,0.3,0.6, 0.9]
    data = class_mappings[grouping_method]["X_" + data_split]
    labels = class_mappings[grouping_method]["Y_" + data_split]


    classes = [*class_mappings[grouping_method]["index_to_label"].values()]
    num_classes = len(class_mappings[grouping_method]["index_to_label"])

    data,labels = select_examples(data,labels,num_examples_per_class=examples_per_class)
    data = model.predict(data)
    pre_knn_emb = np.zeros((len(n_neighbors), len(min_dists), len(data), 2))

    for i, k in enumerate(n_neighbors):
        for j, dist in enumerate(min_dists):
            pre_knn_emb[i, j] = umap.UMAP(n_neighbors=k,
                                          min_dist=dist,
                                          random_state=42,
                                          # precomputed_knn=pre_knn,
                                          ).fit_transform(data)
    fig, axs = plt.subplots(len(n_neighbors), len(min_dists), figsize=(30, 30))
    for i, ax_row in enumerate(axs):
        for j, ax in enumerate(ax_row):
            scatter = ax.scatter(pre_knn_emb[i, j, :, 0],
                                 pre_knn_emb[i, j, :, 1],
                                 c=labels / num_classes,
                                 cmap=cmap,
                                 alpha=0.2,
                                 s=100,
                                 )
            leg = scatter.legend_elements()[0]
            for lh in leg:
                lh.set_alpha(1)

            ax.legend(handles=leg, labels=classes)

            ax.set_xticks([])
            ax.set_yticks([])
            if i == 0:
                ax.set_title("min_dist = {}".format(min_dists[j]), size=15)
            if j == 0:
                ax.set_ylabel("n_neighbors = {}".format(n_neighbors[i]), size=15)

    fig.suptitle("High Balance Grouping || Metric: " + metric+" || Split: "+data_split, y=0.92, size=20)
    plt.subplots_adjust(wspace=0.05, hspace=0.05)
    return fig


