import os
import random

import numpy as np
import squirrel as sq
import tensorflow as tf
try:
    tf.config.experimental.set_memory_growth(gpus[0], True)
except:
    pass

from tensorflow import keras
from tqdm.auto import tqdm
from collections import Counter
from typing import Sequence, Tuple
from collections import defaultdict
from multiprocessing import cpu_count
from sklearn.model_selection import train_test_split
FLAGS = tf.compat.v1.flags.FLAGS
FLAGS.__delattr__("wikipedia_auto_select_flume_mode")

from tensorflow_similarity.types import IntTensor, FloatTensor
# from augmenters import RandAugment,AutoAugment,SimCLRAugmenter,RandomErasing


def resize(img, img_size):
    """
    Done with tf.image.resize and CPU
    :param img: (num_samples,size,size,channels)
    :param img_size: wanted final image size
    :return: resized images (num_samples,img_size,img_size,channels)
    """
    with tf.device("/cpu:0"):
        # Resize according to needed img_size for backbone Efficinetnet
        img = tf.image.resize(img, [img_size, img_size])
        # Rescale to [0,255]
        img = tf.keras.layers.Rescaling(255 / 65536)(img)
        return img.numpy()


def get_class_mappings(y):
    """
    assings each single label to a high_balance_class defined in the dict below

    :param y: labels array with all single classes
    :return:
    new_high_balance class labels as array y = (int,int,int,...)
    new_high_balance labels as strings ("Round","Asymmetric",....)
    high_balance_dict containg classes and corresponding labels
    """
    # Each multiclass label [0,1,...,42] is not only the label but also the order of number of samples within each
    # class, therefore we have the most examples of class 0 and the least amount of examples in class 42


    # My grouping of the multiclasses with all 43 multiclasses, where I created 10 high_balance classes, each describing
    # the most prominent feature. The multiclasses are into the high_balance classes sucht that I achive a good balance of samples
    # for each high_balanceclass
    #BAD SOLUTION: mapping [20] is only prolate classes with total of 7 samples, I put them into the Bent classes...
    multiclass_mapping_high_balance = {}
    multiclass_mapping_high_balance["Round"]        = [0]
    multiclass_mapping_high_balance["Elliptical"]   = [1, 10]
    multiclass_mapping_high_balance["NewtonRings"]  = [2]
    multiclass_mapping_high_balance["Asymmetric"]   = [3, 14, 16, 37]
    multiclass_mapping_high_balance["Empty"]        = [4]
    multiclass_mapping_high_balance["Bent"]         = [5, 8, 20, 23, 24, 27]
    multiclass_mapping_high_balance["Streak"]       = [6, 11, 12, 18, 26, 29, 30]
    multiclass_mapping_high_balance["DoubleRings"]  = [7, 9, 15, 17, 19, 22, 39, 42]
    multiclass_mapping_high_balance["Layered"]      = [13, 21, 25, 28, 31, 32, 33, 34, 35, 36, 38, 40, 41]
    # multiclass_mapping_high_balance["Prolate"]      = [20]


    class_mappings = {"high_balance": {}}
    class_mappings["high_balance"]["multi_to_main"] = multiclass_mapping_high_balance


    # Create Class Dics that give the corresponding label written out as str to the class as int
    new_labels_defined_by_mapping = []
    for i, label_i in enumerate(y):
        for j, mapping_class_name_j in enumerate(
                [*class_mappings["high_balance"]["multi_to_main"].keys()]):
            if label_i in class_mappings["high_balance"]["multi_to_main"][mapping_class_name_j]:
                new_mapping_defined_label = j

        new_labels_defined_by_mapping.append(new_mapping_defined_label)

    mapping_labels = np.array([*class_mappings["high_balance"]["multi_to_main"].keys()])
    new_dict = {c_id: c_lbl for c_id, c_lbl in zip(range(len(mapping_labels)), mapping_labels)}
    class_mappings["high_balance"]["index_to_label"] = new_dict
    class_mappings["high_balance"]["Y"] = np.array(new_labels_defined_by_mapping)

    return class_mappings


def categorical_to_integer(y):
    """
    Labeling the categorical labels according to your wishes(info from squirrel load dataset)
    abbreviated and written_out description possible
    :param y: np.array of catecorical lables array([0,0,1,2....],[1,1,0,....],...)
    :return:
    Dicts with classes corresponding to their labels(long and abbreviated version)
    And array of single_y int label for each class sorted according to their sample frequency.
    dict_written_out   = {
                     0:spherical_oblate_round_...
                     1:spherical_oblate_elliptical_...
                     2: ....}
    dict_abbreviated  = {
                     0:spherical_oblate_round_...
                     1:spherical_oblate_elliptical_...
                     2: ....}
    y           = array(0,1,3,22,3,0,38,...)

    """
    # Define abbreviated_label_names for dicts
    labels_written_out = np.array(
        ["spherical_oblate", "round", "elliptical", "prolate", "streak", "bent", "asymmetric", "newtonrings",
         "doublerings", "layered", "empty"])
    labels_abbreviated = np.array(["SO", "R", "E", "P", "S", "B", "A", "NR", "DR", "L", "EY"])
    list_written_out = []
    list_abbreviated = []

    # Go through each categrical 11-Dim label
    for single_y in y:
        # convert to bool
        single_y = single_y.astype(dtype=bool)
        # Transform bool to combination of classes
        # Example: y =   [1,1,1,0,0,0,0,0,1,0,0]
        #         Apply y-bool to dictionary:labels_written_out
        #         label="spherical_oblate-round-elliptical-doublerings"
        label_written_out = '-'.join(labels_written_out[single_y])
        label_abbreviated = '-'.join(labels_abbreviated[single_y])
        list_written_out.append(label_written_out)
        list_abbreviated.append(label_abbreviated)

    # Get all class combinations label_names
    abbreviated_label_names = np.array([*Counter(list_abbreviated).keys()])
    written_out_label_names = np.array([*Counter(list_written_out).keys()])

    # Get frequency of each class and sort according to their frequency
    abbreviated_label_frequency = np.array([*Counter(list_abbreviated).values()])
    sorted_index_by_frequency = np.argsort(-abbreviated_label_frequency)
    sorted_abbreviated_label_names = abbreviated_label_names[sorted_index_by_frequency].tolist()
    sorted_written_out_label_names = written_out_label_names[sorted_index_by_frequency].tolist()

    # Create dicts with class label correspondance
    y_dict = dict(zip(sorted_abbreviated_label_names, range(len(sorted_abbreviated_label_names))))
    dict_abbreviated = dict(zip(range(len(sorted_abbreviated_label_names)), sorted_abbreviated_label_names))
    dict_written_out = dict(zip(range(len(sorted_abbreviated_label_names)), sorted_written_out_label_names))

    # create single_y class int array to be able to workt with tensorflow_similarity
    y = np.array([y_dict[ls] for ls in list_abbreviated])

    class_mappings = {"written_out": {}, "abbreviated": {}}
    class_mappings["written_out"]["index_to_label"] = dict_written_out
    class_mappings["abbreviated"]["index_to_label"] = dict_abbreviated
    class_mappings["abbreviated"]["Y"] = y
    class_mappings["written_out"]["Y"] = y

    return y, class_mappings


def add_augmentation_low_frequency_class(all_data_x, all_data_y, split_x, split_y, number_of_minimum_samples_per_class):
    """
    :param all_data_x: (num_samples,img_size,img_size,channels) all samples
    :param all_data_y: array(int,int,int...) all samples
    :param split_x: (num_samples,img_size,img_size,channels)
    :param split_y: array(int,int,int...) with maybe missing classes i.e test split after stratify split
    :param number_of_minimum_samples_per_class: number of max augmentations
    :return:
    split_x,split_y with at least num_augs samples per class
    """
    # Array with all classes and found_classes
    all_possible_y = [*Counter(all_data_y).keys()]

    # Define Augmentations that should be applied to increase samples from <num_augs to = num_augs
    augmentation_layers = keras.Sequential(
        [
            keras.layers.RandomFlip("horizontal_and_vertical"),
            keras.layers.RandomContrast(0.5),
            keras.layers.RandomTranslation(0.05, 0.05),
            keras.layers.RandomZoom(0.05),
        ]
    )
    additional_augmented_x = []
    additional_augmented_y = []

    for y in all_possible_y:
        indexes_of_y_occurences = [i for i, y_single in enumerate(split_y) if y_single == y]
        if len(indexes_of_y_occurences) == 0:
            y_index_from_all = [i for i, ys in enumerate(all_data_y) if ys == y]
            for j in range(number_of_minimum_samples_per_class):
                additional_augmented_x.append(augmentation_layers(random.choice(all_data_x[y_index_from_all])))
                additional_augmented_y.append(y)
        elif len(indexes_of_y_occurences) < number_of_minimum_samples_per_class and len(indexes_of_y_occurences) > 0:
            y_index_from_all = [i for i, ys in enumerate(all_data_y) if ys == y]
            for j in range(number_of_minimum_samples_per_class - len(indexes_of_y_occurences)):
                additional_augmented_x.append(augmentation_layers(random.choice(all_data_x[y_index_from_all])))
                additional_augmented_y.append(y)
    try:
        split_x_with_augmentations = np.concatenate((np.array(additional_augmented_x), split_x))
        split_y_with_augmentations = np.concatenate((np.array(additional_augmented_y), split_y))
    except:
        pass
    return split_x_with_augmentations, split_y_with_augmentations


def get_data(
        img_size=224,
        test_size=0.2,
        number_of_minimum_samples_per_class=2,

):
    """
    Loads the unbalanced static helium_nanodroplets dataset with squirrel as X_train,y_train,X_test,y_test
    It is guaranteed that at least one example per class exists in test train split.
    --- Basic Information about dataset raw loaded with squirrel:

        Default Image data shape is (1035,1035) with 16 bit scaling (2^16=65536).
        11 Dimensional categorical labels
        43 Single Classes
        More info in "info" when loaded

    Parameters
    ----------
    img_size:
        Efficientnet B0 needs (224,224,3) with [0,255]
        Therefore we resize to (224,224)
    number_of_minimum_samples_per_class:
        num_augs defines the least number of samples that one class will have over all data points
        There exists classes with only 1 example. To achive at least num_aug of samples per class
        We apply num_augs augmentations to the existing samples.
        At least 2 examples per class are needed to apply contrastive learning.
    Returns
    ----------
    numpy Arrays of X_train,y_train,X_test,y_test
    X = array(num_samples,img_size,img_size,channels)
    y = array(int,int,int...)
    """
    # Load data wiht squirrel into one dataset with info and batch_size=-1 to load as one batch
    data, info = sq.load(name='static_helium_nanodroplets',
                         split=["train"],
                         with_info=True,
                         batch_size=-1)

    # transform to numpy for convinience and split into data and labels
    data = sq.as_numpy(data)
    all_data_x, all_data_y = data[0]["image"], data[0]["label"]

    # Resize and expand according to description above
    all_data_x = np.expand_dims(all_data_x, axis=3)
    all_data_x = resize(all_data_x, img_size)

    # Transform categorcial labels to single labelstring to make it usable for tensorflow similarity
    # Both dicts are essentially the same and contain all classes and their corresponding labels written out and abbreviated
    # Classes are also sorted according to their number of samples: Class 0 has most samples Class 1 has second most samples etc
    all_data_y, class_mappings = categorical_to_integer(all_data_y)

    # Add augmenations to classes with less than num_augs samples (more info about num_augs above)
    all_data_x, all_data_y = add_augmentation_low_frequency_class(all_data_x, all_data_y, all_data_x, all_data_y,
                                                                  number_of_minimum_samples_per_class)

    # stratify split data into train test
    X_train, X_test, y_train, y_test = train_test_split(all_data_x, all_data_y, test_size=test_size, stratify=all_data_y,random_state=42)


    # Get all classes in y_train and sort them, to get [0,1,2,3,4....,41,42] Class zero has most samples class 42 has the least.
    classes = [*Counter(y_train).keys()]
    classes.sort()

    # Get class_mapping dictionaries and mutli_occurence train and test arrays
    class_mappings_train = get_class_mappings(y_train)
    class_mappings_test = get_class_mappings(y_test)

    # Add train test dict entrys for each mapping
    new_class_mappings = {"written_out": {}, "abbreviated": {}, "high_balance": {}}
    for mapping in new_class_mappings:
        if mapping == "high_balance":
            new_class_mappings[mapping]["Y_train"] = class_mappings_train[mapping]["Y"]
            new_class_mappings[mapping]["X_train"] = X_train
            new_class_mappings[mapping]["X_test"] = X_test
            new_class_mappings[mapping]["Y_test"] = class_mappings_test[mapping]["Y"]
            new_class_mappings[mapping]["multi_to_main"] = class_mappings_train[mapping][
                "multi_to_main"]
            new_class_mappings[mapping]["index_to_label"] = class_mappings_train[mapping][
                "index_to_label"]
        else:
            new_class_mappings[mapping]["Y_train"] = y_train
            new_class_mappings[mapping]["X_train"] = X_train
            new_class_mappings[mapping]["X_test"] = X_test
            new_class_mappings[mapping]["Y_test"] = y_test
            new_class_mappings[mapping]["index_to_label"] = class_mappings[mapping][
                "index_to_label"]

    Y_train = new_class_mappings["high_balance"]["Y_train"]
    Y_test = new_class_mappings["high_balance"]["Y_test"]
    X_train = new_class_mappings["high_balance"]["X_train"]
    X_test = new_class_mappings["high_balance"]["X_test"]
    train_class_list = [*Counter(Y_train).keys()]


    train_class_list.sort()

    return X_train, X_test, Y_train, Y_test, train_class_list, new_class_mappings

def get_augmenter(augmenter_type, img_size):
    if augmenter_type == "AutoAugment" or augmenter_type == "SimCLR" or augmenter_type == "RandAugment" or augmenter_type == "RandomEarising":
        if augmenter_type == "AutoAugment":
            augmenter = AutoAugment(augmentation_name="reduced_cifar10")
        elif augmenter_type == "SimCLR":
            augmenter = SimCLRAugmenter(img_size, img_size)
        elif augmenter_type == "RandAugment":
            augmenter = RandAugment(num_layers=2, magnitude=8)
        elif augmenter_type == "RandomEarising":
            augmenter = RandomErasing(probability=0.4)

        @tf.function()
        def process(img):
            img = tf.image.grayscale_to_rgb(img)
            img = augmenter.distort(img)
            img = tf.image.rgb_to_grayscale(img)
            return img

        def loader(x, y, *args):
            imgs = tf.stack(x)
            imgs = tf.map_fn(process, imgs, parallel_iterations=cpu_count(), dtype="float32")
            return imgs, y

    elif augmenter_type == "NuxAugment":

        _augmentation = {"brightness": 0.1, "jitter": 0.3,
                         "scale": (0.5, 1.0), "fill_scale": (0.05, 0.15),
                         "fill_ratio": (0.5, 1.5)}

        nux_augmenter = gpu_augmenter(in_shape=(img_size, img_size, 1), ratio=(3 / 4, 4 / 3), strength=0.3,
                                      fill_value=0.,
                                      **_augmentation, model="efficient")

        def loader(x, y, *args):
            imgs = tf.stack(x)
            imgs = tf.keras.layers.Rescaling(1 / 255)(imgs)
            imgs = nux_augmenter(imgs)
            return imgs, y
    elif augmenter_type == "MySimCLR":
        my_sim_augmenter = MySim(min_area=0.75, brightness=0.4, jitter=0.1)
        #0.75,0.3,0.1
        def loader(x, y, *args):
            imgs = tf.stack(x)
            imgs = tf.keras.layers.Rescaling(1 / 255)(imgs)
            imgs = my_sim_augmenter(imgs)
            imgs = tf.keras.layers.Rescaling(255)(imgs)

            return imgs, y
    return loader

class RandomColorAffine(tf.keras.layers.Layer):
    """
    Adjusted to fit for 1-Channel images
    """

    def __init__(self, brightness=0., jitter=0., **kwargs):
        super().__init__(**kwargs)

        self.brightness = brightness
        self.jitter = jitter

    def call(self, images, training=True):
        if training:
            batch_size = tf.shape(images)[0]

            brightness_scales = 1 + tf.random.uniform(
                (batch_size, 1, 1, 1), minval=-self.brightness, maxval=self.brightness
            )
            jitter_matrices = tf.random.uniform(
                (batch_size, 1, 1, 1), minval=-self.jitter, maxval=self.jitter
            )

            color_transforms = (
                    tf.eye(1, batch_shape=[batch_size, 1]) * brightness_scales
                    + jitter_matrices
            )
            images = tf.clip_by_value(tf.matmul(images, tf.cast(color_transforms, images.dtype)), 0, 1)
        return images


def MySim(min_area=0.5, brightness=0.7, jitter=0.1):
    """
    Important: Augmentationlayers contain Rescaling layers adjsuted to static_helium_nanodroplet data and
    Efficientnetbackbone!
    Cropping: forces the model to encode different parts of the same image similarly, we implement it with the
    RandomTranslation and RandomZoom layers
    Color jitter: prevents a trivial color histogram-based solution to the task by distorting color histograms.
    A principled way to implement that is by affine transformations in color space.
    :param min_area: range(0,1) defines zoom-factor
    :param brightness: range(0,1) defines brightness change
    :param jitter: range(0,1) diorts color histogramm
    :return:
    A sequential keras model ready to be eaten by EfficientnetSim, architecture from tensorflow_similarity
    """
    zoom_factor = 1.0 - tf.sqrt(tf.cast(min_area,tf.float32))
    augmentation_layers = tf.keras.Sequential(
        [
            tf.keras.layers.RandomFlip("horizontal"),
            tf.keras.layers.RandomTranslation(zoom_factor / 2, zoom_factor / 2),
            tf.keras.layers.RandomZoom((-zoom_factor, 0.0), (-zoom_factor, 0.0)),
            RandomColorAffine(tf.cast(brightness,tf.float32), tf.cast(jitter,tf.float32)),
        ]
    )
    return augmentation_layers

def select_examples(x,y,
    class_list: Sequence[int] = None,
    num_examples_per_class: int = None,
) -> Tuple[np.ndarray, np.ndarray]:
    """Randomly select at most N examples per class

    Args:
        x: A 2-D Tensor containing the data.

        y: A 1-D Tensor containing the labels.

        class_list: Filter the list of examples to only keep thoses those who
        belong to the supplied class list. In no class is supplied, keep
        examples for all the classes. Default to None - keep all the examples.

        num_examples_per_class: Restrict the number of examples for EACH
        class to num_examples_per_class if set. If not set, all the available
        examples are selected. Defaults to None - no selection.

    Returns:
        A Tuple containing the subset of x and y.
    """

    # cast class_list if it exist to avoid slowness
    if class_list is not None:
        class_list_int = set([int(c) for c in class_list])
    else:
        class_list_int = set([int(e) for e in y])

    # Mapping class to idx
    index_per_class = defaultdict(list)
    cls = [int(c) for c in y]

    for idx in tqdm(range(len(x)), desc="filtering examples"):
        cl = cls[idx]  # need to cast tensor

        # if user provided a class_list, check it's part of it.
        if class_list is not None and cl in class_list_int:
            index_per_class[cl].append(idx)
        else:
            # just add all class examples
            index_per_class[cl].append(idx)

    # restrict numbers of samples
    idxs = []
    for class_id in tqdm(class_list_int, desc="selecting classes"):
        class_idxs = index_per_class[class_id]

        # restrict num examples?
        if num_examples_per_class:
            if num_examples_per_class >= len(class_idxs):
                num_examples_per_class = len(class_idxs)
            random.seed(42)
            idxs.extend(random.choices(class_idxs, k=num_examples_per_class))
        else:
            idxs.extend(class_idxs)

    # random.shuffle(idxs)

    batch_x = []
    batch_y = []
    for idx in tqdm(idxs, desc="gather examples"):
        batch_x.append(x[idx])
        batch_y.append(y[idx])

    return np.array(batch_x), np.array(batch_y)