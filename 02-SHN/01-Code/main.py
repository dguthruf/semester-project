import os
import gc
import shutil
import pathlib

import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)

from collections import Counter
from utils import get_augmenter, get_data, select_examples

import tensorflow_similarity as tfsim
tfsim.utils.tf_cap_memory()
FLAGS = tf.compat.v1.flags.FLAGS
FLAGS.__delattr__("wikipedia_auto_select_flume_mode")

from tensorflow import keras
from tensorflow.keras.callbacks import TensorBoard
from visualizations import tensorboard_visualization
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.callbacks import Callback

from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.samplers import MultiShotMemorySampler  # sample data
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance

"""
-------------------------------------------------------------------------------------------------------------------
-----------------------------------------------Define Overall Things-----------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
model_name = "B5PartialHLR"              #Defines model save path and tensorboard log path

# Paths
working_path = pathlib.Path().resolve()
path_object = pathlib.Path(working_path)
parent_path = path_object.parent
model_parent_path = os.path.join(parent_path, "02-Models/")
log_parent_path = os.path.join(parent_path, "03-Logs/")

model_path = os.path.join(model_parent_path, model_name)
tensorboard_path = os.path.join(log_parent_path, model_name)

# Hyperparameters
# Sampler
augmenter_type = "MySimCLR"                 #Options: MySimCLR, RandAugment, AutoAugment, NuxAugment, RandomEarising, SimCLR. Go to utils to adjust specifics
EXAMPLES_PER_CLASS_TRAIN = 6                #Larger->larger batch_size, less jumping, slightly better results, higher memory usage

max_querries_per_class = 100                #Validation querries per class
max_targets_per_class = 200                 #Targets per class put into embeddingspace for ANN classification

# Model
model_mode = "partial"                      #Options: frozen, partial, full Partial unfrezzes last three layers. Frozen bad results, but fast, partial good results fast, full good results slow and high memory usage
model_variant = "B5"                        #Options: B0,B1,...,B7, Higher ->more memory usage slightly better results

learning_rate = 0.0001
epochs = 100
embedding_size = 128                        #Defines output dimensions of model, to low and to high gives bad results->64-512 are good values

optimizer = keras.optimizers.Nadam(
    learning_rate=learning_rate)            #Adam provides best results, AdaMax also good
distance = 'cosine'                         #Options: cosine, l1,l2. cosine provides superior results
loss = tfsim.losses.MultiSimilarityLoss(
    distance=distance)                      #Options: MultiSimilarityLoss, TripletLoss, PairwiseLoss,CircleLoss. MSL provides superior results, TripletLoss also good with hard mining

# Callbacks
monitor = 'f1score'                        #Options: val_loss,f1score,binary_accuracy. needs to be defined for earlystopping callback
monitor_mode = "max"
early_stopping_patience = 30
plateau_patience = 21                       #After how many non improving periods we adjust learning rate
factor = 0.3                                #Adjusting learning rate to  new_lr = lr * factor.
"""
-------------------------------------------------------------------------------------------------------------------
----------------------------------------------------Prepare Data---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
#If higher backbone chosen, need to resize img_size to fit to model input
variant_dict = {"B0": 224,
                "B1": 240,
                "B2": 260,
                "B3": 300,
                "B4": 380,
                "B5": 456,
                "B6": 528,
                "B7": 600}
img_size = variant_dict[model_variant]

augmenter = get_augmenter(augmenter_type=augmenter_type,
                          img_size=img_size)

X_train, X_val, Y_train, Y_val, train_class_list, class_mappings = get_data(img_size=img_size,
                                                                            test_size=0.35)

num_classes = len([*Counter(Y_val).keys()])
num_train_classes = len(train_class_list)

# Steps per epoch choosen such, that in one epoch whole data is run
STEPS_PER_EPOCH_TRAIN = len(Y_train) // (num_train_classes * EXAMPLES_PER_CLASS_TRAIN)

# Perpare train and val data with MultishotMemorySampler from tensorflow_similarity
# With that it is guaranteed that each batch contains at least 2 samples per class per batch
# If EXAMPLES_PER_CLASS >2 we have EXAMPLES_PER_CLASS per Batch Batchsize = EXAMPLES_PER_CLASS * NumClasses
train_ds = MultiShotMemorySampler(X_train, Y_train,
                                  classes_per_batch=num_train_classes,
                                  examples_per_class_per_batch=EXAMPLES_PER_CLASS_TRAIN,
                                  class_list=train_class_list,
                                  steps_per_epoch=STEPS_PER_EPOCH_TRAIN,
                                  augmenter=augmenter
                                  )

#Querry and Target Classes for classification to get f1score and binary accuracy at epoch end
targets_x, targets_y = select_examples(X_train, Y_train,
                                       num_examples_per_class=max_targets_per_class)
queries_x, queries_y = select_examples(X_val, Y_val,
                                       num_examples_per_class=max_querries_per_class)
"""
-------------------------------------------------------------------------------------------------------------------
--------------------------------------------------Prepare Modell---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
# Callbacks
# Needed if we train on all avaialbe classes
tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y,
                   k=5,
                   matcher = "match_majority_vote",
                   metrics=['f1score', 'binary_accuracy'])
tbc = TensorBoard(log_dir=tensorboard_path,
                  histogram_freq = 1,
                  profile_batch = '120,160')
early = EarlyStopping(monitor=monitor,
                      mode=monitor_mode,
                      patience=early_stopping_patience,
                      restore_best_weights=True,
                      verbose=1)
redonplat = ReduceLROnPlateau(monitor=monitor,
                              mode=monitor_mode,
                              patience=plateau_patience,
                              factor = factor,
                              verbose=2)
class ClearMemory(Callback):
    def on_epoch_end(self, epoch, logs=None):
        gc.collect()
        tf.keras.backend.clear_session()

callbacks = [tsc, tbc, early, redonplat,ClearMemory()]
if model_name=="Test":
    try:
        shutil.rmtree(tensorboard_path)
    except:
        pass

#We use pretrained weights from image_net
model = EfficientNetSim((img_size, img_size, 1),
                        embedding_size=embedding_size,
                        variant=model_variant,
                        trainable=model_mode,
                        weights="imagenet",
                        augmentation=None,
                        )
model.compile(optimizer,
              loss=loss,
              distance=distance)
model.summary()
# Start training
history = model.fit(train_ds,
                    epochs=epochs,
                    validation_data=(X_val, Y_val),
                    callbacks=callbacks,
                    )

"""
-------------------------------------------------------------------------------------------------------------------
--------------------------------------------------Post Training----------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
# Calibrate Model such that the calibration_metric is optimized
model.reset_index()
model.index(targets_x, targets_y, data=targets_x)
calibration = model.calibrate(
    queries_x,
    queries_y,
    calibration_metric="f1",
    matcher="match_nearest",
    extra_metrics=["precision", "recall", "binary_accuracy"],
    verbose=1,
)

# Save model
model.save(model_path, save_index=True)
"""
-------------------------------------------------------------------------------------------------------------------
--------------------------------------------------Visuallization---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
# Plott all figures and save in tensorboard
tensorboard_visualization(class_mappings, model_path, tensorboard_path,calibration)
