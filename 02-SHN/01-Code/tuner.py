import os
import pathlib
import gc
import tensorflow as tf
import numpy as np
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import keras_tuner as kt
os.environ["CUDA_VISIBLE_DEVICES"] = "3"
gpus = tf.config.experimental.list_physical_devices('GPU')
os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
tf.config.experimental.set_memory_growth(gpus[0], True)

from collections import Counter

import tensorflow_similarity as tfsim
tfsim.utils.tf_cap_memory()
FLAGS = tf.compat.v1.flags.FLAGS
FLAGS.__delattr__("wikipedia_auto_select_flume_mode")

from tensorflow import keras
from tensorflow.keras.callbacks import TensorBoard
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.callbacks import Callback
from multiprocessing import cpu_count

from utils import MySim, select_examples,get_data
from nn import gpu_augmenter
from augmenters import RandAugment,AutoAugment,SimCLRAugmenter,RandomErasing

from tensorflow_similarity.architectures import EfficientNetSim  # EfficientNet based image similarity model
from tensorflow_similarity.samplers import MultiShotMemorySampler  # sample data
from tensorflow_similarity.callbacks import EvalCallback  # evaluate matching performance
"""
-------------------------------------------------------------------------------------------------------------------
-----------------------------------------------Define Overall Things-----------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
hyperparameter_name = "PartialB0MySimCLR2"

# Paths
working_path = pathlib.Path().resolve()
path_object = pathlib.Path(working_path)
parent_path = path_object.parent
model_parent_path = os.path.join(parent_path, "04-Tuner/Models")
log_parent_path = os.path.join(parent_path, "04-Tuner/Logs")

model_path = os.path.join(model_parent_path, hyperparameter_name)
tensorboard_path = os.path.join(log_parent_path, hyperparameter_name)

#Sampler
img_size = 224
examples_per_class_per_batch = 4
max_querries_per_class = 100
max_targets_per_class = 200

# Callbacks
monitor = 'f1score'
monitor_mode = "max"
early_stopping_patience = 10

"""
-------------------------------------------------------------------------------------------------------------------
----------------------------------------------------Prepare Data---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------
"""
X_train,X_val,Y_train,Y_val,train_class_list,class_mappings = get_data(img_size=img_size,
                                                                       test_size=0.7
                                                                       )

num_classes = len([*Counter(Y_val).keys()])
number_of_classes = len(train_class_list)

#Steps per epoch choosen such, that in one epoch whole data is run
steps_per_epoch = int(np.max([*Counter(Y_train).values()]) // examples_per_class_per_batch)

#Perpare train and val data with MultishotMemorySampler from tensorflow_similarity
#With that it is guaranteed that each batch contains at least 2 samples per class per batch
#If EXAMPLES_PER_CLASS >2 we have EXAMPLES_PER_CLASS per Batch Batchsize = EXAMPLES_PER_CLASS * NumClasses
train_ds = MultiShotMemorySampler(X_train, Y_train,
                                  classes_per_batch=number_of_classes,
                                  examples_per_class_per_batch=examples_per_class_per_batch,
                                  class_list=train_class_list,
                                  steps_per_epoch=steps_per_epoch,
                                  )


class MyHyperModel(kt.HyperModel):
    def build(self, hp):

        trainable = hp.Choice("trainable", ["frozen", "partial","full"], default='partial')

        optimizer = hp.Choice("optimizer",
                              ["adam", "sgd", "adamax", "nadam"],
                              default='nadam')

        learning_rate = hp.Choice("lr",
                                  values=[0.0005, 0.0001, 0.00005, 0.00001, 0.000008, 0.000005],
                                  default=0.00001)

        embedding_size = hp.Choice("emb_size",
                                   values=[32, 128, 256, 320, 384, 448, 512, 1024],
                                   default=128)

        loss = hp.Choice("loss",
                         ["circle", "multi", "triplet"],
                         default='multi')


        if optimizer == 'adam':
            optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
        elif optimizer == 'sgd':
            optimizer = keras.optimizers.SGD(learning_rate=learning_rate)
        elif optimizer == 'adamax':
            optimizer = keras.optimizers.Adamax(learning_rate=learning_rate)
        elif optimizer == 'adadelta':
            optimizer = keras.optimizers.Adadelta(learning_rate=learning_rate)
        elif optimizer == "nadam":
            optimizer = keras.optimizers.Nadam(learning_rate=learning_rate)


        if loss == 'circle':
            with hp.conditional_scope("loss", ['circle']):
                gamma = hp.Choice("gamma",
                                  values=[128, 256],
                                  default=128)
                loss = tfsim.losses.CircleLoss(gamma=gamma)
        elif loss == 'multi':
            with hp.conditional_scope("loss", ['multi']):
                distance_m = hp.Choice("distance_m",
                                       ["Cosine", "L2"],
                                       default='Cosine')
                loss = tfsim.losses.MultiSimilarityLoss(distance=distance_m)
        elif loss == 'triplet':
            with hp.conditional_scope("loss", ['triplet']):
                p_mining = hp.Choice("p_mining",
                                     ["hard"],
                                     default="hard")
                n_mining = hp.Choice("n_mining",
                                     ["hard", "semi-hard"],
                                     default="semi-hard")
                distance_t = hp.Choice("distance_t",
                                       ["Cosine","L2"],
                                       default='Cosine')
                loss = tfsim.losses.TripletLoss(distance=distance_t, positive_mining_strategy=p_mining,
                                                negative_mining_strategy=n_mining)

        model = EfficientNetSim((img_size, img_size, 1), embedding_size=embedding_size, variant="B0", trainable=trainable,
                                augmentation=None)
        model.compile(optimizer, loss=loss)

        return model

    def fit(self, hp, model, train_ds, val_ds, callbacks, **kwargs):

        (X_val, y_val) = val_ds
        size = 224


        augmenter_type = hp.Choice("augmenter_type",
                                   values=["None", "RandAugment", "MySimCLR", "AutoAugment", "SimCLR", "NuxSimCLR","RandomEarising"],
                                   default="MySimCLR")
        if augmenter_type == 'RandAugment':
            with hp.conditional_scope("augmenter_type", ['RandAugment']):
                num_layers = hp.Choice("num_layers",
                                       values=[0, 1, 2, 3, 4, 5, ],
                                       default=2)
                magnitude = hp.Choice("magnitude",
                                      values=[0, 2, 4, 6, 8, 10, 12],
                                      default=10)
                augmenter = RandAugment(num_layers=num_layers, magnitude=magnitude)

        elif augmenter_type == 'AutoAugment':
            with hp.conditional_scope("augmenter_type", ['AutoAugment']):
                autoaugment = hp.Choice("autoaugment",
                                        values=["v0", "reduced_cifar10", "svhn", "reduced_imagenet"],
                                        default="v0")
                augmenter = AutoAugment(augmentation_name=autoaugment)

        elif augmenter_type == 'RandomEarising':
            with hp.conditional_scope("augmenter_type", ['RandomEarising']):
                random_erasing_probability = hp.Choice("random_erasing_probability",
                                                       values=[0.0, 0.1, 0.2, 0.3, 0.4, 0.5],
                                                       default=0.3)
                augmenter = RandomErasing(probability=random_erasing_probability)


        elif augmenter_type == 'MySimCLR':
            with hp.conditional_scope("augmenter_type", ['MySimCLR']):
                min_area = hp.Choice("min_area",
                                     values=[0.5, 0.75, 0.9],
                                     default=0.75)
                brightness = hp.Choice("brightness",
                                       values=[0.1, 0.3, 0.7],
                                       default=0.3)
                jitter = hp.Choice("jitter",
                                   values=[0.1, 0.1, 0.3],
                                   default=0.1)
                augmenter = MySim(min_area=min_area, brightness=brightness, jitter=jitter)


        elif augmenter_type == 'SimCLR':
            with hp.conditional_scope("augmenter_type", ['SimCLR']):
                simclr = hp.Choice("simclr",
                                   values=["v2", "v1"],
                                   default="v1")
                jitter_stength = hp.Choice("jitter_stength",
                                           values=[0.1, 0.3, 0.8],
                                           default=0.1)
                augmenter = SimCLRAugmenter(size, size,version=simclr,color_distort=False,jitter_stength=jitter_stength)

        elif augmenter_type == 'NuxSimCLR':
            with hp.conditional_scope("augmenter_type", ['NuxSimCLR']):
                nux_strength = hp.Choice("nux_strength",
                                 values=[0.1, 0.2, 0.3, 0.4, 0.5],
                                 default=0.3)

                nux_brightness = hp.Choice("nux_brightness",
                                           values=[0.1, 0.3, 0.5],
                                           default=0.3)
                nux_jitter = hp.Choice("nux_jitter",
                                       values=[0.1, 0.3],
                                       default=0.1)
                _augmentation = {"brightness": nux_brightness, "jitter":nux_jitter,
                                 "scale": (0.5, 1.0), "fill_scale": (0.05, 0.15),
                                 "fill_ratio": (0.5, 1.5)}

                augmenter = gpu_augmenter(in_shape=(size, size, 1), ratio=(3 / 4, 4 / 3), strength=nux_strength,
                                              fill_value=0.,
                                              **_augmentation)

        if augmenter_type=="RandAugment" or augmenter_type=="RandomEarising" or augmenter_type=="AutoAugment" or augmenter_type=="SimCLR":
            @tf.function()
            def process(img):
                img = tf.image.grayscale_to_rgb(img)
                if augmenter_type=="SimCLR":
                    img = tf.keras.layers.Rescaling(1/255)(img)
                img = augmenter.distort(img)
                if augmenter_type=="SimCLR":
                    img = tf.keras.layers.Rescaling(255)(img)
                img = tf.image.rgb_to_grayscale(img)
                return img

            def loader(x, y, *args):
                imgs = tf.stack(x)
                imgs = tf.map_fn(process, imgs, parallel_iterations=cpu_count(), dtype="float32")
                return imgs, y

        elif augmenter_type=="MySimCLR" or augmenter_type=="NuxSimCLR":
            def loader(x, y, *args):
                imgs = tf.stack(x)
                imgs = tf.keras.layers.Rescaling(1/255)(imgs)
                imgs = augmenter(imgs)
                imgs = tf.keras.layers.Rescaling(255)(imgs)
                return imgs, y

        elif augmenter_type == "None":
            def loader(x, y, *args):
                imgs = x
                return imgs, y
        examples_per_class_per_batch = hp.Choice("examples_per_class_per_batch",
                                   values=[2, 4,10,14, 20, 28],
                                   default=4)
        # steps_per_epoch_factor = hp.Choice("steps_per_epoch_factor",
        #                            values=[1,2,3, 5],
        #                            default=5)
        train_ds.augmenter = loader
        train_ds.examples_per_class_per_batch=examples_per_class_per_batch
        train_ds.steps_per_epoch = int(np.max([*Counter(Y_train).values()]) // examples_per_class_per_batch)


        history = model.fit(
            train_ds,
            validation_data=(X_val,y_val),
            callbacks=callbacks,
            **kwargs,
        )

        return {"f1score": history.history["f1score"][-1],
                "binary_accuracy": history.history["binary_accuracy"][-1],
                "val_loss":history.history["val_loss"][-1],
                }

class ClearMemory(Callback):
    def on_epoch_end(self, epoch, logs=None):
        gc.collect()
        tf.keras.backend.clear_session()

targets_x, targets_y = select_examples(X_train, Y_train, num_examples_per_class=max_targets_per_class)
queries_x, queries_y = select_examples(X_val, Y_val, num_examples_per_class=max_querries_per_class)

tsc = EvalCallback(queries_x, queries_y, targets_x, targets_y,
                   k=5,
                   matcher="match_majority_vote",
                   metrics=['f1score', 'binary_accuracy'])
tbc = TensorBoard(log_dir=tensorboard_path)
early = EarlyStopping(monitor=monitor,
                      mode=monitor_mode,
                      patience=early_stopping_patience,
                      restore_best_weights=True,
                      verbose=1)

class ClearMemory(Callback):
    def on_epoch_end(self, epoch, logs=None):
        gc.collect()
        tf.keras.backend.clear_session()

callbacks = [tsc,
             tbc,
             # early,
             ClearMemory()]


hp = kt.HyperParameters()
# loss = hp.Choice("loss", ["circle", "multi",
#                           "triplet",
#                           ], default='multi')
# with hp.conditional_scope("loss", ['circle']):
#     gamma = hp.Choice("gamma", values=[
#                                        # 64,
#                                        128,
#                                        256,
#                                        ], default=256)
# with hp.conditional_scope("loss", ['multi']):
#     distance_m = hp.Choice("distance_m", [
#                                           "Cosine",
#                                           "L2",
#                                           ], default='Cosine')
# with hp.conditional_scope("loss", ['triplet']):
#     p_mining = hp.Choice("p_mining", ["hard"], default="hard")
#     n_mining = hp.Choice("n_mining", ["hard","semi-hard"], default="hard")
#     distance_t = hp.Choice("distance_t", ["Cosine","L2"], default='Cosine')


# trainable = hp.Choice("trainable", ["frozen", "partial","full"], default='frozen')
augmenter_type = hp.Choice("augmenter_type",
                           values=[
                                   # "None",
                                   # "RandAugment",
                                   "MySimCLR",
                                   # "AutoAugment",
                                   # "SimCLR",
                                   # "NuxSimCLR",
                                   # "RandomEarising",
                                   ],
                           default="MySimCLR")
# with hp.conditional_scope("augmenter_type", ['RandAugment']):
#     num_layers = hp.Choice("num_layers",
#                            values=[1,2,3],
#                            default=3)
#     magnitude = hp.Choice("magnitude",
#                           values=[5,7,10],
#                           default=10)
# #
# with hp.conditional_scope("augmenter_type", ['AutoAugment']):
#     autoaugment = hp.Choice("autoaugment",
#                             values=["reduced_cifar10", "reduced_imagenet"],
#                             default="reduced_cifar10")
# # # #
# with hp.conditional_scope("augmenter_type", ['RandomEarising']):
#     random_erasing_probability = hp.Choice("random_erasing_probability",
#                                            values=[0.1,0.7],
#                                            default=0.1)
#
# # #
with hp.conditional_scope("augmenter_type", ['MySimCLR']):
    min_area = hp.Choice("min_area",
                         values=[0.25,0.35,0.45,0.55,0.65,0.75,],
                         default=0.25)
    brightness = hp.Choice("brightness",
                           values=[0.3,0.4,0.5,0.6],
                           default=0.3)
    jitter = hp.Choice("jitter",
                       values=[0.1,0.2],
                       default=0.2)


# with hp.conditional_scope("augmenter_type", ['SimCLR']):
#     simclr = hp.Choice("simclr",
#                        values=["v1"],
#                        default="v1")
#     jitter_stength = hp.Choice("jitter_stength",
#                                values=[0.1, 0.3],
#                                default=0.1)
#
#
# with hp.conditional_scope("augmenter_type", ['NuxSimCLR']):
#     nux_strength = hp.Choice("nux_strength",
#                              values=[0.1, 0.5],
#                              default=0.1)
#     nux_brightness = hp.Choice("nux_brightness",
#                            values=[0.1, 0.5],
#                            default=0.5)
#     nux_jitter = hp.Choice("nux_jitter",
#                        values=[0.1],
#                        default=0.1)


# embedding_size = hp.Choice("emb_size", values=[32,64,128,256,512,1024,2048], default=512)
# learning_rate = hp.Choice("lr",
#                           values=[
#                                   0.00009,0.00005,0.00001,
#                                   0.000009,0.000005,0.000001],
#                           default=0.00001)
# optimizer = hp.Choice("optimizer", ["adam", "sgd", "adamax","nadam"], default='adam')
# examples_per_class_per_batch = hp.Choice("examples_per_class_per_batch",
#                                          values=[2,4,10],
#                                          default=4)
# steps_per_epoch_factor = hp.Choice("steps_per_epoch_factor",
#                                    values=[1,3,5],
#                                    default=5)
tuner = kt.RandomSearch(
    MyHyperModel(),
    objective=kt.Objective(monitor, monitor_mode),
    seed=42,
    max_trials=1000,
    hyperparameters=hp,
    tune_new_entries=False,
    executions_per_trial=1,
    #distribution_strategy=tf.distribute.MirroredStrategy(),
    overwrite=True,
    directory=model_path,
    project_name=hyperparameter_name,
)

#
# tuner = kt.Hyperband(
#     MyHyperModel(),
#     objective=kt.Objective("val_loss", "min"),
#     seed=42,
#     max_epochs=50,
#     hyperband_iterations=5,
#     hyperparameters=hp,
#     factor=4,
#     tune_new_entries=False,
#     #distribution_strategy=tf.distribute.MirroredStrategy(),
#     overwrite=True,
#     directory=model_path,
#     project_name=hyperparameter_name,
# )


# tuner = kt.BayesianOptimization(
#     MyHyperModel(),
#     objective=kt.Objective(monitor, monitor_mode),
#     seed=42,
#     max_trials=1000,
#     hyperparameters=hp,
#     tune_new_entries=False,
#     executions_per_trial=1,
#     #distribution_strategy=tf.distribute.MirroredStrategy(),
#     overwrite=True,
#     directory=model_path,
#     project_name=hyperparameter_name,
# )

tuner.search_space_summary()
tuner.search(train_ds, epochs=120, val_ds=(X_val, Y_val), callbacks=callbacks)

