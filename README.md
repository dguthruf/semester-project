# Contrastive Learning approach for classifying complex features in diffraction images
This repo builds the codebase for the semester-project, the report can be found at [*Report*](03-Report/Semester_Project.pdf).

<p align="center">Abstract</p>

<p align="justify">
In recent years, there has been remarkable success in single diffraction
imaging of nano-sized isolated particles, due to the increasing availability
of intense short wavelength pulses, enabling in situ measurements with
ultra-high spatial and temporal resolution. Datasets consisting of millions
of unique diffraction images can already be obtained, which poses
a monumental challenge as new approaches to exploring vast amounts
of diffraction data must be found. A semantic dimensionality reduction
method that generates meaningful embeddings for downstream tasks is
crucial to such approaches. Here, we present the first study of contrastive
supervised metric learning in the domain of diffraction images, where we
learned a distance function, that maps highly abstract diffraction image
data to more suitable representation space, that is able to capture semantic
similarities within the diffraction images. In particular, we show that
our method learns a suitable data representation space in combination
with the Cosine Metric, where semantically similar images are embedded
close to each other and dissimilar are pushed apart.
</p>

![Model Results](03-Report/nn.png)
<p align="justify">
Figure 1: Here we show three randomly selected input images from three different
classes as described in Table 4: Streak, Layered, Elliptical. Next to each
input image are the images shown whose four embeddings are the closest within
the representation space. Additionally, the unique main class label can be seen
on top of every image, next to the Cosine Distance to the input image. Further,
the abbreviated multi-class labels are shown at the bottom, where each letter
corresponds to the initial letter of the features in the original multi-class labelset,
see Table 4 (e.g. an image with multi class label ”Prolate-Streak-Bent” has the
abbreviated version ”P-S-B”). Note, the maximal possible distance within the
embedding space has a value of two, since we use the Cosine Distance, consider
Eq. 2 and Section 2.1. See more examples in Figures 14, 15.</p>

Consider the [*Report*](03-Report/Semester_Project.pdf) which provides all hyperparameter settings that can be adjusted within the main.py file.



